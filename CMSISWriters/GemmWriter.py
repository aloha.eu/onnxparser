
# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.


import sys, math, os, struct
import numpy as np
from .CMSISWriter import CMSISWriter


class GemmWriter(CMSISWriter):
        def __init__ (self,  reader):

          super().__init__(reader)
        

            
#  ███████╗ █████╗ ██╗   ██╗███████╗    ██████╗  █████╗ ██████╗  █████╗ ███╗   ███╗
#  ██╔════╝██╔══██╗██║   ██║██╔════╝    ██╔══██╗██╔══██╗██╔══██╗██╔══██╗████╗ ████║
#  ███████╗███████║██║   ██║█████╗      ██████╔╝███████║██████╔╝███████║██╔████╔██║
#  ╚════██║██╔══██║╚██╗ ██╔╝██╔══╝      ██╔═══╝ ██╔══██║██╔══██╗██╔══██║██║╚██╔╝██║
#  ███████║██║  ██║ ╚████╔╝ ███████╗    ██║     ██║  ██║██║  ██║██║  ██║██║ ╚═╝ ██║
#  ╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚══════╝    ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝

        def save_parameters(self, path, RPI):
             from tqdm import tqdm
             aa=0
             filename = self.name + "_weights.h"
             if (RPI==0):
                data="int16_t"
             else:
                data="int8_t"
             with open(os.path.join(path, filename), "w") as f:           
              if ( self.operation == "Gemm"):
                w=self.reader.weights.flatten()
                w_disp=[0]*(self.isizes[0]*self.osizes[0])
                if (self.prev_layers[0].operation!="Gemm"):
                  lastif=int(self.isizes[0]/(np.prod( self.prev_layers[0].osizes[1:])))
                  for i in range(0,self.osizes[0]):
                    for j in range(0,lastif):
                      for k in range(0,np.prod( self.prev_layers[0].osizes[1:])):
                        w_disp[i*self.isizes[0]+k*lastif+j]=w[i*self.isizes[0]+j*(np.prod( self.prev_layers[0].osizes[1:]))+k]
                else:
                  w_disp=w
                f.write("const "+data+" "+ self.name+"_weights["+str(self.isizes[0])+"*"+str(self.osizes[0])+"]={")
                for wb in tqdm(w_disp, bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                  f.write("%d"%self.reader.convert_float_to_short(wb,'h',self.qf))
                  aa += 1         
                  if (aa != self.osizes[0]*self.isizes[0] and self.operation == "Gemm"):
                     f.write(",\n")                 
                  else:
                     f.write("};")

             filename = self.name + "_biases.h"
             if self.reader.biases is not None: # check if biases exsist
                biases=self.reader.biases.flatten()
             else:
                biases = np.zeros(self.osizes[0])
             with open(os.path.join(path, filename), "w") as f:
                f.write("const "+data+" "+self.name+"_biases["+str(self.osizes[0])+"]={")
                aa=0
                for wb in tqdm(biases, bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                  f.write("%d"%self.reader.convert_float_to_short(wb,'h',self.qf))
                  aa += 1
                  if (aa != self.osizes[0]):
                    f.write(",\n")
                  else:
                    f.write("};")
            
         
                  
           

#  ██████╗ ███████╗ ██████╗██╗      █████╗ ██████╗ ███████╗
#  ██╔══██╗██╔════╝██╔════╝██║     ██╔══██╗██╔══██╗██╔════╝
#  ██║  ██║█████╗  ██║     ██║     ███████║██████╔╝█████╗  
#  ██║  ██║██╔══╝  ██║     ██║     ██╔══██║██╔══██╗██╔══╝  
#  ██████╔╝███████╗╚██████╗███████╗██║  ██║██║  ██║███████╗
#  ╚═════╝ ╚══════╝ ╚═════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝
#                                                          

#---------------------------------------------------------------------------------------------------------------#
        def get_declaration_string(self):
            string = ''           
            string += '#include "{}_weights.h"\n'.format(self.name)
            string += '#include "{}_biases.h"\n'.format(self.name)
            return string
            
#---------------------------------------------------------------------------------------------------------------#

#  ██╗    ██╗██╗██████╗ ██╗███╗   ██╗ ██████╗ 
#  ██║    ██║██║██╔══██╗██║████╗  ██║██╔════╝ 
#  ██║ █╗ ██║██║██████╔╝██║██╔██╗ ██║██║  ███╗
#  ██║███╗██║██║██╔══██╗██║██║╚██╗██║██║   ██║
#  ╚███╔███╔╝██║██║  ██║██║██║ ╚████║╚██████╔╝
#   ╚══╝╚══╝ ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝ ╚═════╝ 
#                                             

        
#inherited

#  ██╗    ██╗███████╗██╗    ██╗      ██████╗  █████╗ ██████╗ 
#  ██║    ██║██╔════╝██║    ██║     ██╔═══██╗██╔══██╗██╔══██╗
#  ██║ █╗ ██║█████╗  ██║    ██║     ██║   ██║███████║██║  ██║
#  ██║███╗██║██╔══╝  ██║    ██║     ██║   ██║██╔══██║██║  ██║
#  ╚███╔███╔╝███████╗██║    ███████╗╚██████╔╝██║  ██║██████╔╝
#   ╚══╝╚══╝ ╚══════╝╚═╝    ╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ 
#                                                            

        def get_weights_loading_string(self):
          string = ''
          
          
          string += "\n"
          return string


#  ██╗███╗   ██╗██╗████████╗
#  ██║████╗  ██║██║╚══██╔══╝
#  ██║██╔██╗ ██║██║   ██║   
#  ██║██║╚██╗██║██║   ██║   
#  ██║██║ ╚████║██║   ██║   
#  ╚═╝╚═╝  ╚═══╝╚═╝   ╚═╝   
#                           


        def get_initialization_string(self):
            
            s=''
            return s

#  ███████╗ ██████╗ ██████╗ ██╗    ██╗ █████╗ ██████╗ ██████╗ 
#  ██╔════╝██╔═══██╗██╔══██╗██║    ██║██╔══██╗██╔══██╗██╔══██╗
#  █████╗  ██║   ██║██████╔╝██║ █╗ ██║███████║██████╔╝██║  ██║
#  ██╔══╝  ██║   ██║██╔══██╗██║███╗██║██╔══██║██╔══██╗██║  ██║
#  ██║     ╚██████╔╝██║  ██║╚███╔███╔╝██║  ██║██║  ██║██████╔╝
#  ╚═╝      ╚═════╝ ╚═╝  ╚═╝ ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ 
#                                                             


#---------------------------------------------------------------------------------------------------------------#
        def get_forward_string(self,RPI):
                
                if self.reader.operation == "Gemm":
                    gemm_ex = True # segnale che è stato trovato un layer Gemm
                    
                    s = "\n//################################################# Linear layer ##########################################################\n"

                    if (RPI==0):
                      s += " arm_fully_connected_q15 ((q15_t *){}_input, (q15_t *){}_weights, {}, {}, {}, {}, (q15_t *) {}_biases, (q15_t *) {}_output, (q15_t *) test_col_buffer) ;".format (self.name,self.name,self.isizes[0],self.osizes[0],self.qf,self.qf,self.name,self.name)                    
                    else:
                      if (self.reader.prev_layers[0].operation!="Gemm"):
                        s += "int16_t* {}_input=(int16_t*){}_input;\n".format(self.reader.name, self.reader.prev_layers[0].name)
                        s += "for (int j=0;j<{};j++) {}_input[j]=(int16_t){}_output[j];\n".format(self.isizes[0],self.reader.name,self.reader.prev_layers[0].name)
                      s += "arm_fully_connected_mat_q7_vec_q15 ({}_input, (q7_t *){}_weights, {}, {}, {}, {}, (q7_t *) {}_biases, (q15_t *) {}_output, (q15_t *) test_col_buffer) ;".format (self.name,self.name,self.isizes[0],self.osizes[0],self.qf,self.qf,self.name,self.name)
                return s

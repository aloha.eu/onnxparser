
# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.

import sys, math, os, struct
import numpy as np
from .CMSISWriter import CMSISWriter


class ConvWriter(CMSISWriter):
        def __init__ (self, reader):

          super().__init__(reader)
        


#  ███████╗ █████╗ ██╗   ██╗███████╗    ██████╗  █████╗ ██████╗  █████╗ ███╗   ███╗
#  ██╔════╝██╔══██╗██║   ██║██╔════╝    ██╔══██╗██╔══██╗██╔══██╗██╔══██╗████╗ ████║
#  ███████╗███████║██║   ██║█████╗      ██████╔╝███████║██████╔╝███████║██╔████╔██║
#  ╚════██║██╔══██║╚██╗ ██╔╝██╔══╝      ██╔═══╝ ██╔══██║██╔══██╗██╔══██║██║╚██╔╝██║
#  ███████║██║  ██║ ╚████╔╝ ███████╗    ██║     ██║  ██║██║  ██║██║  ██║██║ ╚═╝ ██║
#  ╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚══════╝    ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝

        def save_parameters(self, path, RPI):
          from tqdm import tqdm
          aa = 0
          print ("\n"+self.name)
          if self.operation == "Conv":
            if self.reader.weights is not None:
              print("RPI CONV "+str(RPI))
              filename = self.name + "_weights.h"
              if (RPI==0):
                data="int16_t"
              else:
                data="int8_t"
              if (self.isizes[0]==1):
                trueIF=2
              else: 
                trueIF=self.isizes[0]
              w_disp = [0]*(self.kernel[0]*self.kernel[1]*trueIF*self.osizes[0])   #riordina i pesi per cmsis
              w = self.reader.weights.flatten()
              for i in range(0,self.osizes[0]):
                for j in range(0,self.isizes[0]):
                  for k in range(0,self.kernel[0]*self.kernel[1]):
                     w_disp[i*trueIF*self.kernel[0]*self.kernel[1]+k*trueIF+j]=w[i*self.isizes[0]*self.kernel[0]*self.kernel[1]+j*self.kernel[0]*self.kernel[1]+k]
           
              with open(os.path.join(path, filename), "w") as f:
                f.write("const "+data+ " "+self.name+"_weights["+str(self.kernel[0])+"*"+str(self.kernel[1])+"*"+str(trueIF)+"*"+str(self.osizes[0])+"]={")
                for wb in tqdm(w_disp, bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                  f.write("%d"%self.reader.convert_float_to_short(wb,'h',self.qf))
                  aa += 1
                  if (aa != self.kernel[0]*self.kernel[1]*trueIF*self.osizes[0]):
                     f.write(",\n")
                  else:
                     f.write("};")
              if self.reader.biases is not None: # check if biases exsist
                biases = self.reader.biases
              else:
                biases = np.zeros(self.osizes[0])
              filename = self.name + "_biases.h"
              with open(os.path.join(path, filename), "w") as f:
                f.write("const "+data+" "+self.name+"_biases["+str(self.osizes[0])+"]={")
                aa = 0
                for wb in tqdm(biases.flatten(), bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                  f.write("%d"%self.reader.convert_float_to_short(wb,'h',self.qf))
                  aa += 1
                  if (aa != self.osizes[0] ):
                    f.write(",\n")
                  else:
                    f.write("};")
              if self.reader.mulbn_scaler is not None: # check if biases exsist
                scaler = self.reader.mulbn_scaler
                filename="{}_scalers.h".format(self.reader.mulbn_name)
                with open(os.path.join(path, filename), "w") as f:
                  f.write("const int "+self.reader.mulbn_name+"_scaler["+str(len(scaler))+"]={")
                  aa=0
                  for wb in tqdm(scaler, bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                    f.write("%d"%wb)
                    aa += 1
                    if (aa != len(scaler) ):
                      f.write(",\n")
                    else:
                      f.write("};")
              if self.reader.addbn_bias is not None: # check if biases exsist
                bias = self.reader.addbn_bias
                filename="{}_bias.h".format(self.reader.addbn_name)
                with open(os.path.join(path, filename), "w") as f:
                  f.write("const int "+self.reader.addbn_name+"_bias["+str(len(bias))+"]={")
                  aa=0
                  for wb in tqdm(bias, bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                    f.write("%d"%wb)
                    aa += 1
                    if (aa != len(bias) ):
                      f.write(",\n")
                    else:
                      f.write("};")
              if self.reader.mulrelu_scaler is not None: # check if biases exsist
                scaler = self.reader.mulrelu_scaler
                filename="{}_scalers.h".format(self.reader.mulrelu_name)
                with open(os.path.join(path, filename), "w") as f:
                  f.write("const int "+self.reader.mulrelu_name+"_scaler["+str(len(scaler))+"]={")
                  aa=0
                  for wb in tqdm(scaler, bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                    f.write("%d"%wb)
                    aa += 1
                    if (aa != len(scaler) ):
                      f.write(",\n")
                    else:
                      f.write("};")
              if self.reader.divrelu_divider is not None: # check if biases exsist
                divider = self.reader.divrelu_divider
                filename="{}_divider.h".format(self.reader.divrelu_name)
                with open(os.path.join(path, filename), "w") as f:
                  if type(divider) is int:
                    f.write("const int "+self.reader.divrelu_name+"_divider[1]={"+str(divider)+"};")
                  else:
                    f.write("const int "+self.reader.divrelu_name+"_divider["+str(len(divider))+"]={")
                    aa=0
                    for wb in tqdm(divider, bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                      f.write("%d"%wb)
                      aa += 1
                      if (aa != len(divider) ):
                        f.write(",\n")
                      else:
                        f.write("};") 
            
          

#  ██████╗ ███████╗ ██████╗██╗      █████╗ ██████╗ ███████╗
#  ██╔══██╗██╔════╝██╔════╝██║     ██╔══██╗██╔══██╗██╔════╝
#  ██║  ██║█████╗  ██║     ██║     ███████║██████╔╝█████╗  
#  ██║  ██║██╔══╝  ██║     ██║     ██╔══██║██╔══██╗██╔══╝  
#  ██████╔╝███████╗╚██████╗███████╗██║  ██║██║  ██║███████╗
#  ╚═════╝ ╚══════╝ ╚═════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝
#                                                          

#---------------------------------------------------------------------------------------------------------------#
        def get_declaration_string(self):
            string = ''
            if self.reader.operation == "Conv":
             string += '#include "{}_weights.h"\n'.format(self.reader.name)
             string += '#include "{}_biases.h"\n'.format(self.reader.name)  
             if self.reader.mulbn_name is not None:
               string += '#include "{}_scalers.h"\n'.format(self.reader.mulbn_name)
               string += '#include "{}_bias.h"\n'.format(self.reader.addbn_name)
               string += '#include "{}_scalers.h"\n'.format(self.reader.mulrelu_name)
               string += '#include "{}_divider.h"\n'.format(self.reader.divrelu_name)    
            return string
            
        
       
#---------------------------------------------------------------------------------------------------------------#

#  ██╗    ██╗██╗██████╗ ██╗███╗   ██╗ ██████╗ 
#  ██║    ██║██║██╔══██╗██║████╗  ██║██╔════╝ 
#  ██║ █╗ ██║██║██████╔╝██║██╔██╗ ██║██║  ███╗
#  ██║███╗██║██║██╔══██╗██║██║╚██╗██║██║   ██║
#  ╚███╔███╔╝██║██║  ██║██║██║ ╚████║╚██████╔╝
#   ╚══╝╚══╝ ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝ ╚═════╝ 
#                                             


#inherited
        

#  ██╗    ██╗███████╗██╗    ██╗      ██████╗  █████╗ ██████╗ 
#  ██║    ██║██╔════╝██║    ██║     ██╔═══██╗██╔══██╗██╔══██╗
#  ██║ █╗ ██║█████╗  ██║    ██║     ██║   ██║███████║██║  ██║
#  ██║███╗██║██╔══╝  ██║    ██║     ██║   ██║██╔══██║██║  ██║
#  ╚███╔███╔╝███████╗██║    ███████╗╚██████╔╝██║  ██║██████╔╝
#   ╚══╝╚══╝ ╚══════╝╚═╝    ╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ 
#                                                            

        def get_weights_loading_string(self):
          string = ''
                   
          
          string += "\n"
          return string


#  ██╗███╗   ██╗██╗████████╗
#  ██║████╗  ██║██║╚══██╔══╝
#  ██║██╔██╗ ██║██║   ██║   
#  ██║██║╚██╗██║██║   ██║   
#  ██║██║ ╚████║██║   ██║   
#  ╚═╝╚═╝  ╚═══╝╚═╝   ╚═╝   
#                           


        def get_initialization_string(self):
         
            s = ''
                
            return s


#  ███████╗ ██████╗ ██████╗ ██╗    ██╗ █████╗ ██████╗ ██████╗ 
#  ██╔════╝██╔═══██╗██╔══██╗██║    ██║██╔══██╗██╔══██╗██╔══██╗
#  █████╗  ██║   ██║██████╔╝██║ █╗ ██║███████║██████╔╝██║  ██║
#  ██╔══╝  ██║   ██║██╔══██╗██║███╗██║██╔══██║██╔══██╗██║  ██║
#  ██║     ╚██████╔╝██║  ██║╚███╔███╔╝██║  ██║██║  ██║██████╔╝
#  ╚═╝      ╚═════╝ ╚═╝  ╚═╝ ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ 
#                                                             


#---------------------------------------------------------------------------------------------------------------#
        def get_forward_string(self,RPI):
                if (self.isizes[0]==1):
                   trueIF = 2
                else:
                   trueIF = self.isizes[0]
                s=''

                s += "\n\n//################################################    Conv layer ###############################################\n"
                s += "//kernel {}x{}\n".format (self.reader.kernel[0], self.reader.kernel[1])

                if self.reader.mulbn_name is not None:
                  if (self.isizes[0]==1):
                     s += "arm_convolve_HWC_q7_basic_nonsquareRPI((uint8_t*){}_input, {}, {}, {},(q7_t *){}_weights, {}, {}, {}, {}, {}, {}, {}, (q7_t *){}_biases, {}, {}, (uint8_t *){}_output,{}, {},\n".format(self.name,self.isizes[2],self.isizes[1],trueIF,self.name,self.osizes[0],self.kernel[0],self.kernel[1],self.padding[0],self.padding[1],self.stride[0],self.stride[1],self.name,self.qf,self.qf,self.name,self.osizes[2],self.osizes[1])
                     s += "                                       (int*){}_scaler, (int*){}_bias, (int*){}_scaler, (int*){}_divider, {}, {},(int16_t*)test_col_buffer, NULL);".format(self.reader.mulbn_name,self.reader.addbn_name,self.reader.mulrelu_name,self.reader.divrelu_name,self.reader.clip_min,self.reader.clip_max)
                  else:
                     s += "arm_convolve_HWC_q7_fast_nonsquareRPI((uint8_t*){}_input, {}, {}, {},(q7_t *){}_weights, {}, {}, {}, {}, {}, {}, {}, (q7_t *){}_biases, {}, {}, (uint8_t *){}_output,{}, {},\n".format(self.name,self.isizes[2],self.isizes[1],trueIF,self.name,self.osizes[0],self.kernel[0],self.kernel[1],self.padding[0],self.padding[1],self.stride[0],self.stride[1],self.name,self.qf,self.qf,self.name,self.osizes[2],self.osizes[1])
                     s += "                                      (int*){}_scaler, (int*){}_bias, (int*){}_scaler, (int*){}_divider, {}, {},(int16_t*)test_col_buffer, NULL); ".format(self.reader.mulbn_name,self.reader.addbn_name,self.reader.mulrelu_name,self.reader.divrelu_name,self.reader.clip_min,self.reader.clip_max)
                else:
                     s += "arm_convolve_HWC_q15_fast_nonsquare((q15_t *){}_input, {}, {}, {},(q15_t *){}_weights, {}, {}, {}, {}, {}, {}, {},(q15_t *){}_biases, {}, {}, (q15_t *){}_output, {}, {}, (q15_t*)test_col_buffer, NULL);\n".format(self.name,self.isizes[2],self.isizes[1],trueIF,self.name,self.osizes[0],self.kernel[0],self.kernel[1],self.padding[0],self.padding[1],self.stride[0],self.stride[1],self.name,self.qf,self.qf,self.name,self.osizes[2],self.osizes[1])
                return s
                
                

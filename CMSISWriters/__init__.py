from .ConvWriter import ConvWriter
from .CMSISWriter import CMSISWriter
from .MaxPoolWriter import MaxPoolWriter
from .ReluWriter import ReluWriter
from .GemmWriter import GemmWriter
from .AddWriter import AddWriter
from .MulWriter import MulWriter
from .DivWriter import DivWriter
from .Float2FixedWriter import Float2FixedWriter
from .Fixed2FloatWriter import Fixed2FloatWriter
from .ClipWriter import ClipWriter
from .BatchNormalizationWriter import BatchNormalizationWriter
from .FlattenWriter import FlattenWriter
from .RegionWriter import RegionWriter

mapping = {"Conv"               : ConvWriter,
           "Gemm"               : GemmWriter,
           "MaxPool"            : MaxPoolWriter,
           "AveragePool"        : MaxPoolWriter,
           "Relu"               : ReluWriter,
           "LeakyRelu"          : ReluWriter,
           "Add"                : AddWriter,
           "Mul"                : MulWriter,
           "Div"                : DivWriter,
           "Clip"               : ClipWriter,
           "BatchNormalization" : BatchNormalizationWriter,
           "Flatten"            : FlattenWriter,
           "Float2Fixed"        : Float2FixedWriter,
           "Fixed2Float"        : Fixed2FloatWriter,
           "Region"             : RegionWriter,
    }
    
    
supported_operators=["Conv", "Gemm", "MaxPool", "AveragePool", "Relu", "LeakyRelu", "Pad", "Fixed2Float","Float2Fixed", "Flatten",
                     "Add", "Mul", "Div", "BatchNormalization", "Region", "Sigmoid", "Concat", "Floor", "BitShift", "Clip", "Constant", "Cast","Input"]
         

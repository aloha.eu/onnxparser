
# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.

from Initializer import *
import sys, math, os
import numpy as np
import onnxReaders as rd
import ARMCLWriters as wr


class NetWriterARMCL():

        def __init__ (self, model_file, path, app_name,  weights_path, readers, init, graph, input_size, **kwargs):
          
          try:
            qf = kwargs['qf']
          except:
            qf = 8
            
          try:
            yolo_like = kwargs['yolo_like']
          except:
            yolo_like = False
            
          self.app_name = app_name
          self.path = path
          
          self.readers = readers
          self.weights_path = weights_path
          
          self.model_file     = model_file

          self.yolo_like      = yolo_like
          self.qf             = qf
          
          self.init     = init
          
          self.input_size     = input_size
          
          self.net = []
          

          i=0
          for r in readers:
                       
            if r.operation in wr.mapping.keys():
              w = wr.mapping[r.operation](r)
              self.net.append(w)
            elif r.operation not in wr.supported_operators:
              print ("Operator not supported: {} in {}".format(r.operation, r.name))
              exit(1)
            else:
              print ("Skipping operator: {} in {}".format(r.operation, r.name))
            
            
            
            
            
            
            
            
            
            
        def save_parameters(self):
        
            print ("Extracting weights...")
            
            os.makedirs(self.weights_path, exist_ok=True)

            for layer in self.net:
              layer.save_parameters(self.weights_path)
          
          
            maxog = 4
            conv_line = '{"name":"%s","weights":{"file":"%s","size":[%d,%d,%d,%d]},"bias":{"file":"%s","size":[%d]}, "maxog":%d}\n'
            with open(os.path.join(self.weights_path,"conv.json"), 'w') as f:
              for layer in self.net:
                if layer.operation=="Conv": # and layer.weights != None and layer.biases != None:
                  w = layer.name + "_weights.bin"
                  b = layer.name + "_biases.bin"
                  wshape = [layer.osizes[0], layer.isizes[0], layer.kernel, layer.kernel]
                  bshape = [layer.osizes[0]]
                  
                  f.write(conv_line%(layer.name, w, wshape[0], wshape[1], wshape[2], wshape[3], b, bshape[0], maxog))
        
          
      
        
        
        def optimizeNet (netReader, batch_norm=False, neuraghe=False):
#           netReader.removePreProcessing(['Conv'])
#           netReader.removeOperators(['Cast', 'Dropout', 'Flatten'])
#           
#           netReader.batch_norm_folding(batch_norm)
#           netReader.md2shift()
#           netReader.mul_rshift_folding()
#           netReader.stride2subsampling(neuraghe)
#           netReader.datatype_conversion()
           pass

        
        
        
        def net_description(self):
           
            net_description = "Source file: %s\n\n" % self.model_file
            net_description +="%-20s %5s %5s %5s %5s %5s %5s\n"%("Layer", "IF", "ih", "iw", "OF", "oh", "ow")
            
            for layer in self.net:
                net_description+= "%-20s %5d %5d %5d %5d %5d %5d\n"%(layer.name, layer.isizes[0], layer.isizes[1], layer.isizes[2], layer.osizes[0], layer.osizes[1], layer.osizes[2])
            return net_description
            
            
            
#---------------------------------------------------------------------------------------------------------------------------------------------
        def print_net(self):
        
                
                name_file        = os.path.join(self.path, self.app_name +".cpp")
                
                print (self.net_description())
                
                print ("Creating file {} ...".format(name_file))

                file_name_weights = "weights.npz"
                file_name_bias = "bias.npz"
                name_file = open(name_file,"w")
                name_file.write("\n//This code is auto_generated.//\n")
                name_file.write("\n//This file must be embedded in the host application that will provide the data to be processed.\n\n\n\n")
                name_file.write("/* ################## Net description ###################\n")
                name_file.write(self.net_description())
                name_file.write("#########################################################*/\n")

                name_file.write('#include "arm_compute/graph.h"\n')
                name_file.write('#include "support/ToolchainSupport.h"\n')
                name_file.write('#include "utils/CommonGraphOptions.h"\n')
                name_file.write('#include "utils/GraphUtils.h"\n')
                name_file.write('#include "utils/Utils.h"\n\n')

                name_file.write('using namespace arm_compute::utils;\n')
                name_file.write('using namespace arm_compute::graph::frontend;\n')
                name_file.write('using namespace arm_compute::graph_utils;\n\n')

                name_file.write('class {} : public Example{{\n\n'.format(self.app_name))

                name_file.write('public:\n')
                name_file.write('    {}()\n'.format(self.app_name))
                name_file.write('        : cmd_parser(), common_opts(cmd_parser), common_params(), graph(0, "{}")\n'.format(self.app_name))
                name_file.write('    {\n')
                name_file.write('    }\n')
                name_file.write('    bool do_setup(int argc, char **argv) override\n')
                name_file.write('    {\n')
                name_file.write('        // Check if the system has enough RAM to run the example, systems with less than 2GB have\n')
                name_file.write('        // to hint the API to minimize memory consumption otherwise it\'ll run out of memory and\n')
                name_file.write('        // fail throwing the bad_alloc exception\n')
                name_file.write('        arm_compute::MEMInfo meminfo;\n')
                name_file.write('        const size_t         mem_total = meminfo.get_total_in_kb();\n')
                name_file.write('        if(mem_total <= arm_compute::MEMInfo::TWO_GB_IN_KB)\n')
                name_file.write('        {\n')
                name_file.write('            arm_compute::MEMInfo::set_policy(arm_compute::MemoryPolicy::MINIMIZE);\n')
                name_file.write('        }\n\n')

                name_file.write('        // Parse arguments\n')
                name_file.write('        cmd_parser.parse(argc, argv);\n')
                name_file.write('        cmd_parser.validate();\n\n')

                name_file.write('        // Consume common parameters\n')
                name_file.write('        common_params = consume_common_graph_parameters(common_opts);\n\n')

                name_file.write('        // Return when help menu is requested\n')
                name_file.write('        if(common_params.help)\n')
                name_file.write('        {\n')
                name_file.write('            cmd_parser.print_help(argv[0]);\n')
                name_file.write('            return false;\n')
                name_file.write('        }\n\n')

                name_file.write('        // Print parameter values\n')
                name_file.write('        std::cout << common_params << std::endl;\n\n')

                name_file.write('        // Get trainable parameters data path\n')
                name_file.write('        std::string data_path = common_params.data_path;\n\n')
                
                

                name_file.write('        // Create a preprocessor object\n')
                name_file.write('        // const std::array<float, 3> mean_rgb{ { 123.68f, 116.779f, 103.939f } };\n')
                name_file.write('        // std::unique_ptr<IPreprocessor> preprocessor = arm_compute::support::cpp14::make_unique<CaffePreproccessor>(mean_rgb);\n\n')

                name_file.write('        // Create input descriptor\n')
                name_file.write('        const auto        operation_layout = common_params.data_layout;\n')
                name_file.write('        const TensorShape tensor_shape     = permute_shape(TensorShape({}U, {}U, {}U, 1U), DataLayout::NCHW, operation_layout);\n'.format(self.input_size[2],self.input_size[1],self.input_size[0]))
                name_file.write('        TensorDescriptor  input_descriptor = TensorDescriptor(tensor_shape, common_params.data_type).set_layout(operation_layout);\n')

                name_file.write('        // Set weights trained layout\n')
                name_file.write('        const DataLayout weights_layout = DataLayout::NCHW;\n\n')

                name_file.write('        // Create graph\n\n')
                
                
                name_file.write('        graph << common_params.target\n')
                name_file.write('              << common_params.fast_math_hint\n')
                name_file.write('              << InputLayer(input_descriptor, get_input_accessor(common_params/*, std::move(preprocessor)*/))\n')
                
                

                
                for layer in self.net:

                    name_file.write(layer.get_forward_string())
#------------------------------------------------------------------------------------------------------------------------------------------------

                name_file.write('              << SoftmaxLayer().set_name("prob")\n')
                name_file.write('              << OutputLayer(get_output_accessor(common_params));\n\n')

                name_file.write('        // Finalize graph\n')
                name_file.write('        GraphConfig config;\n')
                name_file.write('        config.num_threads = common_params.threads;\n')
                name_file.write('        config.use_tuner   = common_params.enable_tuner;\n')
                name_file.write('        config.tuner_mode  = common_params.tuner_mode;\n')
                name_file.write('        config.tuner_file  = common_params.tuner_file;\n\n')

                name_file.write('        graph.finalize(common_params.target, config);\n\n')

                name_file.write('        return true;\n')
                name_file.write('    }\n')
                name_file.write('    void do_run() override\n')
                name_file.write('    {\n')
                name_file.write('        // Run graph\n')
                name_file.write('        graph.run();\n')
                name_file.write('    }\n\n')

                name_file.write('private:\n')
                name_file.write('    CommandLineParser  cmd_parser;\n')
                name_file.write('    CommonGraphOptions common_opts;\n')
                name_file.write('    CommonGraphParams  common_params;\n')
                name_file.write('    Stream             graph;\n')
                name_file.write('};\n\n')

                name_file.write('/** Main program \n')
                name_file.write(' *\n')
                name_file.write(' * @note To list all the possible arguments execute the binary appended with the --help option\n')
                name_file.write(' *\n')
                name_file.write(' * @param[in] argc Number of arguments\n')
                name_file.write(' * @param[in] argv Arguments\n')
                name_file.write(' */\n')
                name_file.write('int main(int argc, char **argv)\n')
                name_file.write('{\n')
                name_file.write('    return arm_compute::utils::run_example<{}>(argc, argv);\n'.format(self.app_name))
                name_file.write('}\n\n')
                
     
     
     
     
          

# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.


import time
import numpy as np
from  onnx import numpy_helper
import struct
import sys
import math
from array import array

class Initializer:
        def __init__ (self, model, path, graph):
            self.model                 = model
            self.graph                 = graph
            self.initializer           = self.model.graph.initializer
            self.all_nodeslist         = graph.node #self.model.graph.node
            self.path = path
           # self.weights, self.weights_size               = self.find_weights()
            
            self.parameters, self.parameters_size               = self.find_parameters()

           # self.bias                  = self.find_bias()
            self.constants         = self.find_constants()

            self.net_input             = self.net_input()
        #    self.model_input_shape     = self.input_model_shape()
            self.layer_input_shape     = self.input_layer_shape()
            self.layer_output_shape    = self.output_model_shape()
            #self.conv_7x7_finder       = self.conv_7x7_layer_finder()
            self.model_input_shape = self.model_input_shape()
            self.channels = self.find_channels()


        def find_constants(self):
          from onnx import numpy_helper
          constants={}
          for node in self.all_nodeslist:
            if node.op_type == "Constant":
            #  print ("aaaaaaaaaaaaaafdsetr")
              #print (node)
              value = numpy_helper.to_array( node.attribute[0].t)    
              
              try:
                a = list(value.flatten())
              except:
                a = [value]
              constants["_{}".format(node.output[0])] = a
        
          return constants

#----------  Returns a dictionary containing channels number for each conv layer, keys are input names -------------------------------------------------------------------------------#
        def find_channels(self):
            channels ={}
            for el in self.model.graph.input:
                # elimino i caratteri "/" dalle stringe dei pesi per evitare errori con pytorch
                el_name = el.name
                el_name = el_name.replace("/","_").replace(".","_")
                el_name = "_{}".format(el_name)
                ch = []
                shape = el.type.tensor_type.shape.dim
                i = 0
                for dim in shape:
                    i = i +1
                for dim in shape:
                    if i>1:      # evito di considerare gli input legati ai pesi
                        ch.append(dim.dim_value)
                        channels[el_name] = ch                 #metto i pesi e l'input corrispondente in un dizionario i cui indici sono i nomi delgi input
            return (channels)

#-----------------------------------------------------------------------------------------------------------------------------------------------------
        def net_input (self):
            all_nodeslist = self.all_nodeslist
            
            output_list = []
            for node in all_nodeslist:
                for l in node.output:
                    l = l.replace("/","_").replace(".","_")
                    l = "_{}".format(l)
                    output_list.append(l)
            # Indiviuo l'input della rete, non deve essere contenuto nel dizionario e non deve trovarsi tra gli output di qualche layer
            for node in all_nodeslist:
                lenght = len(node.input)
                for k in range (lenght):
                    node.input[k] =node.input[k]. replace("/","_").replace(".","_")
                    node.input[k] = "_{}".format(node.input[k])
                    if  node.input[k] not in self.parameters.keys() and node.input[k] not in output_list:
                        net_input = node.input[k]
                        net_input = net_input.replace("/","_").replace(".","_")
                    node.input[k] = node.input[k][1:]
            return net_input

#------------------------------------------------------------------------------------------------------------------------------------------
        def find_weights(self):
          #  start_time_weights = time.time()
            initializer =  self.initializer
            all_nodeslist = self.all_nodeslist
            weights = {}
            weights_size ={}
            # elimino i caratteri "/" dalle stringe dei bias per evitare errori con pytorch
            
            from tqdm import tqdm
            
            for k in tqdm(initializer, bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                weights_name = k.name
                weights_name = weights_name.replace("/","_").replace(".","_")
                weights_name = "_{}".format(weights_name)
                init_el = numpy_helper.to_array(k)
             #   print (weights_name)
             #   print (init_el.shape)
                if init_el.ndim != 1:   # individuo i bias, la matrice dei bias ha una sola dimensione
                        shape = init_el.shape
                        weights[weights_name] = init_el            #metto i pesi e l'input corrispondente in un dizionario i cui indici sono i nomi delgi input
                        weights_size[weights_name]= shape
          #  print("---weights Completed in  %s seconds ---" % (time.time() - start_time_weights))
           # print (weights_size)
            return weights, weights_size

#------------------------------------------------------------------------------------------------------------------------------------------
        def find_parameters(self):
          #  start_time_parameters = time.time()
            initializer =  self.initializer
            all_nodeslist = self.all_nodeslist
            parameters = {}
            parameters_size ={}
            # elimino i caratteri "/" dalle stringe dei bias per evitare errori con pytorch
            
            from tqdm import tqdm
            
            for k in tqdm(initializer, bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                parameters_name = k.name
                parameters_name = parameters_name.replace("/","_").replace(".","_")
                parameters_name = "_{}".format(parameters_name)
                init_el = numpy_helper.to_array(k)
             #   print (parameters_name)
             #   print (init_el.shape)
                shape = init_el.shape
                parameters[parameters_name] = init_el            #metto i pesi e l'input corrispondente in un dizionario i cui indici sono i nomi delgi input
                parameters_size[parameters_name]= shape
          #  print("---parameters Completed in  %s seconds ---" % (time.time() - start_time_parameters))
           # print (parameters_size)
            return parameters, parameters_size



#--------------------------------------------------------------------------------------------------------------------------------------
        def find_bias(self):
           # start_time_bias = time.time()
            initializer =  self.initializer
            all_nodeslist = self.all_nodeslist
            bias = {}
            # elimino i caratteri "/" dalle stringe dei bias per evitare errori con pytorch
            
            from tqdm import tqdm
            
            for k in tqdm(initializer, bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                bias_name = k.name
                bias_name = bias_name.replace("/","_").replace(".","_")
                bias_name = "_{}".format(bias_name)
                init_el = numpy_helper.to_array(k)
                if init_el.ndim == 1:   # individuo i bias, la matrice dei bias ha una sola dimensione
                        bias[bias_name] = init_el            #metto i bias e l'input corrispondente in un dizionario i cui indici sono i nomi delgi input
          #  print("---bias Completed in  %s seconds ---" % (time.time() - start_time_bias))
            return bias


        def export_parameters(self):
            import numpy as np
            import os
            os.makedirs(self.path, exist_ok = True)
            print (self.path)
            np.savez(os.path.join(self.path, "parameters.npz"), **self.parameters)

        def write_weights(self):
            import numpy as np
            import os
            start_time_write_weights = time.time()
          # scrive una lista contente i pesi e il nome del nodo a cui sono associati
            os.makedirs(self.path, exist_ok = True)
            print (self.path)
            np.savez(os.path.join(self.path, "weights.npz"), **self.weights)
            print("---write_weights Completed in  %s seconds ---" % (time.time() - start_time_write_weights))


        def write_bias(self):
            import numpy as np
            import os
            start_time_write_bias = time.time()
            os.makedirs(self.path, exist_ok = True)
            print (self.path)
            np.savez(os.path.join(self.path, "bias.npz"), **self.bias)
            print("---write_bias Completed in  %s seconds ---" % (time.time() - start_time_write_bias))




#--------------------------------------   restituisce le dimensioni dell'input del modello ------------------------------------------------

        def input_model_shape (self):
            graph = self.graph
            model_input = []
            for input_ in graph.input:
                if input_.name == "data_0" or input_.name == "image" or input_.name == "gpu_0/data_0": # trovare una soluzione più generale per identificare l' input della rete
                    dim = input_.type.tensor_type.shape.dim
                    model_input.append(dim)
            return model_input

#---------- Returns batch size, channels, width and lenght of net input -------------------------------------------------------------------------------#
        def model_input_shape (self):
            model = self.model
            shape = []
            net_input =  self.net_input
            net_input = net_input[1:]
            for n in model.graph.input:
                if net_input ==  n.name.replace('.','_'):
                    net_input = n
            net_input_shape = net_input.type.tensor_type.shape.dim
            for dim in net_input_shape:
                shape.append(dim.dim_value)

            return shape

#----------------- restituisce le dimensioni degli input della rete che si trovano all'interno di initalizer nel modello onnx
        def input_layer_shape (self):
            graph = self.graph

            layer_input_shape = {}
            for input_ in graph.input:
                input_name = input_.name
                input_name = input_name.replace("/","_").replace(".","_")
                input_name = "_{}".format(input_name)
                dim = input_.type.tensor_type.shape.dim
                if len(dim) >0 :
                    dim = dim[0]
                else:
                    dim = 0
                layer_input_shape[input_name] = dim
            return layer_input_shape

#------------------ restituisce il nome dell'output della rete ----------------------------------
        def output_model_shape (self):
            graph = self.graph
            layer_output = []
            for output_ in graph.output:
                dim = output_.type.tensor_type.shape.dim
                layer_output.append(dim)
            return layer_output
        
        
        


# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.

from Initializer import *
import sys, math, os
import numpy as np
import onnxReaders as rd
import pytorchWriters as wr


class NetWriterPytorch():

        def __init__ (self, model_file, path, app_name,  weights_path, readers, init, graph, input_size, **kwargs):
          
          try:
            softmax      = kwargs['softmax']
          except:
            softmax = False
          
          try:
            yolo_like      = kwargs['yolo_like']
          except:
            yolo_like = False
          
          self.app_name = app_name
          self.path = path
          
          self.readers = readers
          self.weights_path = weights_path
          
          self.model_file     = model_file

          self.yolo_like      = yolo_like
          
          self.load_params = kwargs.get('load_params', True)

          
          self.init     = init
          
          self.input_size     = input_size
          
          self.net = []
          self.softmax_layer=softmax

          i=0
          for r in readers:
                       
            if r.operation in wr.mapping.keys():
              print (r.operation)
              w = wr.mapping[r.operation](r)
              self.net.append(w)
            elif r.operation not in wr.supported_operators:
              print ("[WRITER] Operator not supported: {} in {}".format(r.operation, r.name))
              exit(1)
            else:
              print ("[WRITER] Skipping operator: {} in {}".format(r.operation, r.name))
            
            
            
          #self.init.write_weights()
          #self.init.write_parameters()
            
            
            
            
        #class method
        def optimizeNet (netReader, batch_norm=False, neuraghe=False):
          #netReader.removePreProcessing(['Conv', 'Pad', 'Gemm'])
          netReader.removeOperators(['Cast'])
          netReader.batch_norm_folding(batch_norm)
          netReader.add_flatten()
          netReader.remove_redundant_Pad()
          netReader.padlayer_folding()
          pass
        
        def save_parameters(self):
        
            
          self.init.export_parameters()
        
          
      

        
        def net_description(self):
           
          ndim = len(self.net[0].isizes)
          
          formatters="".join(["%5s "]*ndim)
          
            
          description  = "Source file: %s\n\n" % self.model_file
          description += "%-20s " % ("Layer")
          description += "%5s "%"IF"
          for s in range (1,ndim):
           description += "%5s "%("ix{}".format(s-1))
          
          description += "%5s "%"OF"
          
          for s in range (1,ndim):
           description += "%5s "%("ox{}".format(s-1))
           
          description += "\n"
          
          
          for layer in self.net:
              ndimi = len(layer.isizes)
              ndimo = len(layer.osizes)
              formattersi="".join(["%5s "]*ndimi)
              formatterso="".join(["%5s "]*ndimo)
              description += "%-20s " + formattersi + formatterso + "\n"
              description  = description%(layer.name, *layer.isizes, *layer.osizes)
          return description
            
            
        

#  ███╗   ██╗███████╗████████╗
#  ████╗  ██║██╔════╝╚══██╔══╝
#  ██╔██╗ ██║█████╗     ██║   
#  ██║╚██╗██║██╔══╝     ██║   
#  ██║ ╚████║███████╗   ██║   
#  ╚═╝  ╚═══╝╚══════╝   ╚═╝   
#                             

        def print_net(self, tool_specific=None):
            import os

            #file_name_weights = os.path.join(self.path, "parameters.npz")
            #file_name_bias = os.path.join(self.path, "parameters.npz")
            
            file_name_parameters = os.path.join( self.path,"parameters.npz")
            name_file = open(os.path.join(self.path, "Net.py"),"w")
            name_file.write("#----This code is auto-generated.----#")

            name_file.write("\nimport torch")
            name_file.write("\nimport torch.nn as nn")
            name_file.write("\nimport torch.nn.functional as F")
            name_file.write("\nfrom torch.autograd import Variable")
            name_file.write("\nimport numpy as np")

            if tool_specific == 'te':
                name_file.write("\nfrom network.wrappers.NetworkBase import NetworkBase\n\n\n")
            
          #  name_file.write("\ndef read_weights (file_name_weights):")
           # name_file.write("\n     return np.load(file_name_weights)")
                        
          #  name_file.write("\ndef read_bias (file_name_bias):")
          #  name_file.write("\n     return np.load(file_name_bias)\n\n")
          
            name_file.write("\ndef read_parameters (file_name_parameters):")
            name_file.write("\n     return np.load(file_name_parameters)\n\n")


            if tool_specific == 'te':
                name_file.write("\nclass Net(NetworkBase, nn.Module):")
                name_file.write("\n    def __init__(self, network_type, loss, accuracy, lr, training, trainable_layers=None, num_filters=16, optimizer='adam', nonlin='elu', num_classes=2, dropout=0.25):")
                name_file.write("\n        NetworkBase.__init__(self, network_type, loss, accuracy, lr, training, trainable_layers, num_filters,optimizer, nonlin, num_classes, dropout)")
                name_file.write("\n        nn.Module.__init__(self)\n")
           #     name_file.write("\n        self.weights = read_weights("'"%s"'")"%file_name_weights)
           #     name_file.write("\n        self.bias = read_bias("'"%s"'")\n\n"%file_name_bias)
                
            elif tool_specific == 'se':
                name_file.write("\nclass Net(nn.Module):")
                name_file.write("\n    def __init__(self):")
                name_file.write("\n        super(Net, self).__init__()\n")
              #  name_file.write("\n        self.weights = read_weights("'"%s"'")"%"parameters.npz")
              #  name_file.write("\n        self.bias = read_bias("'"%s"'")\n\n"%"parameters.npz")
                
            elif tool_specific == None:
                name_file.write("\nclass Net(nn.Module):")
                name_file.write("\n    def __init__(self):")
                name_file.write("\n        super(Net, self).__init__()\n")
                name_file.write("\n        if not hasattr(self, 'device'):\n")
                name_file.write("\n          self.device = 'cpu'\n")
              #  name_file.write("\n        self.weights = read_weights("'"%s"'")"%file_name_weights)
              #  name_file.write("\n        self.bias = read_bias("'"%s"'")\n\n"%file_name_bias)

            convoperator=0
            totconvoperator=0
            gemmoperator=0
            totgemmoperator=0
            for layer in self.net:
                if layer.operation == 'Conv':
                      totconvoperator+=1
                elif layer.operation == 'Gemm':
                      totgemmoperator+=1
                      
            for layer in self.net:                
                if layer.to_be_initialized:
                    if layer.operation == 'Conv':
                      convoperator+=1
                      layer.conv_count=convoperator
                      layer.conv_net_number = totconvoperator
                    elif layer.operation == 'Gemm':
                      gemmoperator+=1
                      layer.gemm_count=gemmoperator
                      layer.gemm_net_number = totgemmoperator
                    name_file.write( layer.get_initialization_string())
            name_file.write("\n\n")

            # Se l'utente ha inseriro dei valori per il numero dei canali, la dimensione dell'input e il numero di classi in uscital'inizializzazione dei pesi e dei bias
            # non viene generata sul file Net.py, in questa maniera verrà utilizzata l'inizializzazione automatica dei pesi e dei bias da parte di pytorch.
          #  if self.user_parameters == False:


            if self.load_params:
              name_file.write("\n\n        self.params = read_parameters("'"%s"'")\n\n"%file_name_parameters)
            
            ## generate the inizialization code for Conv and Gemm layer weights ##
            for layer in self.net:
              name_file.write( layer.get_parameters_loading_string())
            name_file.write("\n\n")

#            ## generate the inizialization code for Conv and Gemm layer bias ##
#            for layer in self.net:
#                if layer.to_be_initialized and ((layer.operation == "Conv" or layer.operation == "Gemm") and len(layer.input_) > 2):
#                    name_file.write( layer.get_initialization_bias())

#            ## generate the inizialization code for Batchnormalization layer weights ##
#            for layer in self.net:
#                if layer.to_be_initialized and layer.operation == "BatchNormalization":
#                    s = layer.get_initialization_batchnormalization()
#                    name_file.write( s )
           # else:
           #     pass

#---------- individua l'input della rete-----------------------------------------------------------
            net_input = self.init.net_input
#---------- stampo il metodo forward ()--------------------------------------------------------------------------------------
            name_file.write("\n\n    def forward(self, {}): \n".format(net_input))
            name_file.write("        #print ("'" input shape:"'",{}.shape)\n".format(net_input))

        #    name_file.write("\n\n        weights = read_weights("'"parameters.npz"'")\n\n")

            for layer in self.net:
                
                name_file.write(layer.get_forward_string())

#----------- individua l'output che la rete deve resitituire ----------------------------------
            forward_output = self.net[-1].output_[0]
            if self.softmax_layer == True:
                name_file.write("        print ("'" output shape:"'",{}.shape)\n".format(forward_output))
            else :
                pass
                
            name_file.write("        return ({})\n".format(forward_output))



#  ████████╗███████╗███████╗████████╗    ███████╗ ██████╗██████╗ ██╗██████╗ ████████╗
#  ╚══██╔══╝██╔════╝██╔════╝╚══██╔══╝    ██╔════╝██╔════╝██╔══██╗██║██╔══██╗╚══██╔══╝
#     ██║   █████╗  ███████╗   ██║       ███████╗██║     ██████╔╝██║██████╔╝   ██║   
#     ██║   ██╔══╝  ╚════██║   ██║       ╚════██║██║     ██╔══██╗██║██╔═══╝    ██║   
#     ██║   ███████╗███████║   ██║       ███████║╚██████╗██║  ██║██║██║        ██║   
#     ╚═╝   ╚══════╝╚══════╝   ╚═╝       ╚══════╝ ╚═════╝╚═╝  ╚═╝╚═╝╚═╝        ╚═╝   
#                                                                                    

     
#-------------------------------------------------------------------------------------------------------------------------------------------------------------
        def print_test_net(self):
                import os

                out_file = open(os.path.join(self.path, "test_net.py"), "w")
                out_file.write ("#----This code is auto-generated.----#")
                out_file.write("\n\nimport Net")
                out_file.write("\nimport torch")
                out_file.write("\nimport sys")
                out_file.write("\nfrom torch.autograd import Variable")
                out_file.write("\nimport h5py")
                out_file.write("\nimport json")
                out_file.write("\nimport numpy as np")
                out_file.write("\nfrom tqdm import tqdm")

                out_file.write("\nfrom te_metrics import *")
                out_file.write("\nfrom te_BatchIterator import *")
                out_file.write("\nfrom rpi_train import *")
                out_file.write("\n\nnet = Net.Net()")
                out_file.write("\nprint (net)")
                out_file.write("\n## Freezing all layers")
                out_file.write("\nfor params in net.parameters():")
                out_file.write("\n params.requires_grad = False")
                out_file.write("\n")
                out_file.write("\n# Create random Tensors to hold inputs and outputs")
                out_file.write("\nx = torch.randn(1, *{})".format(self.input_size))#channels, net_input_height, net_input_width ))
                out_file.write("\nprint("'"starting test...."'")")
                
                
                out_file.write("\nif len (sys.argv)==2:")
                out_file.write("\n  dataset = str(sys.argv[1])")
                out_file.write("\nelse:")
                out_file.write("\n  dataset = './dataset.hdf5'")
                
                
                
                out_file.write("\nregime={}")
                out_file.write("\nregime['batch_size'] = 1")
                
                out_file.write("\nargs={}")
                out_file.write("\nargs['loss'] = 'dice'")
                out_file.write("\nargs['task'] = 'segmentation'")
                out_file.write("\nargs['output_folder'] = '.'")
                out_file.write("\nargs['num_classes'] = 1")
                out_file.write("\nargs['use_cuda'] = torch.cuda.is_available()")
                out_file.write("\nargs['device'] = 'cuda' if args['use_cuda'] else 'cpu'")
                out_file.write("\n")
                out_file.write("\n")
                out_file.write("\ncriterion_marginalize = False")
                out_file.write("\naccuracy_fn = None")

                out_file.write("\nif args['loss'] == 'margin':")
                out_file.write("\n    criterion_marginalize = True")
                out_file.write("\n    criterion = nn.SoftMarginLoss()")
                out_file.write("\nelif args['loss'] == 'sigmoid':")
                out_file.write("\n    criterion_marginalize = True")
                out_file.write("\n    criterion = nn.BCEWithLogitsLoss()")
                out_file.write("\nelif args['loss'] == 'ce_loss':")
                out_file.write("\n    criterion_marginalize = True")
                out_file.write("\n    criterion = ce_loss")
                out_file.write("\n    accuracy_fn = percentage")
                out_file.write("\nelif args['loss'] == 'dice':")
                out_file.write("\n    criterion = DiceLoss()")
                out_file.write("\n    accuracy_fn = dice_Coefficient")
                out_file.write("\n")
                out_file.write("\nelif args['loss'] == 'detection_loss':")
                out_file.write("\n    criterion = detection_loss")
                out_file.write("\n    def accuracy_fn(y_output, y_true, **kwargs):")
                out_file.write("\n        y_pred = predictions(y_output, anchors=args['anchors'], image_size=args['input_shape'][1], conf_threshold=args['conf_thres'], nms_threshold=args['nms_thres'])")
                out_file.write("\n        return mAP_metric(y_pred, y_true, iouThreshold=0.4)")
                out_file.write("\n")
                out_file.write("\nelse:")
                out_file.write("\n    print ('loss not implemented: ',args['loss'])")
                out_file.write("\n    raise Exception")
                
                
                out_file.write("\n\nnet.to(args['device'])")
                
                
                out_file.write("\n\n\nslice_split = None")
                out_file.write("\ndataset_h5  = None")
                out_file.write("\nif dataset.endswith('.hdf5'):")
                out_file.write("\n  print('dataset: {}'.format(dataset))")
                out_file.write("\n  dataset_h5 = h5py.File(dataset, 'r')")
                out_file.write("\n  ")
                out_file.write("\n  try:")
                out_file.write("\n    with open(path_slice_split) as f:")
                out_file.write("\n      slice_split = json.load(f)['train_valid_split']")
                out_file.write("\n  except:")
                out_file.write("\n    print ('[WARNING]: train/valid split not found, using random split 0.7 train 0.3 valid')")
                out_file.write("\n    import random")
                out_file.write("\n    ind = random.sample([i for i in range(len(dataset_h5['X_data']))], len(dataset_h5['X_data']))      ")
                out_file.write("\n    ")
                out_file.write("\n    slice_split = [ind[0:round(0.7*len(dataset_h5['X_data']))],ind[round(0.7*len(dataset_h5['X_data']))+1:]]")

                out_file.write("\n  path_preprocessing_pipeline_instance = os.path.join(os.path.split(dataset)[0],'preprocessing_pipeline.json')")
  
      
                out_file.write("\nelse:")
                out_file.write("\n  print('folder: {}'.format(dataset))")
                out_file.write("\n  path_preprocessing_pipeline_instance = os.path.join(os.path.split(dataset)[0],'..','preprocessing_pipeline.json')")
                
                
                out_file.write("\nwith open(path_preprocessing_pipeline_instance) as f:")
                out_file.write("\n  pre_pipeline_config = json.load(f)")
                
                
                out_file.write("\ntry:")
                
                out_file.write("\n  dataset_h5=None")
                out_file.write("\n  if dataset_h5:")
                out_file.write("\n    dataset_h5 = h5py.File(dataset, 'r')")
                
                out_file.write("\n  criterion_marginalize = True")
                out_file.write("\n  criterion = nn.CrossEntropyLoss()")
                out_file.write("\n  train_func, validate_func, test_func = rpi_func_build(")
                out_file.write("\n                                 data_split   = slice_split,")
                out_file.write("\n                                 dataset_h5   = dataset_h5,")
                out_file.write("\n                                 tbx_writer   = None,")
                out_file.write("\n                                 tbx_beholder = None,")
                out_file.write("\n                                 use_cuda     = args['use_cuda'],")
                out_file.write("\n                                 optimizer    = None,")
                out_file.write("\n                                 criterion    = criterion,")
                out_file.write("\n                                 accuracy     = accuracy_fn,")
                out_file.write("\n                                 task         = args['task'],")
                out_file.write("\n                                 lq_quant     = False,")
                out_file.write("\n                                 transpose    = True,")
                out_file.write("\n                                 marginalize  = criterion_marginalize,")
                out_file.write("\n                                 preprocessing_pipeline_instance = pre_pipeline_config,")
                out_file.write("\n                                 regime       = regime,")
                out_file.write("\n                                 verbose      = False,")
                out_file.write("\n                                 num_classes  = args['num_classes'])")
                
                out_file.write("\n")
                out_file.write("\n  if dataset_h5:")
                out_file.write("\n    acc = validate_func(net, 0, criterion=nn.CrossEntropyLoss())")
                out_file.write("\n    print ('validation %.2f%%' % (100*acc.item()))")
                out_file.write("\n  else:")
                out_file.write("\n    test_func(net, 0, folder=dataset)")
                
                out_file.write("\n  print("'"test completed."'")")
                out_file.write("\nexcept:")
                out_file.write("\n  print('Error in validation')")
                
                out_file.write("\nprint('Testing inference...')")
                out_file.write("\nout = net(x)")
                out_file.write("\nprint('Done...')")
                
                out_file.write("\nprint('Testing export...')")
                out_file.write("\ntry:")
                out_file.write("\n  dummy_input = Variable(torch.randn(1, *{}))".format(self.input_size))#channels, net_input_height, net_input_width))
                out_file.write("\n  torch.onnx.export(net, dummy_input, './test.onnx', verbose=False)")
                out_file.write("\n  print('done')")
                out_file.write("\nexcept:")
                out_file.write("\n  print('unable to export onnx')")

                
                
                out_file.write("\n")
                out_file.write("\n'''")
                out_file.write("\ny = torch.randn(1, 1000)")
                out_file.write("\n# in this case we will use Mean Squared Error (MSE) as our loss function.")
                out_file.write("\nloss_fn = torch.nn.MSELoss(reduction='sum')")
                out_file.write("\nnet = Net()")
                out_file.write("\n   learning_rate = 1e-7")
                out_file.write("\nfor t in range(500):")
                out_file.write("\n    # Forward pass: compute predicted y by passing x to the model. Module objects")
                out_file.write("\n    # override the __call__ operator so you can call them like functions. When")
                out_file.write("\n    # doing so you pass a Tensor of input data to the Module and it produces")
                out_file.write("\n    # a Tensor of output data.")
                out_file.write("\n    y_pred = net(x)")
                out_file.write("\n    # Compute and print loss. We pass Tensors containing the predicted and true")
                out_file.write("\n    # values of y, and the loss function returns a Tensor containing the loss")
                out_file.write("\n    loss = loss_fn(y_pred, y)")
                out_file.write("\n    print(t, loss.item())")
                out_file.write("\n    # Zero the gradients before running the backward pass.")
                out_file.write("\n    print("'"Zero the gradients before running the backward pass."'")")
                out_file.write("\n    net.zero_grad()")
                out_file.write("\n    # Backward pass: compute gradient of the loss with respect to all the learnable")
                out_file.write("\n    # parameters of the model. Internally, the parameters of each Module are stored")
                out_file.write("\n    # in Tensors with requires_grad=True, so this call will compute gradients for")
                out_file.write("\n    # all learnable parameters in the model.")
                out_file.write("\n    print ("'"Backward pass...."'")")
                out_file.write("\n    loss.backward()")
                out_file.write("\n    print ("'"Backward pass complete"'")")
                out_file.write("\n    # Update the weights using gradient descent. Each parameter is a Tensor, so")
                out_file.write("\n    # we can access its gradients like we did before.")
                out_file.write("\n    print ("'"updating the weights using gradient descent....."'")")
                out_file.write("\n    with torch.no_grad():")
                out_file.write("\n        for param in net.parameters():")
                out_file.write("\n            param -= learning_rate * param.grad")
                out_file.write("\n    print ("'"weights update complete"'")")
                out_file.write("\n")
                out_file.write("\n'''")
     

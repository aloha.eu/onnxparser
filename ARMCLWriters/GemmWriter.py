
# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.


import sys, math, os, struct
import numpy as np
from .ARMCLWriter import ARMCLWriter


class GemmWriter(ARMCLWriter):
        def __init__ (self,  reader):

          super().__init__(reader)
        

            
#  ███████╗ █████╗ ██╗   ██╗███████╗    ██████╗  █████╗ ██████╗  █████╗ ███╗   ███╗
#  ██╔════╝██╔══██╗██║   ██║██╔════╝    ██╔══██╗██╔══██╗██╔══██╗██╔══██╗████╗ ████║
#  ███████╗███████║██║   ██║█████╗      ██████╔╝███████║██████╔╝███████║██╔████╔██║
#  ╚════██║██╔══██║╚██╗ ██╔╝██╔══╝      ██╔═══╝ ██╔══██║██╔══██╗██╔══██║██║╚██╔╝██║
#  ███████║██║  ██║ ╚████╔╝ ███████╗    ██║     ██║  ██║██║  ██║██║  ██║██║ ╚═╝ ██║
#  ╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚══════╝    ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝

        def save_parameters(self, path):

          print ("\n"+self.reader.name)
          
          filename = os.path.join(path,self.reader.name + "_w.npy")
          np.save(filename, np.array(self.reader.weights).astype(np.float32))
          
          filename =os.path.join(path, self.reader.name + "_b.npy")
          if self.reader.biases is not None: # check if biases exsist
            biases = self.reader.biases
          else:
            biases = np.zeros(self.reader.osizes[0])
          np.save(filename, np.array(biases).astype(np.float32))
          
            
         
                  
           

#  ██████╗ ███████╗ ██████╗██╗      █████╗ ██████╗ ███████╗
#  ██╔══██╗██╔════╝██╔════╝██║     ██╔══██╗██╔══██╗██╔════╝
#  ██║  ██║█████╗  ██║     ██║     ███████║██████╔╝█████╗  
#  ██║  ██║██╔══╝  ██║     ██║     ██╔══██║██╔══██╗██╔══╝  
#  ██████╔╝███████╗╚██████╗███████╗██║  ██║██║  ██║███████╗
#  ╚═════╝ ╚══════╝ ╚═════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝
#                                                          

#---------------------------------------------------------------------------------------------------------------#
        def get_declaration_string(self):
            string = ''
           
            if self.reader.operation == "Gemm":
                string = "\nLINEAR {}_param;\n".format(self.reader.name)
            return string
            
#---------------------------------------------------------------------------------------------------------------#

#  ██╗    ██╗██╗██████╗ ██╗███╗   ██╗ ██████╗ 
#  ██║    ██║██║██╔══██╗██║████╗  ██║██╔════╝ 
#  ██║ █╗ ██║██║██████╔╝██║██╔██╗ ██║██║  ███╗
#  ██║███╗██║██║██╔══██╗██║██║╚██╗██║██║   ██║
#  ╚███╔███╔╝██║██║  ██║██║██║ ╚████║╚██████╔╝
#   ╚══╝╚══╝ ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝ ╚═════╝ 
#                                             

        
#inherited

#  ██╗    ██╗███████╗██╗    ██╗      ██████╗  █████╗ ██████╗ 
#  ██║    ██║██╔════╝██║    ██║     ██╔═══██╗██╔══██╗██╔══██╗
#  ██║ █╗ ██║█████╗  ██║    ██║     ██║   ██║███████║██║  ██║
#  ██║███╗██║██╔══╝  ██║    ██║     ██║   ██║██╔══██║██║  ██║
#  ╚███╔███╔╝███████╗██║    ███████╗╚██████╔╝██║  ██║██████╔╝
#   ╚══╝╚══╝ ╚══════╝╚═╝    ╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ 
#                                                            

        def get_weights_loading_string(self):
          string = ''
        
          if self.reader.operation == "Gemm":
            
            size = self.reader.isizes[0] * self.reader.osizes[0]
            string += '%s_param->weights = (DATA*)malloc(%d * sizeof(DATA));\n' % (self.reader.name, size)
            string += 'sprintf(filename, "%%s/%s_weights.bin", load_data_dir);\n' % (self.reader.name)
            string += 'load_fixed(filename, %d, %s_param->weights);\n' % (size, self.reader.name)
            size  = self.reader.osizes[0]
            string += '%s_param->bias = (DATA*)malloc(%d * sizeof(DATA));\n' % (self.reader.name, size)
            string += 'sprintf(filename, "%%s/%s_biases.bin", load_data_dir);\n' % (self.reader.name)
            string += 'load_fixed(filename,%d,%s_param->bias);\n' % (size, self.reader.name)
            
            
            
            
            
          
          
          string += "\n"
          return string


#  ██╗███╗   ██╗██╗████████╗
#  ██║████╗  ██║██║╚══██╔══╝
#  ██║██╔██╗ ██║██║   ██║   
#  ██║██║╚██╗██║██║   ██║   
#  ██║██║ ╚████║██║   ██║   
#  ╚═╝╚═╝  ╚═══╝╚═╝   ╚═╝   
#                           


        def get_initialization_string(self):
            
            if self.reader.operation == "Gemm":
                s = "\n\n// #########################################\n"
                s += "//        FULLY CONNECTED LAYER init *\n"
                s += "//##########################################\n\n"
                name = self.reader.nodes_input()[0]
                
                s += "{}_param = linear_create();\n"                                   .format (self.reader.name)
                


                if self.reader.prev_layers[0].operation != "Reshape" and self.reader.prev_layers[0].operation != "Gemm": # devo inserire una condizione che faccia si che la riga s14 venga generata solo nel caso in cui la gemm sia la prima della rete


                    s+= "{}_param->in_s = {};\n"               .format (self.reader.name, self.reader.isizes[0])
                elif self.reader.prev_layers[0].operation != "Gemm":
            
                    s+= "SIZE {}_dim = {};\n"                     .format (self.reader.input_[0], self.reader.initializer.layer_input_shape[name[1]].dim_value)
                   
                else :
                    s += ""
                
                
                s += "{}_param->out_s = {};\n"                                     .format (self.reader.name, self.reader.osizes[0])#initializer.layer_input_shape[name[1]].dim_value)
                s += "{}_param->in_s = {};\n"                                      .format (self.reader.name, self.reader.isizes[0])
                
                s+= " {}_param->qf = {};\n"                                        .format (self.reader.name, self.reader.qf)

            return s

#  ███████╗ ██████╗ ██████╗ ██╗    ██╗ █████╗ ██████╗ ██████╗ 
#  ██╔════╝██╔═══██╗██╔══██╗██║    ██║██╔══██╗██╔══██╗██╔══██╗
#  █████╗  ██║   ██║██████╔╝██║ █╗ ██║███████║██████╔╝██║  ██║
#  ██╔══╝  ██║   ██║██╔══██╗██║███╗██║██╔══██║██╔══██╗██║  ██║
#  ██║     ╚██████╔╝██║  ██║╚███╔███╔╝██║  ██║██║  ██║██████╔╝
#  ╚═╝      ╚═════╝ ╚═╝  ╚═╝ ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ 
#                                                             


#---------------------------------------------------------------------------------------------------------------#
        def get_forward_string(self):
                
                s  ='              << FullyConnectedLayer(\n'
                s +='                  {}U,\n'.format(self.osizes[0])
                s +='                  get_weights_accessor(data_path, "{}_w.npy", weights_layout),\n'.format(self.name)
                s +='                  get_weights_accessor(data_path, "{}_b.npy"))\n'.format(self.name)
                s +='              .set_name("{}")\n'.format(self.name)

                return s

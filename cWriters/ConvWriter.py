
# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.

import sys, math, os, struct
import numpy as np
from .CWriter import CWriter


class ConvWriter(CWriter):
        def __init__ (self, reader):

          super().__init__(reader)
          
          try: 
            self.RPI = reader.RPI
          except AttributeError:
            self.RPI= False
          if self.RPI:
            self.rpi_string = "_rpi"
          else:
            self.rpi_string = ""
            
          if reader.datatype == 'f':
            self.rpi_string = self.rpi_string+'_f'


#  ███████╗ █████╗ ██╗   ██╗███████╗    ██████╗  █████╗ ██████╗  █████╗ ███╗   ███╗
#  ██╔════╝██╔══██╗██║   ██║██╔════╝    ██╔══██╗██╔══██╗██╔══██╗██╔══██╗████╗ ████║
#  ███████╗███████║██║   ██║█████╗      ██████╔╝███████║██████╔╝███████║██╔████╔██║
#  ╚════██║██╔══██║╚██╗ ██╔╝██╔══╝      ██╔═══╝ ██╔══██║██╔══██╗██╔══██║██║╚██╔╝██║
#  ███████║██║  ██║ ╚████╔╝ ███████╗    ██║     ██║  ██║██║  ██║██║  ██║██║ ╚═╝ ██║
#  ╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚══════╝    ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝

        def save_parameters(self, path):
          from tqdm import tqdm
          print ("\n"+self.reader.name)
          if self.reader.operation == "Gemm" or self.reader.operation == "Conv":
            if self.reader.weights is not None:
              filename = self.reader.name + "_weights.bin"
              with open(os.path.join(path, filename), "wb") as f:
                
                for wb in tqdm(self.reader.weights.flatten(), bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                 # print (wb)
                 # print (self.reader.convert_float_to_short(wb,'h',self.reader.qf))
                  w_byte=struct.pack("h",self.reader.convert_float_to_short(wb,'h',self.reader.qf, layer_name = self.reader.name))
                    
                  f.write(w_byte)
             
              filename = self.reader.name + "_biases.bin"
              if self.reader.biases is not None: # check if biases exsist
                biases = self.reader.biases
              else:
                biases = np.zeros(self.reader.osizes[0])
              with open(os.path.join(path, filename), "wb") as f:
                for wb in tqdm(biases, bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                  b_byte=struct.pack("h",self.reader.convert_float_to_short(wb,'h',self.reader.qf, layer_name = self.reader.name))
                  f.write(b_byte)
            
          
        def get_weights(self):
          if len(self.reader.input_)>1:
            if self.reader.operation == "BatchNormalization":
              if self.reader.input_[1] in self.reader.initializer.parameters.keys():
                return self.reader.initializer.parameters[self.reader.input_[1]]
              else:
                return None
            else:
              if self.reader.input_[1] in self.reader.initializer.parameters.keys():
                return self.reader.initializer.parameters[self.reader.input_[1]]
              else:
                return None
          else:
            return None
        
        def get_biases(self):
          if len(self.reader.input_)>2:
            if self.reader.operation == "BatchNormalization":
              if self.reader.input_[2] in self.reader.initializer.parameters.keys():
                return self.reader.initializer.parameters[self.reader.input_[2]]
              else:
                return None
            
            else:
              if self.reader.input_[2] in self.reader.initializer.parameters.keys():
                return self.reader.initializer.parameters[self.reader.input_[2]]
              else:
                return None
          else:
            return np.zeros(self.reader.osizes[0])
      
 
#---------------------------------------------------------------------------------------------------------------#
        def layers_stride (self):
            node = self.reader.node
            if node.op_type == "Conv" or  node.op_type == "MaxPool" or node.op_type =="AveragePool":
                       lenght = (len(node.attribute))
                       i = 0
                       for attr in node.attribute:
                               if (i < lenght) :
                                       if node.attribute[i].name == "strides":
                                               stride =node.attribute[i].ints[0]
                                               break
                                       else : i = i+1
            else:
                stride = (None)
            return stride


#---------------------------------------------------------------------------------------------------------------#
        def layers_dilation (self):
            node = self.reader.node
            no_dilations = True
            if node.op_type == "Conv" :
                no_dilations = True
                for attr in node.attribute:
                    if attr.name == "dilations":
                        dilations = attr.ints[0]
                        no_dilations = False
            if no_dilations == True :
                dilations = 1   # default vale for group in Conv layers
            return dilations
        
#---------------------------------------------------------------------------------------------------------------#
        def layers_padding (self):
            node = self.reader.node
            padding = [0,0,0,0]
            if node.op_type == "Conv" or  node.op_type == "MaxPool" or node.op_type =="AveragePool":
                for attr in node.attribute:
                    if (attr.name == "pads"):
                       
#                        padding_x = 0
#                        padding_y = 0
#                        padding_x_s = attr.ints[0]
#                        padding_x_e = attr.ints[2]
#                        padding_y_s = attr.ints[1]
#                        padding_y_e = attr.ints[3]
#                        if padding_x_s !=0:
#                            padding_x = padding_x_s
#                        elif padding_x_e !=0:
#                            padding_x = padding_x_e
#                        if padding_y_s != 0:
#                            padding_y = padding_y_s
#                        elif padding_y_e !=0:
#                            padding_y = padding_y_e
#                        padding[0] = padding_x
#                        padding[1] = padding_y
                        
                        padding[0]=attr.ints[0]
                        padding[1]=attr.ints[1]
                        padding[2]=attr.ints[2]
                        padding[3]=attr.ints[3]
                        
                    elif (attr.name == "auto_pad" ):
                        pad = attr.s
                        pad =pad.decode("utf-8")
                        if pad == "VALID":
                            padding =[0,0,0,0]
                        elif pad == "NOTSET":
                            break
                        else: padding = pad
            else :
                   padding[0] = 0
                   padding[1] = 0
                   padding[2] = 0
                   padding[3] = 0
                   
            if padding == 'SAME_UPPER':   # output input feature should have same dimensions of input feature
                raise NotImplementedError()
#                kernel = self.reader.kernel
#                padding =[0,0,0,0]
#                pad = (kernel-1)/2
#                pad = int(pad)
#                padding[0] = pad
#                padding[1] = pad
#                padding[2] = pad
#                padding[3] = pad
#                if node.op_type == "MaxPool": #FIXME: this works only with few cases
#                  padding =[0,0,0,0]
#                  padding[0] = (kernel-self.reader.stride)%2 #x begin
#                  padding[1] = pad #x end
#                  padding[2] = (kernel-self.reader.stride)%2 #y begin
#                  padding[3] = pad #y end
                
            return padding
        
#---------------------------------------------------------------------------------------------------------------#
        def concat_axis (self):
            node = self.reader.node
            i = 0
            if  node.op_type == "Concat":
                    for attr in node.attribute:
                            if node.attribute[i].name == "axis":
                                    param = node.attribute[i].i
                                    break
                            else:  i = i+1
            else : param = None
            return param

#---------------------------------------------------------------------------------------------------------------#
        def find_shape(self):
            global prev_shape
            node = self.reader.node
            if node.op_type == "Reshape":
                bias_and_nodes = self.reader.initializer.parameters
                weights_and_nodes = self.reader.initializer.parameters
                input_1 = self.reader.input_[1]
                shape =  bias_and_nodes[input_1]
                prev_shape = shape

                if (shape[0] == 0 and shape[1] ==-1):
                    shape_2=True
                else: shape_2=False
                if self.reader.input_[0]  in weights_and_nodes:
                    input_ = self.reader.input_[0]  # l'input va cercato tra gli initializer
                else: input_ = None
            else :
                shape = None
                input_= None
                shape_2 = False
            return shape , input_, shape_2


#-------------------------------------------------------------------------------------------------------------#
        def conv_output_dim2 ( self):
            node = self.reader.node

            if node.op_type =="Conv" or node.op_type =="Gemm":
                k=self.reader.input_[1]
                
                if k in self.reader.initializer.parameters.keys():
                       
                        output_channels_n = self.reader.initializer.parameters[k].shape[0]
                        output_channels_n_gemm = 0
                self.reader.output_channels_prev_layer_ = output_channels_n
            else:
                output_channels_n = self.reader.isizes[0]
                output_channels_n_gemm = None
            return output_channels_n , output_channels_n_gemm

        


#---------------------------------------------------------------------------------------------------------------#
        def constant (self):
            node = self.reader.node
            if node.op_type == "Constant":

                for attribute in node.attribute:
                    if attribute.name == "value":
                        value = attribute.t.raw_data
                    else:
                        print ("Constant attribute not supported")
            else: value = None
            return value


#-----------------------------------------------------------------
        def pad_parameters(self):
            node = self.reader.node
            if node.op_type == "Pad":
                for attribute in node.attribute:
                    if attribute.name == "mode":
                        mode = attribute.s
                        mode = mode.decode("utf-8")
                    elif attribute.name == "pads":
                        pads = attribute.ints[0]
                    elif attribute.name == "value":
                        value = attribute.f
            else :
                mode = None
                pads = None
                value = None
            return mode, pads, value
            

#  ██████╗ ███████╗ ██████╗██╗      █████╗ ██████╗ ███████╗
#  ██╔══██╗██╔════╝██╔════╝██║     ██╔══██╗██╔══██╗██╔════╝
#  ██║  ██║█████╗  ██║     ██║     ███████║██████╔╝█████╗  
#  ██║  ██║██╔══╝  ██║     ██║     ██╔══██║██╔══██╗██╔══╝  
#  ██████╔╝███████╗╚██████╗███████╗██║  ██║██║  ██║███████╗
#  ╚═════╝ ╚══════╝ ╚═════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝
#                                                          

#---------------------------------------------------------------------------------------------------------------#
        def get_declaration_string(self):
            string = ''
            rpi = ""
            if self.RPI:
              rpi = "_RPI"

            string = "\nSPATCONV{} {}_param; " .format(self.rpi_string.upper(), self.reader.name)
                
           
            return string
            
        
       
#---------------------------------------------------------------------------------------------------------------#

#  ██╗    ██╗██╗██████╗ ██╗███╗   ██╗ ██████╗ 
#  ██║    ██║██║██╔══██╗██║████╗  ██║██╔════╝ 
#  ██║ █╗ ██║██║██████╔╝██║██╔██╗ ██║██║  ███╗
#  ██║███╗██║██║██╔══██╗██║██║╚██╗██║██║   ██║
#  ╚███╔███╔╝██║██║  ██║██║██║ ╚████║╚██████╔╝
#   ╚══╝╚══╝ ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝ ╚═════╝ 
#                                             


#inherited
        

#  ██╗    ██╗███████╗██╗    ██╗      ██████╗  █████╗ ██████╗ 
#  ██║    ██║██╔════╝██║    ██║     ██╔═══██╗██╔══██╗██╔══██╗
#  ██║ █╗ ██║█████╗  ██║    ██║     ██║   ██║███████║██║  ██║
#  ██║███╗██║██╔══╝  ██║    ██║     ██║   ██║██╔══██║██║  ██║
#  ╚███╔███╔╝███████╗██║    ███████╗╚██████╔╝██║  ██║██████╔╝
#   ╚══╝╚══╝ ╚══════╝╚═╝    ╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ 
#                                                            

        def get_weights_loading_string(self):
          string = ''
        
          if self.reader.operation == "Conv":
            
              wsize = np.prod(self.reader.kernel) * self.reader.isizes[0] * self.reader.osizes[0]/self.reader.group
              string += 'sprintf(filename, "%%s/SW/%s_weights.bin", load_data_dir);\n'%(self.reader.name)
              string += '%s_param -> kernel = (DATA*)malloc(%d * sizeof(DATA));\n' % (self.reader.name, wsize)
              string += 'load_fixed(filename, %d, %s_param -> kernel);\n'%(wsize,self.reader.name)
              size = self.reader.osizes[0]
              string += 'sprintf(filename, "%%s/SW/%s_biases.bin", load_data_dir);\n'%(self.reader.name)
              string += '%s_param -> bias = (DATA*)malloc(%d * sizeof(DATA));\n' % (self.reader.name, size)
              string += 'load_fixed(filename, %d, %s_param -> bias);\n'%(size,self.reader.name)
             # string += 'wPointer+=%d;\n\n'%wsize
            
          
          
          
          string += "\n"
          return string


#  ██╗███╗   ██╗██╗████████╗
#  ██║████╗  ██║██║╚══██╔══╝
#  ██║██╔██╗ ██║██║   ██║   
#  ██║██║╚██╗██║██║   ██║   
#  ██║██║ ╚████║██║   ██║   
#  ╚═╝╚═╝  ╚═══╝╚═╝   ╚═╝   
#                           


        def get_initialization_string(self):
          
                 
          dt_rpi = "float"
          
          s ="\n\n//######################################\n"
          s+= "//            CONVOLUTIONAL LAYER init  \n"
          s+="// ######################################\n"
          s+= "\n\n{}_param = spatconv{}_create();\n"                                 .format (self.reader.name, self.rpi_string)
                    
          out_dim = self.reader.osizes[0]
          ker     = self.reader.kernel
          in_dim  = self.reader.isizes[0]
          iW      = self.reader.isizes[2]
          iH      = self.reader.isizes[1]
          oW      = self.reader.osizes[2]
          oH      = self.reader.osizes[1]
          pad0    = self.reader.padding[0]
          pad1    = self.reader.padding[1]
          Stride  = self.reader.stride
             
          s+= "{}_param->pout ={};\n"                                               .format (self.reader.name ,out_dim)
          s+= "{}_param->pin = {};\n\n"                                               .format ( self.reader.name, in_dim)
          s+= "{}_param->kern_s[0] = {};\n"                                         .format (self.reader.name, out_dim)
          s+= "{}_param->kern_s[1] = {};\n"                                         .format (self.reader.name, in_dim)
          s+= "{}_param->kern_s[2] = {};\n"                                         .format (self.reader.name, ker[0])
          s+= "{}_param->kern_s[3] = {};\n\n"                                       .format (self.reader.name, ker[1])
          
          s+= "{}_param->in_s[0] = {};\n"                                         .format (self.reader.name, in_dim)
          s+= "{}_param->in_s[1] = {};\n"                                         .format (self.reader.name, iH)
          s+= "{}_param->in_s[2] = {};\n\n"                                         .format (self.reader.name, iW)
          
          s+= "{}_param->out_s[0] = {};\n"                                       .format (self.reader.name, out_dim)
          s+= "{}_param->out_s[1] = {};\n"                                         .format (self.reader.name, oH)
          s+= "{}_param->out_s[2] = {};\n\n"                                         .format (self.reader.name, oW)
          
          s+= "{}_param->groups = {};\n\n"                                         .format (self.reader.name,self.reader.group)
          s+= "{}_param->qf = {};\n"                                         .format (self.reader.name, self.reader.qf) 
          s+= "{}_param->activate = {};\n"                                         .format (self.reader.name, 0)
          s+= "{}_param->precision8 = {};\n\n"                                         .format (self.reader.name, 0)

          
          s+= "{}_param->pad[0] = {};\n".format(self.reader.name,  pad0)
          s+= "{}_param->pad[1] = {};\n\n".format(self.reader.name,  pad1)
          s+= "{}_param->stride[0] = {};\n".format(self.reader.name,  Stride[0])
          s+= "{}_param->stride[1] = {};\n\n".format(self.reader.name,  Stride[1])
          
          s+= "{}_param->dil[0] = {};\n".format(self.reader.name,  1)
          s+= "{}_param->dil[1] = {};\n\n".format(self.reader.name,  1)
       
          s+= "{}_param->maxog= 4;\n\n\n"                                           .format (self.reader.name)
          
          if dt_rpi=="float":
            conversion=int
          else:
            conversion=float
          
#          import IPython
#          IPython.embed()
          if self.RPI:
              s+= "{}_param->scaler = {:0.9f};\n"                          .format (self.reader.name, self.reader.mulrelu_scaler[0])
              s+= "{}_param->divider = {};\n"                              .format (self.reader.name, conversion(self.reader.divrelu_divider[0])) 
              s+= "{}_param->clip_max = {};\n"                             .format (self.reader.name, conversion(self.reader.clip_max)) 
              s+= "{}_param->clip_min = {};\n"                             .format (self.reader.name, conversion(self.reader.clip_min)) 
              
              s+= "{}_param->scalers_bn = ({}*)malloc ({} *sizeof ({}));\n".format(self.reader.name, dt_rpi, len(self.reader.mulbn_scaler), dt_rpi)
              s+= "{}_param->biases_bn = ({}*)malloc ({} *sizeof ({}));\n".format(self.reader.name, dt_rpi, len(self.reader.addbn_bias), dt_rpi)
            
              s+= "{}_param->num = {};\n".format(self.reader.name, len(self.reader.mulbn_scaler))
              
              array=""
              for i, b in enumerate(self.reader.mulbn_scaler):
                array += str(conversion(b))
                if i%30==0 and i>0:
                  array += ",\n"
                else:
                  array += ", "
              array=array[0:-2]
              
              
              s+= "{} {}_scaler[{}] ={{{}}};\n".format(dt_rpi, self.reader.name, len(self.reader.mulbn_scaler), array)
              s+= "for (int n=0; n<{}_param->num;n++)\n".format(self.reader.name)
              s+= "  {}_param->scalers_bn[n] = {}_scaler[n];\n".format(self.reader.name,self.reader.name)
            
              
              array=""
              for i, b in enumerate(self.reader.addbn_bias):
                array += str(conversion(b))
                if i%30==0 and i>0:
                  array += ",\n"
                else:
                  array += ", "
              array=array[0:-2]
              
              s+= "{} {}_adder[{}] ={{{}}};\n".format(dt_rpi, self.reader.name, len(self.reader.addbn_bias),array)
              s+= "for (int n=0; n<{}_param->num;n++)\n".format(self.reader.name)
              s+= "  {}_param->biases_bn[n] = {}_adder[n];\n".format(self.reader.name,self.reader.name)
          
          
          return s


#  ███████╗ ██████╗ ██████╗ ██╗    ██╗ █████╗ ██████╗ ██████╗ 
#  ██╔════╝██╔═══██╗██╔══██╗██║    ██║██╔══██╗██╔══██╗██╔══██╗
#  █████╗  ██║   ██║██████╔╝██║ █╗ ██║███████║██████╔╝██║  ██║
#  ██╔══╝  ██║   ██║██╔══██╗██║███╗██║██╔══██║██╔══██╗██║  ██║
#  ██║     ╚██████╔╝██║  ██║╚███╔███╔╝██║  ██║██║  ██║██████╔╝
#  ╚═╝      ╚═════╝ ╚═╝  ╚═╝ ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ 
#                                                             


#---------------------------------------------------------------------------------------------------------------#
        def get_forward_string(self):
               
                 
                s=''

                s += "\n\n//################################################    Conv{} layer ###############################################\n".format(self.rpi_string)
                s += "//kernel {}x{}\n".format (self.reader.kernel[0], self.reader.kernel[1])
                
                s += "int {}_job_id =spatconv{}_forward_wrap ({}_param);\n".format(self.reader.name, self.rpi_string, self.reader.name)

               
                return s
                
                

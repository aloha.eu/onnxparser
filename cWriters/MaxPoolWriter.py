
# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.


import sys, math, os
import numpy as np
from .CWriter import CWriter



class MaxPoolWriter(CWriter):
        def __init__ (self,  reader):

          super().__init__(reader)
        
        
                
                  

#  ███████╗ █████╗ ██╗   ██╗███████╗    ██████╗  █████╗ ██████╗  █████╗ ███╗   ███╗
#  ██╔════╝██╔══██╗██║   ██║██╔════╝    ██╔══██╗██╔══██╗██╔══██╗██╔══██╗████╗ ████║
#  ███████╗███████║██║   ██║█████╗      ██████╔╝███████║██████╔╝███████║██╔████╔██║
#  ╚════██║██╔══██║╚██╗ ██╔╝██╔══╝      ██╔═══╝ ██╔══██║██╔══██╗██╔══██║██║╚██╔╝██║
#  ███████║██║  ██║ ╚████╔╝ ███████╗    ██║     ██║  ██║██║  ██║██║  ██║██║ ╚═╝ ██║
#  ╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚══════╝    ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝

        def save_parameters(self, path):
          pass

#  ██████╗ ███████╗ ██████╗██╗      █████╗ ██████╗ ███████╗
#  ██╔══██╗██╔════╝██╔════╝██║     ██╔══██╗██╔══██╗██╔════╝
#  ██║  ██║█████╗  ██║     ██║     ███████║██████╔╝█████╗  
#  ██║  ██║██╔══╝  ██║     ██║     ██╔══██║██╔══██╗██╔══╝  
#  ██████╔╝███████╗╚██████╗███████╗██║  ██║██║  ██║███████╗
#  ╚═════╝ ╚══════╝ ╚═════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝
#                                                          

#---------------------------------------------------------------------------------------------------------------#
        def get_declaration_string(self):
            string = ''
            if self.reader.operation == "MaxPool" or self.reader.operation== "AveragePool" or self.reader.operation == "GlobalAveragePool":
                string = "\nMAXPOOL {}_param; " .format(self.reader.name)
                
            return string
            
#---------------------------------------------------------------------------------------------------------------#

#  ██╗    ██╗██╗██████╗ ██╗███╗   ██╗ ██████╗ 
#  ██║    ██║██║██╔══██╗██║████╗  ██║██╔════╝ 
#  ██║ █╗ ██║██║██████╔╝██║██╔██╗ ██║██║  ███╗
#  ██║███╗██║██║██╔══██╗██║██║╚██╗██║██║   ██║
#  ╚███╔███╔╝██║██║  ██║██║██║ ╚████║╚██████╔╝
#   ╚══╝╚══╝ ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝ ╚═════╝ 
#                                             

      
#inherited
        

#  ██╗    ██╗███████╗██╗    ██╗      ██████╗  █████╗ ██████╗ 
#  ██║    ██║██╔════╝██║    ██║     ██╔═══██╗██╔══██╗██╔══██╗
#  ██║ █╗ ██║█████╗  ██║    ██║     ██║   ██║███████║██║  ██║
#  ██║███╗██║██╔══╝  ██║    ██║     ██║   ██║██╔══██║██║  ██║
#  ╚███╔███╔╝███████╗██║    ███████╗╚██████╔╝██║  ██║██████╔╝
#   ╚══╝╚══╝ ╚══════╝╚═╝    ╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ 
#                                                            

        def get_weights_loading_string(self):
          string = ''
        
          return string


#  ██╗███╗   ██╗██╗████████╗
#  ██║████╗  ██║██║╚══██╔══╝
#  ██║██╔██╗ ██║██║   ██║   
#  ██║██║╚██╗██║██║   ██║   
#  ██║██║ ╚████║██║   ██║   
#  ╚═╝╚═╝  ╚═══╝╚═╝   ╚═╝   
#                           


        def get_initialization_string(self):

            if self.reader.operation == "AveragePool":
            
                s = "\n\n// ###############################################################\n"
                s += "// #######             AVERAGE POOL LAYER init          #############\n"
                s += "// #############################################################\n\n"
            elif self.reader.operation == "GlobalAveragePool":
            
                s = "\n\n// ###############################################################\n"
                s += "// #######        GLOBAL AVERAGE POOL LAYER init        #############\n"
                s += "// #############################################################\n\n"
                s += "TO be implemented\n"
            else:
                s = "\n\n// ###############################################################\n"
                s += "// #######             MAXPOOL LAYER init          #############\n"
                s += "// #############################################################\n\n"
                
            s += "{}_param = maxpool_create();\n".format (self.reader.name)
            
            s+= "{}_param->in_s[0] = {};\n".format(self.reader.name,  self.reader.isizes[0])
            s+= "{}_param->in_s[1] = {};\n".format(self.reader.name,  self.reader.isizes[1])
            s+= "{}_param->in_s[2] = {};\n".format(self.reader.name,  self.reader.isizes[2])
            s+= "{}_param->out_s[0] = {};\n".format(self.reader.name, self.reader.isizes[0])
            s+= "{}_param->out_s[1] = {};\n".format(self.reader.name, self.reader.osizes[1])
            s+= "{}_param->out_s[2] = {};\n".format(self.reader.name, self.reader.osizes[2])
            
            s+= "{}_param->kern_s[0] = {};\n".format(self.reader.name,  self.reader.kernel[0])
            s+= "{}_param->kern_s[1] = {};\n".format(self.reader.name,  self.reader.kernel[1])
            s+= "{}_param->stride[0] = {};\n".format(self.reader.name,  self.reader.stride[0])
            s+= "{}_param->stride[1] = {};\n".format(self.reader.name,  self.reader.stride[1])
            s+= "{}_param->pad[0] = {};\n".format(self.reader.name,  self.reader.padding[0])
            s+= "{}_param->pad[1] = {};\n".format(self.reader.name,  self.reader.padding[1])
            s+= "{}_param->pad[2] = {};\n".format(self.reader.name,  self.reader.padding[2])
            s+= "{}_param->pad[3] = {};\n".format(self.reader.name,  self.reader.padding[3])
            s+= "{}_param->mode = {};\n".format(self.reader.name,  self.reader.pool_mode)
            s+= "{}_param->qf = {};\n".format(self.reader.name,  self.reader.qf)
                                                
                                
                
            return s



#  ███████╗ ██████╗ ██████╗ ██╗    ██╗ █████╗ ██████╗ ██████╗ 
#  ██╔════╝██╔═══██╗██╔══██╗██║    ██║██╔══██╗██╔══██╗██╔══██╗
#  █████╗  ██║   ██║██████╔╝██║ █╗ ██║███████║██████╔╝██║  ██║
#  ██╔══╝  ██║   ██║██╔══██╗██║███╗██║██╔══██║██╔══██╗██║  ██║
#  ██║     ╚██████╔╝██║  ██║╚███╔███╔╝██║  ██║██║  ██║██████╔╝
#  ╚═╝      ╚═════╝ ╚═╝  ╚═╝ ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ 
#                                                             


#---------------------------------------------------------------------------------------------------------------#
        def get_forward_string(self):
                

            a7 = "\n\n// ############################## MaxPool layer #############################################\n"
          
            a6 = "\nmaxpool_forward_wrap ({}_param);\n\n".format(self.reader.name)
            s =a7 + a6

               
            return s

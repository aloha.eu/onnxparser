
# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.

import sys, math, os, struct
import numpy as np
from .NEURAgheWriter import NEURAgheWriter


class ConvTransposeWriter(NEURAgheWriter):
        def __init__ (self, reader):

          super().__init__(reader)
          
          
          



#  ███████╗ █████╗ ██╗   ██╗███████╗    ██████╗  █████╗ ██████╗  █████╗ ███╗   ███╗
#  ██╔════╝██╔══██╗██║   ██║██╔════╝    ██╔══██╗██╔══██╗██╔══██╗██╔══██╗████╗ ████║
#  ███████╗███████║██║   ██║█████╗      ██████╔╝███████║██████╔╝███████║██╔████╔██║
#  ╚════██║██╔══██║╚██╗ ██╔╝██╔══╝      ██╔═══╝ ██╔══██║██╔══██╗██╔══██║██║╚██╔╝██║
#  ███████║██║  ██║ ╚████╔╝ ███████╗    ██║     ██║  ██║██║  ██║██║  ██║██║ ╚═╝ ██║
#  ╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚══════╝    ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝

        def save_parameters(self, path):
          from tqdm import tqdm
          print ("\n"+self.reader.name)
          if self.reader.weights is not None:
            filename = self.reader.name + "_weights.bin"
            with open(os.path.join(path, filename), "wb") as f:
              
              for wb in tqdm(self.reader.weights.flatten(), bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
               # print (wb)
               # print (self.reader.convert_float_to_short(wb,'h',self.reader.qf))
                w_byte=struct.pack("h",self.reader.convert_float_to_short(wb,'h',self.reader.qf, layer_name = self.reader.name))
                  
                f.write(w_byte)
           
            filename = self.reader.name + "_biases.bin"
            if self.reader.biases is not None: # check if biases exsist
              biases = self.reader.biases
            else:
              biases = np.zeros(self.reader.osizes[0])
            with open(os.path.join(path, filename), "wb") as f:
              for wb in tqdm(biases, bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                b_byte=struct.pack("h",self.reader.convert_float_to_short(wb,'h',self.reader.qf, layer_name = self.reader.name))
                f.write(b_byte)
            
          
        def get_weights(self):
          if len(self.reader.input_)>1:
            if self.reader.input_[1] in self.reader.initializer.parameters.keys():
              return self.reader.initializer.parameters[self.reader.input_[1]]
            else:
              return None
          else:
            return None
        
        def get_biases(self):
          if len(self.reader.input_)>2:
            if self.reader.input_[2] in self.reader.initializer.parameters.keys():
              return self.reader.initializer.parameters[self.reader.input_[2]]
            else:
              return None
          else:
            return np.zeros(self.reader.osizes[0])
      
 
            

#  ██████╗ ███████╗ ██████╗██╗      █████╗ ██████╗ ███████╗
#  ██╔══██╗██╔════╝██╔════╝██║     ██╔══██╗██╔══██╗██╔════╝
#  ██║  ██║█████╗  ██║     ██║     ███████║██████╔╝█████╗  
#  ██║  ██║██╔══╝  ██║     ██║     ██╔══██║██╔══██╗██╔══╝  
#  ██████╔╝███████╗╚██████╗███████╗██║  ██║██║  ██║███████╗
#  ╚═════╝ ╚══════╝ ╚═════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝
#                                                          

#---------------------------------------------------------------------------------------------------------------#
        def get_declaration_string(self):
            string = ''
            
            string = "\nSPATCONVTRANSPOSE {}_param; " .format(self.reader.name)
           
            return string
            
        
       
#---------------------------------------------------------------------------------------------------------------#

#  ██╗    ██╗██╗██████╗ ██╗███╗   ██╗ ██████╗ 
#  ██║    ██║██║██╔══██╗██║████╗  ██║██╔════╝ 
#  ██║ █╗ ██║██║██████╔╝██║██╔██╗ ██║██║  ███╗
#  ██║███╗██║██║██╔══██╗██║██║╚██╗██║██║   ██║
#  ╚███╔███╔╝██║██║  ██║██║██║ ╚████║╚██████╔╝
#   ╚══╝╚══╝ ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝ ╚═════╝ 
#                                             


#inherited
        

#  ██╗    ██╗███████╗██╗    ██╗      ██████╗  █████╗ ██████╗ 
#  ██║    ██║██╔════╝██║    ██║     ██╔═══██╗██╔══██╗██╔══██╗
#  ██║ █╗ ██║█████╗  ██║    ██║     ██║   ██║███████║██║  ██║
#  ██║███╗██║██╔══╝  ██║    ██║     ██║   ██║██╔══██║██║  ██║
#  ╚███╔███╔╝███████╗██║    ███████╗╚██████╔╝██║  ██║██████╔╝
#   ╚══╝╚══╝ ╚══════╝╚═╝    ╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ 
#                                                            

        def get_weights_loading_string(self):
          string = ''

          wsize = np.prod(self.reader.kernel) * self.reader.isizes[0] * self.reader.osizes[0]
          string += 'sprintf(filename, "%%s/SW/%s_weights.bin", load_data_dir);\n'%(self.reader.name)
          string += '%s_param -> kernel = (DATA*)malloc(%d * sizeof(DATA));\n' % (self.reader.name, wsize)
          string += 'load_fixed(filename, %d, %s_param -> kernel);\n'%(wsize,self.reader.name)
          
          size = self.reader.osizes[0]
          string += 'sprintf(filename, "%%s/SW/%s_biases.bin", load_data_dir);\n'%(self.reader.name)
          string += '%s_param -> bias = (DATA*)malloc(%d * sizeof(DATA));\n' % (self.reader.name, size)
          string += 'load_fixed(filename, %d, %s_param -> bias);\n'%(size,self.reader.name)
          
          string += "\n"
          return string


#  ██╗███╗   ██╗██╗████████╗
#  ██║████╗  ██║██║╚══██╔══╝
#  ██║██╔██╗ ██║██║   ██║   
#  ██║██║╚██╗██║██║   ██║   
#  ██║██║ ╚████║██║   ██║   
#  ╚═╝╚═╝  ╚═══╝╚═╝   ╚═╝   
#                           


        def get_initialization_string(self):
                
          s ="\n\n//######################################\n"
          s+= "//            CONVTRANSPOSE LAYER init  \n"
          s+="// ######################################\n"
          s+= "\n\n {}_param = spatconvtranspose_create();\n".format (self.reader.name)
          
          
          out_dim = self.reader.osizes[0]
          ker     = self.reader.kernel
          in_dim  = self.reader.isizes[0]
          iW      = self.reader.isizes[2]
          iH      = self.reader.isizes[1]
          oW      = self.reader.osizes[2]
          oH      = self.reader.osizes[1]
          pad0    = self.reader.padding[0]
          pad1    = self.reader.padding[1]
          Stride  = self.reader.stride
             
          s+= " {}_param->pout ={};\n"                                               .format (self.reader.name ,out_dim)
          s+= " {}_param->pin = {};\n\n"                                               .format ( self.reader.name, in_dim)
          s+= " {}_param->kern_s[0] = {};\n"                                         .format (self.reader.name, out_dim)
          s+= " {}_param->kern_s[1] = {};\n"                                         .format (self.reader.name, in_dim)
          s+= " {}_param->kern_s[2] = {};\n"                                         .format (self.reader.name, ker[0])
          s+= " {}_param->kern_s[3] = {};\n\n"                                       .format (self.reader.name, ker[1])
          
          s+= " {}_param->in_s[0] = {};\n"                                         .format (self.reader.name, in_dim)
          s+= " {}_param->in_s[1] = {};\n"                                         .format (self.reader.name, iH)
          s+= " {}_param->in_s[2] = {};\n\n"                                         .format (self.reader.name, iW)
          
          s+= " {}_param->out_s[0] = {};\n"                                       .format (self.reader.name, out_dim)
          s+= " {}_param->out_s[1] = {};\n"                                         .format (self.reader.name, oH)
          s+= " {}_param->out_s[2] = {};\n\n"                                         .format (self.reader.name, oW)
          
          s+= " {}_param->qf = {};\n"                                         .format (self.reader.name, self.reader.qf) 
          s+= " {}_param->activate = {};\n"                                         .format (self.reader.name, 0)
          s+= " {}_param->precision8 = {};\n\n"                                         .format (self.reader.name, 0)

          
          s+= "{}_param->pad[0] = {};\n".format(self.reader.name,  pad0)
          s+= "{}_param->pad[1] = {};\n\n".format(self.reader.name,  pad1)
          s+= "{}_param->stride[0] = {};\n".format(self.reader.name,  Stride[0])
          s+= "{}_param->stride[1] = {};\n\n".format(self.reader.name,  Stride[1])
          
          s+= "{}_param->dil[0] = {};\n".format(self.reader.name,  1)
          s+= "{}_param->dil[1] = {};\n\n".format(self.reader.name,  1)
          
          return s


#  ███████╗ ██████╗ ██████╗ ██╗    ██╗ █████╗ ██████╗ ██████╗ 
#  ██╔════╝██╔═══██╗██╔══██╗██║    ██║██╔══██╗██╔══██╗██╔══██╗
#  █████╗  ██║   ██║██████╔╝██║ █╗ ██║███████║██████╔╝██║  ██║
#  ██╔══╝  ██║   ██║██╔══██╗██║███╗██║██╔══██║██╔══██╗██║  ██║
#  ██║     ╚██████╔╝██║  ██║╚███╔███╔╝██║  ██║██║  ██║██████╔╝
#  ╚═╝      ╚═════╝ ╚═╝  ╚═╝ ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ 
#                                                             


#---------------------------------------------------------------------------------------------------------------#
        def get_forward_string(self):

          s=''

          s += "\n\n//################################################ CONVTRANSPOSE layer ###############################################\n"
          s += "//kernel {}x{}\n".format (self.reader.kernel[0], self.reader.kernel[1])
          
          s += "int {}_job_id =spatconvtranspose_forward_wrap ({}_param);\n".format(self.reader.name, self.reader.name)

         
          return s
                
                

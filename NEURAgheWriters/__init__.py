from .ConvWriter import ConvWriter
from .NEURAgheWriter import NEURAgheWriter
from .MaxPoolWriter import MaxPoolWriter
from .ReluWriter import ReluWriter
from .GemmWriter import GemmWriter
from .AddWriter import AddWriter
from .MulWriter import MulWriter
from .DivWriter import DivWriter
from .Float2FixedWriter import Float2FixedWriter
from .Fixed2FloatWriter import Fixed2FloatWriter
from .ClipWriter import ClipWriter
from .BatchNormalizationWriter import BatchNormalizationWriter
from .ConvTransposeWriter import ConvTransposeWriter
from .FlattenWriter import FlattenWriter
from .RegionWriter import RegionWriter
from .ConcatWriter import ConcatWriter
from .PadWriter import PadWriter
from .CropWriter import CropWriter
from .BitShiftWriter import BitShiftWriter
from .InputWriter import InputWriter

mapping = {"Conv"               : ConvWriter,
           "Gemm"               : GemmWriter,
           "MaxPool"            : MaxPoolWriter,
           "AveragePool"        : MaxPoolWriter,
           "GlobalAveragePool"  : MaxPoolWriter,
           "Relu"               : ReluWriter,
           "LeakyRelu"          : ReluWriter,
           "Add"                : AddWriter,
           "Mul"                : MulWriter,
           "Div"                : DivWriter,
           "Clip"               : ClipWriter,
           "BatchNormalization" : BatchNormalizationWriter,
           "ConvTranspose"      : ConvTransposeWriter,
           "Flatten"            : FlattenWriter,
           "Float2Fixed"        : Float2FixedWriter,
           "Fixed2Float"        : Fixed2FloatWriter,
           "Region"             : RegionWriter,
           "Concat"             : ConcatWriter,
           "Pad"                : PadWriter,
           "BitShift"           : BitShiftWriter,
           "Input"              : InputWriter,
           "Crop"               : CropWriter,
    }
    
    
supported_operators=["Conv", "Gemm", "MaxPool", "AveragePool", "GlobalAveragePool", "Relu", "LeakyRelu",
                     "Pad", "Crop", "Fixed2Float","Float2Fixed", "Flatten", "Add", "Mul", "Div", "BatchNormalization",
                     "ConvTranspose", "Region", "Sigmoid", "Concat", "Floor", "BitShift", "Clip", "Constant", "Cast","Input"]
         

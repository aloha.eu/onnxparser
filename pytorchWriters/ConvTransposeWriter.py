
# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.

import sys, math, os, struct
import numpy as np
from .PytorchWriter import PytorchWriter


class ConvTransposeWriter(PytorchWriter):
        def __init__ (self, reader):
          self.conv_count = 0
          self.conv_net_number = 0
          self.dilation = reader.dilation
          self.group = reader.group
          super().__init__(reader)
        

          '''
#  ███████╗ █████╗ ██╗   ██╗███████╗    ██████╗  █████╗ ██████╗  █████╗ ███╗   ███╗
#  ██╔════╝██╔══██╗██║   ██║██╔════╝    ██╔══██╗██╔══██╗██╔══██╗██╔══██╗████╗ ████║
#  ███████╗███████║██║   ██║█████╗      ██████╔╝███████║██████╔╝███████║██╔████╔██║
#  ╚════██║██╔══██║╚██╗ ██╔╝██╔══╝      ██╔═══╝ ██╔══██║██╔══██╗██╔══██║██║╚██╔╝██║
#  ███████║██║  ██║ ╚████╔╝ ███████╗    ██║     ██║  ██║██║  ██║██║  ██║██║ ╚═╝ ██║
#  ╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚══════╝    ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝

        def save_parameters(self, path):
          from tqdm import tqdm
          print ("\n"+self.reader.name)
          if self.reader.operation == "Gemm" or self.reader.operation == "Conv":
            if self.reader.weights is not None:
              filename = self.reader.name + "_weights.bin"
              with open(os.path.join(path, filename), "wb") as f:
                
                for wb in tqdm(self.reader.weights.flatten(), bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                 # print (wb)
                 # print (self.reader.convert_float_to_short(wb,'h',self.reader.qf))
                  w_byte=struct.pack("h",self.reader.convert_float_to_short(wb,'h',self.reader.qf))
                    
                  f.write(w_byte)
             
              filename = self.reader.name + "_biases.bin"
              if self.reader.biases is not None: # check if biases exsist
                biases = self.reader.biases
              else:
                biases = np.zeros(self.reader.osizes[0])
              with open(os.path.join(path, filename), "wb") as f:
                for wb in tqdm(biases, bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                  b_byte=struct.pack("h",self.reader.convert_float_to_short(wb,'h',self.reader.qf))
                  f.write(b_byte)
            
          
        def get_weights(self):
          if len(self.reader.input_)>1:
            if self.reader.operation == "BatchNormalization":
              if self.reader.input_[1] in self.reader.initializer.parameters.keys():
                return self.reader.initializer.parameters[self.reader.input_[1]]
              else:
                return None
            else:
              if self.reader.input_[1] in self.reader.initializer.parameters.keys():
                return self.reader.initializer.parameters[self.reader.input_[1]]
              else:
                return None
          else:
            return None
        
        def get_biases(self):
          if len(self.reader.input_)>2:
            if self.reader.operation == "BatchNormalization":
              if self.reader.input_[2] in self.reader.initializer.parameters.keys():
                return self.reader.initializer.parameters[self.reader.input_[2]]
              else:
                return None
            
            else:
              if self.reader.input_[2] in self.reader.initializer.parameters.keys():
                return self.reader.initializer.parameters[self.reader.input_[2]]
              else:
                return None
          else:
            return np.zeros(self.reader.osizes[0])
      
 
#---------------------------------------------------------------------------------------------------------------#
        def layers_stride (self):
            node = self.reader.node
            if node.op_type == "Conv" or  node.op_type == "MaxPool" or node.op_type =="AveragePool":
                       lenght = (len(node.attribute))
                       i = 0
                       for attr in node.attribute:
                               if (i < lenght) :
                                       if node.attribute[i].name == "strides":
                                               stride =node.attribute[i].ints[0]
                                               break
                                       else : i = i+1
            else:
                stride = (None)
            return stride


#---------------------------------------------------------------------------------------------------------------#
        def layers_dilation (self):
            node = self.reader.node
            no_dilations = True
            if node.op_type == "Conv" :
                no_dilations = True
                for attr in node.attribute:
                    if attr.name == "dilations":
                        dilations = attr.ints[0]
                        no_dilations = False
            if no_dilations == True :
                dilations = 1   # default vale for group in Conv layers
            return dilations

#---------------------------------------------------------------------------------------------------------------#
        def layers_padding (self):
            node = self.reader.node
            padding = [0,0,0,0]
            if node.op_type == "Conv" or  node.op_type == "MaxPool" or node.op_type =="AveragePool":
                for attr in node.attribute:
                    if (attr.name == "pads"):
                       
#                        padding_x = 0
#                        padding_y = 0
#                        padding_x_s = attr.ints[0]
#                        padding_x_e = attr.ints[2]
#                        padding_y_s = attr.ints[1]
#                        padding_y_e = attr.ints[3]
#                        if padding_x_s !=0:
#                            padding_x = padding_x_s
#                        elif padding_x_e !=0:
#                            padding_x = padding_x_e
#                        if padding_y_s != 0:
#                            padding_y = padding_y_s
#                        elif padding_y_e !=0:
#                            padding_y = padding_y_e
#                        padding[0] = padding_x
#                        padding[1] = padding_y
                        
                        padding[0]=attr.ints[0]
                        padding[1]=attr.ints[1]
                        padding[2]=attr.ints[2]
                        padding[3]=attr.ints[3]
                        
                    elif (attr.name == "auto_pad" ):
                        pad = attr.s
                        pad =pad.decode("utf-8")
                        if pad == "VALID":
                            padding =[0,0,0,0]
                        elif pad == "NOTSET":
                            break
                        else: padding = pad
            else :
                   padding[0] = 0
                   padding[1] = 0
                   padding[2] = 0
                   padding[3] = 0
                   
            if padding == 'SAME_UPPER':   # output input feature should have same dimensions of input feature
                kernel = self.reader.kernel
                padding =[0,0,0,0]
                pad = (kernel-1)/2
                pad = int(pad)
                padding[0] = pad
                padding[1] = pad
                padding[2] = pad
                padding[3] = pad
                if node.op_type == "MaxPool": #FIXME: this works only with few cases
                  padding =[0,0,0,0]
                  padding[0] = (kernel-self.reader.stride)%2 #x begin
                  padding[1] = pad #x end
                  padding[2] = (kernel-self.reader.stride)%2 #y begin
                  padding[3] = pad #y end
                
            return padding

#---------------------------------------------------------------------------------------------------------------#
        def layers_kernel (self):
            node = self.reader.node
            if node.op_type == "Conv" or  node.op_type == "MaxPool"  or node.op_type =="AveragePool":
                   lenght = (len(node.attribute))
                   for attr in node.attribute:
                                   if attr.name == "kernel_shape":
                                           kernel = attr.ints[0]
                                           break
            else:  kernel = None
            return kernel

#---------------------------------------------------------------------------------------------------------------#
        def concat_axis (self):
            node = self.reader.node
            i = 0
            if  node.op_type == "Concat":
                    for attr in node.attribute:
                            if node.attribute[i].name == "axis":
                                    param = node.attribute[i].i
                                    break
                            else:  i = i+1
            else : param = None
            return param

#---------------------------------------------------------------------------------------------------------------#
        def find_shape(self):
            global prev_shape
            node = self.reader.node
            if node.op_type == "Reshape":
                bias_and_nodes = self.reader.initializer.parameters
                weights_and_nodes = self.reader.initializer.parameters
                input_1 = self.reader.input_[1]
                shape =  bias_and_nodes[input_1]
                prev_shape = shape

                if (shape[0] == 0 and shape[1] ==-1):
                    shape_2=True
                else: shape_2=False
                if self.reader.input_[0]  in weights_and_nodes:
                    input_ = self.reader.input_[0]  # l'input va cercato tra gli initializer
                else: input_ = None
            else :
                shape = None
                input_= None
                shape_2 = False
            return shape , input_, shape_2


#-------------------------------------------------------------------------------------------------------------#
        def conv_output_dim2 ( self):
            node = self.reader.node

            if node.op_type =="Conv" or node.op_type =="Gemm":
                k=self.reader.input_[1]
                
                if k in self.reader.initializer.parameters.keys():
                       
                        output_channels_n = self.reader.initializer.parameters[k].shape[0]
                        output_channels_n_gemm = 0
                self.reader.output_channels_prev_layer_ = output_channels_n
            else:
                output_channels_n = self.reader.isizes[0]
                output_channels_n_gemm = None
            return output_channels_n , output_channels_n_gemm

        


#---------------------------------------------------------------------------------------------------------------#
        def constant (self):
            node = self.reader.node
            if node.op_type == "Constant":

                for attribute in node.attribute:
                    if attribute.name == "value":
                        value = attribute.t.raw_data
                    else:
                        print ("Constant attribute not supported")
            else: value = None
            return value


#-----------------------------------------------------------------
        def pad_parameters(self):
            node = self.reader.node
            if node.op_type == "Pad":
                for attribute in node.attribute:
                    if attribute.name == "mode":
                        mode = attribute.s
                        mode = mode.decode("utf-8")
                    elif attribute.name == "pads":
                        pads = attribute.ints[0]
                    elif attribute.name == "value":
                        value = attribute.f
            else :
                mode = None
                pads = None
                value = None
            return mode, pads, value
            
'''
#  ██████╗ ███████╗ ██████╗██╗      █████╗ ██████╗ ███████╗
#  ██╔══██╗██╔════╝██╔════╝██║     ██╔══██╗██╔══██╗██╔════╝
#  ██║  ██║█████╗  ██║     ██║     ███████║██████╔╝█████╗  
#  ██║  ██║██╔══╝  ██║     ██║     ██╔══██║██╔══██╗██╔══╝  
#  ██████╔╝███████╗╚██████╗███████╗██║  ██║██║  ██║███████╗
#  ╚═════╝ ╚══════╝ ╚═════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝
#                                                          

#---------------------------------------------------------------------------------------------------------------#
        def get_declaration_string(self):
            string = ''
            if self.reader.operation == "Conv":
                string = "\nSPATCONV {}_param; " .format(self.reader.name)
                
           
            return string
            
        
       
#---------------------------------------------------------------------------------------------------------------#

#  ██╗    ██╗██╗██████╗ ██╗███╗   ██╗ ██████╗ 
#  ██║    ██║██║██╔══██╗██║████╗  ██║██╔════╝ 
#  ██║ █╗ ██║██║██████╔╝██║██╔██╗ ██║██║  ███╗
#  ██║███╗██║██║██╔══██╗██║██║╚██╗██║██║   ██║
#  ╚███╔███╔╝██║██║  ██║██║██║ ╚████║╚██████╔╝
#   ╚══╝╚══╝ ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝ ╚═════╝ 
#                                             


#inherited
        

#  ██╗    ██╗███████╗██╗    ██╗      ██████╗  █████╗ ██████╗ 
#  ██║    ██║██╔════╝██║    ██║     ██╔═══██╗██╔══██╗██╔══██╗
#  ██║ █╗ ██║█████╗  ██║    ██║     ██║   ██║███████║██║  ██║
#  ██║███╗██║██╔══╝  ██║    ██║     ██║   ██║██╔══██║██║  ██║
#  ╚███╔███╔╝███████╗██║    ███████╗╚██████╔╝██║  ██║██████╔╝
#   ╚══╝╚══╝ ╚══════╝╚═╝    ╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ 
#                                                            


#---------------------------------------------------------------------------------------------------------------#
        def get_parameters_loading_string(self):
            s=""
            if self.load_params:
              s =  "        self.{}.weight = torch.nn.Parameter (torch.tensor(self.params["'"{}"'"]))\n".format(self.name, self.input_[1])            
              if len(self.input_)>2:
                s += "        self.{}.bias   = torch.nn.Parameter (torch.tensor(self.params["'"{}"'"]))\n".format(self.name, self.input_[-1])
              else:              
                s += "        self.{}.bias.data.fill_(0)\n".format(self.name)
              s+= "\n"
            return s


#  ██╗███╗   ██╗██╗████████╗
#  ██║████╗  ██║██║╚══██╔══╝
#  ██║██╔██╗ ██║██║   ██║   
#  ██║██║╚██╗██║██║   ██║   
#  ██║██║ ╚████║██║   ██║   
#  ╚═╝╚═╝  ╚═══╝╚═╝   ╚═╝   
#                           


        def get_initialization_string(self):
          s=''
          input_features_size = self.isizes[-1]
          mod = input_features_size % 2
          if mod > 0:
              even_input = False
          else:
              even_input = True
          if even_input:
              s = "\n        self.{} = nn.ConvTranspose2d ({}, {},  {}, stride = {}, padding = ({}), dilation = {}, groups = {})".format(self.name,
                            self.isizes[0], self.osizes[0], self.kernel, self.stride, self.padding[0:4:2], self.dilation, self.group)

             
          elif  self.kernel % 2 >0: # se il kernel è dispari e l'input è dispari
             
                  a0 = "\n        self.zeropad2d_{} = nn.ZeroPad2d((1,0,1,0))".format(self.name)
                  a1 = "\n        self.{} = nn.Conv2d ({}, {},  {}, stride = {}, padding = ({}), dilation = {}, groups = {}) ".format(self.name,
                  self.isizes[0], self.osizes[0], self.kernel, self.stride, self.padding[0:4:2], self.dilation, self.group)
                  s = a1 #a0 + a1

            
                
          return s


#  ███████╗ ██████╗ ██████╗ ██╗    ██╗ █████╗ ██████╗ ██████╗ 
#  ██╔════╝██╔═══██╗██╔══██╗██║    ██║██╔══██╗██╔══██╗██╔══██╗
#  █████╗  ██║   ██║██████╔╝██║ █╗ ██║███████║██████╔╝██║  ██║
#  ██╔══╝  ██║   ██║██╔══██╗██║███╗██║██╔══██║██╔══██╗██║  ██║
#  ██║     ╚██████╔╝██║  ██║╚███╔███╔╝██║  ██║██║  ██║██████╔╝
#  ╚═╝      ╚═════╝ ╚═╝  ╚═╝ ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ 
#                                                             


#---------------------------------------------------------------------------------------------------------------#
        def get_forward_string(self):

                s=''
                input_features_size = self.isizes[-1]
                mod = input_features_size % 2
                if mod > 0:
                  even_input = False
                else:
                  even_input = True
          
                if even_input: # nel caso in cui l'input della conv sia pari
                    a0 = "        {} = self.{} ({})\n\n".format( self.output_[0], self.name , self.input_[0])
                    a1 =""# "        #print ({}.shape)\n".format(self.output_[0])
                    a2 = ""
                elif (self.kernel % 2) ==0:         # nel caso in cui l'input della conv sia dispari
                    a0 = "        {} = self.zeropad ({})\n\n".format( self.input_[0],self.input_[0])
                    a1 = "        {} = self.{} ({})\n\n".format( self.output_[0], self.name , self.input_[0])
                    a2 = "        #print ({}.shape)\n".format(self.output_[0])
                else :
                    a0 = "        {} = self.{} ({})\n\n".format( self.output_[0], self.name , self.input_[0])
                    a1 = "        #print ({}.shape)\n".format(self.output_[0])
                    a2 = ""
                s = a0 + a1 + a2

               
                return s
                
                

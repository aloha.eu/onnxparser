
# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.


import sys, math, os, struct
import numpy as np
from .PytorchWriter import PytorchWriter


class GemmWriter(PytorchWriter):
        def __init__ (self,  reader):
          self.gemm_count = 0
          self.gemm_net_number = 0

          super().__init__(reader)
        
          '''
            
#  ███████╗ █████╗ ██╗   ██╗███████╗    ██████╗  █████╗ ██████╗  █████╗ ███╗   ███╗
#  ██╔════╝██╔══██╗██║   ██║██╔════╝    ██╔══██╗██╔══██╗██╔══██╗██╔══██╗████╗ ████║
#  ███████╗███████║██║   ██║█████╗      ██████╔╝███████║██████╔╝███████║██╔████╔██║
#  ╚════██║██╔══██║╚██╗ ██╔╝██╔══╝      ██╔═══╝ ██╔══██║██╔══██╗██╔══██║██║╚██╔╝██║
#  ███████║██║  ██║ ╚████╔╝ ███████╗    ██║     ██║  ██║██║  ██║██║  ██║██║ ╚═╝ ██║
#  ╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚══════╝    ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝

        def save_parameters(self, path):
          from tqdm import tqdm
          print ("\n"+self.reader.name)
          if self.reader.operation == "Gemm" or self.reader.operation == "Conv":
            if self.reader.weights is not None:
              filename = self.reader.name + "_weights.bin"
              with open(os.path.join(path, filename), "wb") as f:
                
                for wb in tqdm(self.reader.weights.flatten(), bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                 # print (wb)
                 # print (self.reader.convert_float_to_short(wb,'h',self.reader.qf))
                  w_byte=struct.pack("h",self.reader.convert_float_to_short(wb,'h',self.reader.qf))
                    
                  f.write(w_byte)
             
              filename = self.reader.name + "_biases.bin"
              if self.reader.biases is not None: # check if biases exsist
                biases = self.reader.biases
              else:
                biases = np.zeros(self.reader.osizes[0])
              with open(os.path.join(path, filename), "wb") as f:
                for wb in tqdm(biases, bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                  b_byte=struct.pack("h",self.reader.convert_float_to_short(wb,'h',self.reader.qf))
                  f.write(b_byte)
            
         
                  
           '''

#  ██████╗ ███████╗ ██████╗██╗      █████╗ ██████╗ ███████╗
#  ██╔══██╗██╔════╝██╔════╝██║     ██╔══██╗██╔══██╗██╔════╝
#  ██║  ██║█████╗  ██║     ██║     ███████║██████╔╝█████╗  
#  ██║  ██║██╔══╝  ██║     ██║     ██╔══██║██╔══██╗██╔══╝  
#  ██████╔╝███████╗╚██████╗███████╗██║  ██║██║  ██║███████╗
#  ╚═════╝ ╚══════╝ ╚═════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝
#                                                          

#---------------------------------------------------------------------------------------------------------------#
        def get_declaration_string(self):
            string = ''
           
            return string
            
#---------------------------------------------------------------------------------------------------------------#

#  ██╗    ██╗██╗██████╗ ██╗███╗   ██╗ ██████╗ 
#  ██║    ██║██║██╔══██╗██║████╗  ██║██╔════╝ 
#  ██║ █╗ ██║██║██████╔╝██║██╔██╗ ██║██║  ███╗
#  ██║███╗██║██║██╔══██╗██║██║╚██╗██║██║   ██║
#  ╚███╔███╔╝██║██║  ██║██║██║ ╚████║╚██████╔╝
#   ╚══╝╚══╝ ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝ ╚═════╝ 
#                                             

        
#inherited

#  ██╗    ██╗███████╗██╗    ██╗      ██████╗  █████╗ ██████╗ 
#  ██║    ██║██╔════╝██║    ██║     ██╔═══██╗██╔══██╗██╔══██╗
#  ██║ █╗ ██║█████╗  ██║    ██║     ██║   ██║███████║██║  ██║
#  ██║███╗██║██╔══╝  ██║    ██║     ██║   ██║██╔══██║██║  ██║
#  ╚███╔███╔╝███████╗██║    ███████╗╚██████╔╝██║  ██║██████╔╝
#   ╚══╝╚══╝ ╚══════╝╚═╝    ╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ 
#                                                            

        def get_parameters_loading_string(self):
          s=""
          if self.load_params:
            s =  "        self.{}.weight = torch.nn.Parameter (torch.tensor(self.params["'"{}"'"]))\n".format(self.name, self.input_[1])
            if len(self.input_)>2:
              s += "        self.{}.bias   = torch.nn.Parameter (torch.tensor(self.params["'"{}"'"]))\n".format(self.name, self.input_[-1])
            else:
              s += "        self.{}.bias.data.fill_(0)\n".format(self.name)
            s+= "\n"
          return s


#  ██╗███╗   ██╗██╗████████╗
#  ██║████╗  ██║██║╚══██╔══╝
#  ██║██╔██╗ ██║██║   ██║   
#  ██║██║╚██╗██║██║   ██║   
#  ██║██║ ╚████║██║   ██║   
#  ╚═╝╚═╝  ╚═══╝╚═╝   ╚═╝   
#                           


        def get_initialization_string(self):
            
            s = "\n        self.{} = nn.Linear ({},{}, bias = True)".format (self.name,self.get_input_pixels(), self.get_output_pixels())

            return s
            
            

#  ███████╗ ██████╗ ██████╗ ██╗    ██╗ █████╗ ██████╗ ██████╗ 
#  ██╔════╝██╔═══██╗██╔══██╗██║    ██║██╔══██╗██╔══██╗██╔══██╗
#  █████╗  ██║   ██║██████╔╝██║ █╗ ██║███████║██████╔╝██║  ██║
#  ██╔══╝  ██║   ██║██╔══██╗██║███╗██║██╔══██║██╔══██╗██║  ██║
#  ██║     ╚██████╔╝██║  ██║╚███╔███╔╝██║  ██║██║  ██║██████╔╝
#  ╚═╝      ╚═════╝ ╚═╝  ╚═╝ ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ 
#                                                             


#---------------------------------------------------------------------------------------------------------------#
        def get_forward_string(self):
                


#            if self.input_[1] not in self.initializer.parameters:
#                if self.gemm_ok == True:
#                    s= "        {} = self.{}( {})\n\n".format(self.output_[0],self.name , self.input_[0])#, self.input_[1], self.input_[2], self.gemm_transB, self.gemm_transA)
#                    self.first_gemm = False

#                elif self.gemm_ok == False and first == True:  # il layer gemm non è preceduto da un reshape e si tratta del primo layer gemm della rete

#                    a0 ="\n        {} = {}.flatten(1)\n".format(self.input_[0], self.input_[0])

#                    a1 = "        {} = self.{}( {})\n\n".format(self.output_[0],self.name , self.input_[0])#, self.input_[1], self.input_[2], self.gemm_transB, self.gemm_transA)
#                    self.first_gemm = False # segnala che è stata incontrato un layer gemm
#                    s= a0 + a1
#                else:
#                    s= "        {} = self.{}( {})\n\n".format(self.output_[0],self.name , self.input_[0])#, self.input_[1], self.input_[2], self.gemm_transB, self.gemm_transA)

#            else:
               # if self.gemm_ok == True:
                s= "        {} = self.{}( {})\n\n".format(self.output_[0],self.name , self.input_[0])#, self.input_[1], self.input_[2], self.gemm_transB, self.gemm_transA)
         #       self.first_gemm_update() #= False

#                elif self.gemm_ok == False and self.first_gemm == True: # il layer gemm non è preceduto da un reshape e si tratta del primo layer gemm della rete

#                    a0 ="\n        {} = {}.flatten(1)\n".format(self.input_[0],self.input_[0])# {}.shape[1] * {}.shape[2] * {}.shape[2])\n".format(self.input_[0], self.input_[0], self.input_[0], self.input_[0], self.input_[0], self.input_[0])

#                    a1 ="        {} = self.{}( {})\n\n".format(self.output_[0],self.name , self.input_[0])#, self.input_[1], self.input_[2], self.gemm_transB, self.gemm_transA)
#                    self.first_gemm_update()# = False # segnala che è stata incontrato un layer gemm
#                    s = a0 + a1

#                elif self.gemm_ok == False and self.first_gemm == False:

#                    s= "        {} = self.{}( {})\n\n".format(self.output_[0],self.name , self.input_[0])#, self.input_[1], self.input_[2], self.gemm_transB, self.gemm_transA)
#                    self.first_gemm_update()#= False # segnala che è stata incontrato un layer gemm

                return s
                
                

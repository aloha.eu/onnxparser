from .ConvWriter import ConvWriter
from .PytorchWriter import PytorchWriter
from .MaxPoolWriter import MaxPoolWriter
from .GlobalAveragePoolWriter import GlobalAveragePoolWriter
from .ReluWriter import ReluWriter
from .GemmWriter import GemmWriter
from .AddWriter import AddWriter
from .MulWriter import MulWriter
from .DivWriter import DivWriter
from .Float2FixedWriter import Float2FixedWriter
from .Fixed2FloatWriter import Fixed2FloatWriter
from .ClipWriter import ClipWriter
from .BatchNormalizationWriter import BatchNormalizationWriter
from .FlattenWriter import FlattenWriter
from .SigmoidWriter import SigmoidWriter
from .ConvTransposeWriter import ConvTransposeWriter
from .ConcatWriter import ConcatWriter
from .DropoutWriter import DropoutWriter
from .TransposeWriter import TransposeWriter
from .ReshapeWriter import ReshapeWriter
from .PadWriter import PadWriter

mapping = {"Conv"               : ConvWriter,
           "Gemm"               : GemmWriter,
           "MaxPool"            : MaxPoolWriter,
           "AveragePool"        : MaxPoolWriter,
           "GlobalAveragePool"  : GlobalAveragePoolWriter,
           "Relu"               : ReluWriter,
           "LeakyRelu"          : ReluWriter,
           "Add"                : AddWriter,
           "Mul"                : MulWriter,
           "Div"                : DivWriter,
           "Clip"               : ClipWriter,
           "BatchNormalization" : BatchNormalizationWriter,
           "Flatten"            : FlattenWriter,
           "Float2Fixed"        : Float2FixedWriter,
           "Fixed2Float"        : Fixed2FloatWriter,
           "Sigmoid"            : SigmoidWriter,
           "ConvTranspose"      : ConvTransposeWriter,
           "Concat"             : ConcatWriter,
           "Dropout"            : DropoutWriter,
           "Reshape"            : ReshapeWriter,
           "Transpose"          : TransposeWriter,
           "Pad"                : PadWriter,
    }
    
    
supported_operators=["Conv", "Gemm", "MaxPool", "ConvTranspose", "AveragePool", "GlobalAveragePool", "Relu", "LeakyRelu", "Pad", "Fixed2Float","Float2Fixed", "Flatten",
                     "Add", "Mul", "Div", "BatchNormalization", "Region", "Sigmoid", "Concat", "Floor", "BitShift", "Clip", "Constant", "Cast","Input",
                     "Dropout", "Transpose", "Reshape"]
                     
         

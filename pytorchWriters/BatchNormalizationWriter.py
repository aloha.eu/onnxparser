
# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.


import sys, math, os
import numpy as np
from .PytorchWriter import PytorchWriter

class BatchNormalizationWriter(PytorchWriter):
        def __init__ (self,  reader):

          
          self.Batchnormalization_params = reader.Batchnormalization_params
          self.batch_eps               = reader.batch_eps
          self.batch_spatial           = reader.batch_spatial
          self.batch_momentum          = reader.batch_momentum
          
          
          super().__init__(reader)

          '''

#  ███████╗ █████╗ ██╗   ██╗███████╗    ██████╗  █████╗ ██████╗  █████╗ ███╗   ███╗
#  ██╔════╝██╔══██╗██║   ██║██╔════╝    ██╔══██╗██╔══██╗██╔══██╗██╔══██╗████╗ ████║
#  ███████╗███████║██║   ██║█████╗      ██████╔╝███████║██████╔╝███████║██╔████╔██║
#  ╚════██║██╔══██║╚██╗ ██╔╝██╔══╝      ██╔═══╝ ██╔══██║██╔══██╗██╔══██║██║╚██╔╝██║
#  ███████║██║  ██║ ╚████╔╝ ███████╗    ██║     ██║  ██║██║  ██║██║  ██║██║ ╚═╝ ██║
#  ╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚══════╝    ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝

        def save_parameters(self, path):
          from tqdm import tqdm
          if self.reader.operation == "BatchNormalization":
            print ("BN datatype ", self.reader.datatype)
            if self.reader.weights is not None:
              filename = self.reader.name + "_weights.bin"
              with open(os.path.join(path, filename), "wb") as f:
                for wb in tqdm(self.reader.weights.flatten(), bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):                 
                  if self.reader.datatype == 'h':
                    w_byte=struct.pack("h",self.reader.convert_float_to_short(wb,'h',self.reader.qf))
                  else:
                    w_byte=struct.pack("f",wb)
                    
                  f.write(w_byte)
             
              filename = self.reader.name + "_biases.bin"
              if self.reader.biases is not None: # check if biases exsist
                biases = self.reader.biases
              else:
                biases = np.zeros(self.reader.osizes[0])
              
              with open(os.path.join(path, filename), "wb") as f:
                for wb in tqdm(biases, bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                  if self.reader.datatype == 'h':
                    b_byte=struct.pack("h",self.reader.convert_float_to_short(wb,'h',self.reader.qf))
                  else:
                    b_byte=struct.pack("f",wb)
                  f.write(b_byte)
            
            if self.reader.mean is not None:
              filename = self.reader.name + "_mean.bin"
              with open(os.path.join(path, filename), "wb") as f:
                for wb in tqdm(self.reader.mean.flatten(), bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                  if self.reader.datatype == 'h':
                    w_byte=struct.pack("h",self.reader.convert_float_to_short(wb,'h',self.reader.qf))
                  else:
                    w_byte=struct.pack("f",wb)
                    
                  f.write(w_byte)
                  
            if self.reader.var is not None:
              filename = self.reader.name + "_var.bin"
              with open(os.path.join(path, filename), "wb") as f:
                for wb in tqdm(self.reader.var.flatten(), bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                  if self.reader.datatype == 'h':
                    w_byte=struct.pack("h",self.reader.convert_float_to_short(wb,'h',self.reader.qf))
                  else:
                    w_byte=struct.pack("f",wb)
                    
                  f.write(w_byte)
                  
                  '''
#  ██████╗ ███████╗ ██████╗██╗      █████╗ ██████╗ ███████╗
#  ██╔══██╗██╔════╝██╔════╝██║     ██╔══██╗██╔══██╗██╔════╝
#  ██║  ██║█████╗  ██║     ██║     ███████║██████╔╝█████╗  
#  ██║  ██║██╔══╝  ██║     ██║     ██╔══██║██╔══██╗██╔══╝  
#  ██████╔╝███████╗╚██████╗███████╗██║  ██║██║  ██║███████╗
#  ╚═════╝ ╚══════╝ ╚═════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝
#                                                          

#---------------------------------------------------------------------------------------------------------------#
        def get_declaration_string(self):
          s=''
          return s
        



#  ██╗    ██╗███████╗██╗    ██╗      ██████╗  █████╗ ██████╗ 
#  ██║    ██║██╔════╝██║    ██║     ██╔═══██╗██╔══██╗██╔══██╗
#  ██║ █╗ ██║█████╗  ██║    ██║     ██║   ██║███████║██║  ██║
#  ██║███╗██║██╔══╝  ██║    ██║     ██║   ██║██╔══██║██║  ██║
#  ╚███╔███╔╝███████╗██║    ███████╗╚██████╔╝██║  ██║██████╔╝
#   ╚══╝╚══╝ ╚══════╝╚═╝    ╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ 
#                                                            

        def get_parameters_loading_string(self):
          s=""
          if self.load_params:
            s = "        self.{}.weight.data       = torch.nn.Parameter (torch.tensor(self.params["'"{}"'"], dtype=torch.float))\n".format(self.name, self.input_[1])
            s+= "        self.{}.bias.data         = torch.nn.Parameter (torch.tensor(self.params["'"{}"'"], dtype=torch.float))\n".format(self.name, self.input_[2])
            s+= "        self.{}.running_mean.data = torch.nn.Parameter (torch.tensor(self.params["'"{}"'"], dtype=torch.float))\n".format(self.name, self.input_[3])
            s+= "        self.{}.running_var.data  = torch.nn.Parameter (torch.tensor(self.params["'"{}"'"], dtype=torch.float))\n".format(self.name, self.input_[4])
            s+= "\n"
          return s


#  ██╗███╗   ██╗██╗████████╗
#  ██║████╗  ██║██║╚══██╔══╝
#  ██║██╔██╗ ██║██║   ██║   
#  ██║██║╚██╗██║██║   ██║   
#  ██║██║ ╚████║██║   ██║   
#  ╚═╝╚═╝  ╚═══╝╚═╝   ╚═╝   
#                           

        def get_initialization_string(self):
          s = "\n        self.{} = nn.BatchNorm2d (num_features={}, eps={})\n".format(self.name, self.osizes[0], self.batch_eps)
          s +="                    # since the momentum definition in PyTorch differs from the definition in ONNX and in other frameworks, we left this value to the default value\n"
          s +="                    # value defined in the onnx: momentum={}\n".format(self.batch_momentum)

          return s


#  ███████╗ ██████╗ ██████╗ ██╗    ██╗ █████╗ ██████╗ ██████╗ 
#  ██╔════╝██╔═══██╗██╔══██╗██║    ██║██╔══██╗██╔══██╗██╔══██╗
#  █████╗  ██║   ██║██████╔╝██║ █╗ ██║███████║██████╔╝██║  ██║
#  ██╔══╝  ██║   ██║██╔══██╗██║███╗██║██╔══██║██╔══██╗██║  ██║
#  ██║     ╚██████╔╝██║  ██║╚███╔███╔╝██║  ██║██║  ██║██████╔╝
#  ╚═╝      ╚═════╝ ╚═╝  ╚═╝ ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ 
#                                                             


#---------------------------------------------------------------------------------------------------------------#
        def get_forward_string(self):
          s = "        {} = self.{} ({})\n\n".format(self.output_[0], self.name, self.input_[0])

                    
          return s
          

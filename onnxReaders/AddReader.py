
# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.


import sys, math, os
import numpy as np
import re
from onnx import numpy_helper
from .OnnxReader import OnnxReader

class AddReader(OnnxReader):
        def __init__ (self, name, init, node=None, prev_layers=None, qf=8, datatype='h'):
          
          self.set_attributes(name, init, node=node, prev_layers=prev_layers, qf=qf, datatype=datatype)
        
          self.datatype='f'
        
          self.osizes = self.get_output_sizes()
          
          self.set_parameters()
          
          #we assume the first input is always the main flow
          for i, inp in enumerate(self.input_):
            if inp == self.prev_layers[0].output_[0]:
              self.input_[0],self.input_[i]=self.input_[i],self.input_[0]

        def set_parameters (self):
          self.bias, self.bias_float      = self.get_bias()
          

        def get_parameters_ (self):
            #print ("Looking for: ", self.input_[1], self.input_[0])
            for i in self.initializer.initializer: # search the value in the initializer
              
              if ('_'+i.name.replace("/","_").replace(".","_")) == self.input_[1] or ('_'+i.name.replace("/","_").replace(".","_")) == self.input_[0]:

                if isinstance(i.float_data[0], list): 
                  a_f = i.float_data[0]
                else:
                  a_f = [i.float_data[0]]
                return a_f

            for i in self.initializer.constants.keys(): # search the value among the constants
              
              if i == self.input_[1] or i == self.input_[0]:
                if isinstance(self.initializer.constants[i], list):
                  a_f = self.initializer.constants[i]                  
                else:
                  a_f = [self.initializer.constants[i]]
                  
                return a_f
          
            return None

        def get_parameters (self):
          if self.operation =="Mul" or self.operation =="Div" or self.operation =="Add":
            '''
            for i in self.initializer.initializer: # search the value in the initializer
              print(i)
              if ('_'+i.name) == re.sub(r'(.*)_', r'\1.', self.input_[1]) or ('_'+i.name) == re.sub(r'(.*)_', r'\1.',self.input_[0]):
                print(i.raw_data)
                float_data=struct.unpack('!f', i.raw_data[0])
                print(float_data)
                if isinstance(float_data[0], list): 
                  a_f = float_data[0]
                else:
                  a_f = [float_data[0]]
                return a_f'''

            for i in self.initializer.initializer: # search for the value in the initializer
              if ('_'+re.sub(r'(.*)_', r'\1.', i.name)) == re.sub(r'(.*)_', r'\1.', self.input_[1]) or \
                 ('_'+re.sub(r'(.*)_', r'\1.', i.name)) == re.sub(r'(.*)_', r'\1.', self.input_[0]):
              # print (i.name)
                try:
                  if isinstance(i.float_data[0], list): 
                    a_f = i.float_data[0]
                  else:
                    a_f = [i.float_data[0]]
                except:
                  a_f = np.squeeze(numpy_helper.to_array(i)).tolist()
                 # print(a_f)
                  #a_f = struct.unpack('f',i.raw_data)
                  if isinstance(a_f, tuple):
                    a_f = list(a_f)
                    
                  if isinstance(a_f, list): 
                    a_f = a_f
                  else:
                    a_f = [a_f]
                return a_f

            for i in self.initializer.constants.keys(): # search the value among the constants
              if i == self.input_[1] or i == self.input_[0]:
                if isinstance(self.initializer.constants[i], list):
                  a_f = self.initializer.constants[i]                  
                else:
                  a_f = [self.initializer.constants[i]]
                  
                return a_f
          
            return None

        def get_bias(self):
          if self.operation =="Add":
            a_f = self.get_parameters()
           # if self.datatype == 'h':
            a   = [self.convert_float_to_short(ii,'h',self.qf) for ii in a_f]
            #else:
           #   a = None
          
            return a, a_f
          else:
            return None, None
        


# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.


import sys, math, os
import numpy as np
import re
from onnx import numpy_helper
from .OnnxReader import OnnxReader

class MulReader(OnnxReader):
        def __init__ (self, name, init, node=None, prev_layers=None, qf=8, datatype='h'):
         # print (node.name)
          self.set_attributes(name, init, node=node, prev_layers=prev_layers, qf=qf, datatype=datatype)
          
          self.datatype = datatype
          
          
          if len(prev_layers)>1:
            ndim = len(prev_layers[0].osizes)
            sizesmax = []
            sizesmax.append (self.prev_layers[0].osizes[0])
            for i in range (1, ndim):
              s[i] = [l.osizes[i] for l in prev_layers]
              
              sizesmax.append (max(s[i]))
              
            self.prev_layers = prev_layers
            self.isizes = sizesmax
          else:
            #we assume the first input is always the main flow
            for i, inp in enumerate(self.input_):
              if inp == self.prev_layers[0].output_[0]:
                self.input_[0],self.input_[i]=self.input_[i],self.input_[0]
              
              
          self.osizes = self.get_output_sizes()

          
          self.set_parameters()


        def set_parameters (self):
          self.scaler, self.scaler_float  = self.get_scaler()




        def get_parameters_ (self):
          #print ("uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu")
          #print ("uuop: ", self.operation)
         # print (self.input_)
          for i in self.initializer.initializer: # search for the value in the initializer
            if ('_'+i.name) == self.input_[1] or ('_'+i.name) == self.input_[0]:
             # print (i.name)
              try:
                if isinstance(i.float_data[0], list): 
                  a_f = i.float_data[0]
                else:
                  a_f = [i.float_data[0]]
              except:
                import struct
                a_f = struct.unpack('f',i.raw_data)
                if isinstance(a_f, tuple):
                  a_f = list(a_f)
                  
                if isinstance(a_f, list): 
                  a_f = a_f
                else:
                  a_f = [a_f]
              return a_f
              

        def get_parameters (self):
          #print ("uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu")
          #print ("uuop: ", self.operation)
         # print (self.input_)
          for i in self.initializer.initializer: # search for the value in the initializer
            # print (i.name)
            if ('_'+re.sub(r'(.*)_', r'\1.', i.name)) == re.sub(r'(.*)_', r'\1.', self.input_[1]) or \
               ('_'+re.sub(r'(.*)_', r'\1.', i.name)) == re.sub(r'(.*)_', r'\1.', self.input_[0]):
              #print (i.name)
              try:
                if isinstance(i.float_data[0], list): 
                  a_f = i.float_data[0]
                else:
                  a_f = [i.float_data[0]]
              except:
                a_f = np.squeeze(numpy_helper.to_array(i)).tolist()
                if isinstance(a_f, tuple):
                  a_f = list(a_f)
                  
                if isinstance(a_f, list): 
                  a_f = a_f
                else:
                  a_f = [a_f]
              return a_f
              

          for i in self.initializer.constants.keys(): # search the value among the constants
            if i == self.input_[1] or i == self.input_[0]:
              if isinstance(self.initializer.constants[i], list):
                a_f = self.initializer.constants[i]                  
              else:
                a_f = [self.initializer.constants[i]]
                
              return a_f
        
          return None

        def get_scaler(self):
          a_f = self.get_parameters()
          if a_f is not None:
           # if self.datatype == 'h':
            a   = [self.convert_float_to_short(ii,'h',self.qf) for ii in a_f]
           # else:
           #   a = None
          else:
            a = None
          return a, a_f



# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.


import sys, math, os
import numpy as np
from .OnnxReader import OnnxReader



class MaxPoolReader(OnnxReader):
        def __init__ (self, name, init, node, prev_layers=None, qf=8, datatype='h'):
             
                self.set_attributes(name, init, node=node, prev_layers=prev_layers, qf=qf, datatype=datatype)
               

                self.initializer             = init

                self.ceil_mode=False
                self.net_input               = init.net_input        # contiene il nome dell'input della rete
                self.input_                  = self.nodes_input()[0] #
                #self.input_original          = self.nodes_input()[1]
                self.stride                  = self.layers_stride()
                self.kernel                  = self.layers_kernel()
                self.padding                 = self.layers_padding()

                self.pool_mode               = self.getPoolMode() 
               
                self.pads                    = self.get_pads()
                               
                

                self.osizes = self.get_output_sizes()
                                
                
              
                self.output_                 = self.nodes_output ()


                self.axis                    = self.concat_axis()
                
                self.constant_param          = self.constant()
                
                self.reshape_params          = self.find_shape()
                self.shape                   = self.reshape_params[0]
                self.reshape_input           = self.reshape_params[1]
                self.shape_2                 = self.reshape_params[2]
                
                self.pad_params              = self.pad_parameters()       # Parametri dell operatore Pad
                self.pad_mode                = self.pad_params[0]          # None if the input of a reshape layer is not an initializer
                self.pad_pads                = self.pad_params[1]
                self.pad_value               = self.pad_params[2]
                
                
                self.to_be_initialized = True
                
                
                self.datatype = 'h'#datatype # h int 16 bit, f floating point
                
                self.estimate_datatype()
                
                
                self.output_features_size = 0
                

        def setStride (self, stride):
          self.stride = stride
          self.padding                 = self.layers_padding()
          self.osizes = self.get_output_sizes()
          
        def getPoolMode(self):
          if self.operation == 'MaxPool':
            return 0
          elif self.operation == 'AveragePool':
            return 2
          elif self.operation == 'SubSampling':
            return 1

        def get_pads(self):
          if self.operation =="Pad":
            for attr in self.node.attribute:
              if attr.name == "pads":                
                return attr.ints
          return None

        def get_output_sizes(self):

          OF = self.conv_output_dim2()[0]
          osizes=[]
          osizes.append(OF)
          
          ndims = len(self.isizes)-1
          for i, s in enumerate(self.isizes[1:]):
            osizes.append ((s - self.kernel[i] + self.padding[i*ndims] + self.padding[i*ndims+1])//self.stride[i] + 1)
          
          return osizes
 

#---------------------------------------------------------------------------------------------------------------#
        def layers_stride (self):
         
          for attr in self.node.attribute:
            if attr.name == "strides":
              return attr.ints
               
          return None

#---------------------------------------------------------------------------------------------------------------#
        def layers_padding (self):
          node = self.node
          
          for attr in node.attribute:
            if (attr.name == "pads"):
               
              padding=attr.ints
              if self.isizes[1]%2 == 1 and sum(padding)==0:
                padding = [0,1,0,1]
                
            elif (attr.name == "auto_pad" ):
              pad = attr.s
              pad =pad.decode("utf-8")
              if pad == "VALID":
                  padding =[0,0,0,0]
              elif pad == "NOTSET":
                  break
              elif pad == "SAME_UPPER":
              
                kernel = self.kernel[0]
                padding =[0,0,0,0]
                
                print ("FIXME!!!!!: layers_padding in MaxPoolReader")
                
                pad = int((self.isizes[-1]%self.stride[0])%kernel)
                
                if pad==1 or self.isizes[-1]%kernel:
                  #self.ceil_mode = True
                
                  padding[0] = 0
                  padding[1] = 1
                  padding[2] = 0
                  padding[3] = 1



          #      pad = (kernel-1)/2
          #      r = (kernel-1)%2
          #      print ("r", r)
          #      pad = int(pad)
          #      if r==0:
          #        padding[0] = pad
          #        padding[1] = pad
          #        padding[2] = pad
          #        padding[3] = pad
          #      else:
          #        padding[0] = pad
          #        padding[1] = pad+r
          #        padding[2] = pad
          #        padding[3] = pad+r
                  
              else:
                print ("Not supported: ", pad)
                exit(1)
              
             # print (padding)
             
         
                 
         
          return padding

#---------------------------------------------------------------------------------------------------------------#
        def layers_kernel (self):
          node = self.node

          for attr in self.node.attribute:
            if attr.name == "kernel_shape":
              return attr.ints
              
          return None

#---------------------------------------------------------------------------------------------------------------#
        def concat_axis (self):
            node = self.node
            i = 0
            if  node.op_type == "Concat":
                    for attr in node.attribute:
                            if node.attribute[i].name == "axis":
                                    param = node.attribute[i].i
                                    break
                            else:  i = i+1
            else : param = None
            return param

#---------------------------------------------------------------------------------------------------------------#
        def find_shape(self):
            global prev_shape
            node = self.node
            shape = None
            input_= None
            shape_2 = False
            return shape , input_, shape_2

#-------------------------------------------------------------------------------------------------------------#
        def conv_output_dim2 ( self):
            node = self.node

            
            output_channels_n = self.isizes[0]
            output_channels_n_gemm = None

            return output_channels_n , output_channels_n_gemm

#---------------------------------------------------------------------------------------------------------------#
        def constant (self):
            node = self.node
            if node.op_type == "Constant":

                for attribute in node.attribute:
                    if attribute.name == "value":
                        value = attribute.t.raw_data
                    else:
                        print ("Constant attribute not supported")
            else: value = None
            return value

#-----------------------------------------------------------------
        def pad_parameters(self):
            node = self.node
            if node.op_type == "Pad":
                for attribute in node.attribute:
                    if attribute.name == "mode":
                        mode = attribute.s
                        mode = mode.decode("utf-8")
                    elif attribute.name == "pads":
                        pads = attribute.ints[0]
                    elif attribute.name == "value":
                        value = attribute.f
            else :
                mode = None
                pads = None
                value = None
            return mode, pads, value



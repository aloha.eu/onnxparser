
# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.


import sys, math, os
import numpy as np
from .OnnxReader import OnnxReader

class BatchNormalizationReader(OnnxReader):
        def __init__ (self, name, init, node=None, prev_layers=None, qf=8, datatype='h'):
          
          self.set_attributes(name, init, node=node, prev_layers=prev_layers, qf=qf, datatype=datatype)
        
        
          self.osizes = self.get_output_sizes()

       
          self.Batchnormalization_params = self.batchnorm_params()
          self.batch_eps               = self.Batchnormalization_params[0]
          self.batch_spatial           = self.Batchnormalization_params[1]
          self.batch_momentum          = self.Batchnormalization_params[2]
                
                
          self.weights = np.array(self.get_weights())
          self.biases  = np.array(self.get_biases())
                
          self.mean    = self.get_mean()
          self.var     = self.get_var()
          
          self.datatype = 'f'


        def get_parameters (self):
          if self.get_var() is not None:
            return self.get_var().flatten()
          else:
            return None

        
        def estimate_datatype(self):
         
          datatype=self.datatype
          
          a = self.get_parameters()

          if a is not None:
            a = np.hstack(a)
            min_value= a.min()            

            if min_value <= 1/pow(2,self.qf):
              datatype ='f'
            else:
              datatype ='h'
            
          if self.datatype != datatype:
            self.datatype = datatype
            print ("[WARNING] datatype in layer {} has been set to float because of the values of the layer parameters".format(self.name))
          #print ("post op: ", self.datatype)
          


        def batchnorm_params (self):
          if self.node is not None:
            node = self.node
            epsilon_ = False
            spatial_ = False
            momentum_ = False
            if node.op_type == "BatchNormalization":
                for attribute in node.attribute:
                    epsilon_ = False
                    spatial_ = False
                    momentum_ = False
                    if attribute.name == "epsilon":
                        epsilon = attribute.f
                        epsilon_ = True
                    else:
                        epsilon = 1e-05
                    if attribute.name == "spatial":
                        spatial = attribute.i
                        spatial_ = True
                    else: spatial = 1
                    if attribute.name == "momentum":
                        momentum = attribute.f
                        momentum_ = True
                    else: momentum = 0.9
            if epsilon_ == False:
                epsilon = 1e-05
            if spatial_ == False:
                spatial =  None
            if momentum_ == False:
                momentum = 0.1
            return epsilon, spatial, momentum
          else:
            return None, None, None

        def get_weights(self):
          if self.input_ is not None:
            if len(self.input_)>1:
              if self.operation == "BatchNormalization":
                if self.input_[1] in self.initializer.parameters.keys():
                  return self.initializer.parameters[self.input_[1]]
                else:
                  return None
            else:
              return None
          else:
            return None
        
        def get_biases(self):
          if self.input_ is not None:
            if len(self.input_)>2:
              if self.operation == "BatchNormalization":
                if self.input_[2] in self.initializer.parameters.keys():
                  return self.initializer.parameters[self.input_[2]]
                else:
                  return None
              
              else:
                if self.input_[2] in self.initializer.parameters.keys():
                  return self.initializer.parameters[self.input_[2]]
                else:
                  return None
            else:
              return np.zeros(self.osizes[0])
          else:
            return None
            
        def get_mean(self):
          if self.input_ is not None:
            if len(self.input_)>3:
              if self.input_[3] in self.initializer.parameters.keys():
                return self.initializer.parameters[self.input_[3]]
              else:
                return None
            else:
              return None
          else:
            return None
            
        def get_var(self):
          if self.input_ is not None:
            if len(self.input_)>4:
              if self.input_[4] in self.initializer.parameters.keys():
                return self.initializer.parameters[self.input_[4]]
              else:
                return None
            else:
              return None
          else:
            return None

          

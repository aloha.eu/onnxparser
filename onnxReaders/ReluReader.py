
# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.


import sys, math, os
import numpy as np
from .OnnxReader import OnnxReader


class ReluReader(OnnxReader):
        def __init__ (self, name, init, node, prev_layers=None, qf=8, datatype='h'):
             
                self.set_attributes(name, init, node=node, prev_layers=prev_layers, qf=qf, datatype=datatype)


                self.initializer             = init

                self.net_input               = init.net_input        # contiene il nome dell'input della rete
                self.input_                  = self.nodes_input()[0] #



                self.osizes    = self.get_output_sizes()
                
                self.output_                 = self.nodes_output ()

                
                self.elu_alpha               = self.elu_params()
                self.leaky_relu_alpha        = self.leaky_relu_alpha()
                self.leaky_relu_alpha_short  = self.leaky_relu_alpha_short(self.leaky_relu_alpha)


                
                self.constant_param          = self.constant()
                
                self.reshape_params          = self.find_shape()
                self.shape                   = self.reshape_params[0]
                self.reshape_input           = self.reshape_params[1]
                self.shape_2                 = self.reshape_params[2]
               
                self.pad_params              = self.pad_parameters()       # Parametri dell operatore Pad
                self.pad_mode                = self.pad_params[0]          # None if the input of a reshape layer is not an initializer
                self.pad_pads                = self.pad_params[1]
                self.pad_value               = self.pad_params[2]


             #   self.datatype = prev_layers[0].datatype
                
#----------- converte il parametro alpha di leaky relu in short int -----------------------------------------------
        def leaky_relu_alpha_short (self, alpha):
            if type(alpha) == float:
                data = self.convert_float_to_short(alpha, self.datatype, self.qf)
            else:
                data = 0
            return data


#-------------------------------------------------------------------------------------------------------------#
        def conv_output_dim2 ( self):
            node = self.node

            if node.op_type =="Conv" or node.op_type =="Gemm":
                k=self.input_[1]
                
                if k in self.initializer.parameters.keys():
                       
                        output_channels_n = self.initializer.parameters[k].shape[0]
                        output_channels_n_gemm = 0
                self.output_channels_prev_layer_ = output_channels_n
            else:
                output_channels_n = self.isizes[0]
                output_channels_n_gemm = None
            return output_channels_n , output_channels_n_gemm


#---------------------------------------------------------------------------------------------------------------#
        def elu_params (self):
          if self.node is not None:
            node = self.node
            if node.op_type == "Elu":
                for attribute in node.attribute:
                    if attribute.name =="alpha":
                        alpha = attribute.f
                else:
                  print ("Slice attribute not supported")
            else:
              alpha = 0
            return alpha
          else:
            return None

#---------------------------------------------------------------------------------------------------------------#
        def constant (self):
          if self.node is not None:
            node = self.node
            if node.op_type == "Constant":

                for attribute in node.attribute:
                    if attribute.name == "value":
                        value = attribute.t.raw_data
                    else:
                        print ("Constant attribute not supported")
            else: value = None
            return value
          else:
            return None

#-----------------------------------------------------------------
        def pad_parameters(self):
          if self.node is not None:
            node = self.node
            if node.op_type == "Pad":
                for attribute in node.attribute:
                    if attribute.name == "mode":
                        mode = attribute.s
                        mode = mode.decode("utf-8")
                    elif attribute.name == "pads":
                        pads = attribute.ints[0]
                    elif attribute.name == "value":
                        value = attribute.f
            else :
                mode = None
                pads = None
                value = None
            return mode, pads, value
          else:
            return None, None, None
#--------------------------------------------------------------------------------
        def leaky_relu_alpha(self):
          if self.node is not None:
            node = self.node
            if node.op_type =="LeakyRelu":
                for attribute in node.attribute:
                    if attribute.name == "alpha":
                        alpha = attribute.f
            else:
              alpha = 0
            return alpha
          else:
            return None

          
          

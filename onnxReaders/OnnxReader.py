
# Copyright (c) 2018
# by Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.


import sys, math, os
import numpy as np

class OnnxReader():
        def __init__ (self, name, init, node=None, prev_layers=None, qf=8, datatype='h'):
        
        
          self.set_attributes(name, init, node=node, prev_layers=prev_layers, qf=qf, datatype=datatype)
        
        
        
        
        def set_attributes (self,name, init, node=None, prev_layers=None, qf=8, datatype='h'):

                if node is not None:

                  self.node                    = node
                  self.operation               = node.op_type
                else:

                  self.node                    = None
                  self.operation               = None
                
                
                self.input_                  = []
                self.output_                 = []

                self.qf                      = qf
                self.name                    = name
                
                if prev_layers != None:
                  if not isinstance(prev_layers, list):
                    prev_layers      = [prev_layers]
                  self.prev_layers   = prev_layers
                  self.isizes       = list(self.prev_layers[0].osizes)
                else:
                  self.prev_layers = None
                  self.isizes     =  None
                
                
                self.to_be_initialized       = True
                
           
                self.initializer             = init

                self.net_input               = init.net_input        # contiene il nome dell'input della rete
               
                
                self.pool_mode               = 0 #inizializzato a zero, vale 1 solo per il pool di adattamento dello stride

                
                self.load_params             = True
                
                self.special_attr_dict = {}
             
                

                
                if node is not None:

                  self.input_                  = self.nodes_input()[0] #kji

                  self.stride                  = self.layers_stride()
                  self.kernel                  = self.layers_kernel()
                  self.padding                 = self.layers_padding()
                
                  self.osizes                  = self.conv_output_dim2()[0]
                                     
                
                  
                  self.output_                 = self.nodes_output ()

                

                  self.reshape_params          = self.find_shape()
                  self.shape                   = self.reshape_params[0]
                  self.reshape_input           = self.reshape_params[1]
                  self.shape_2                 = self.reshape_params[2]
                else:
                  self.input_                  = None

                  self.stride                  = None
                  self.kernel                  = None
                  self.padding                 = None
                
                  self.osizes                  = None
                
                  
                  self.output_                 = None

                

                  self.reshape_params          = None
                  self.shape                   = None
                  self.reshape_input           = None
                  self.shape_2                 = None
                  
                self.datatype                = datatype # h int 16 bit, f floating point
                
                self.estimate_datatype()
                
                
                
                
                
        def set_parameters (self):
            # you should implement this function for the parameters of the specific layer, if any.            
            return None
       

        def get_parameters (self):
            # you should implement this function for the parameters of the specific layer, if any.            
            return None

        
        def estimate_datatype(self):
          #print ("estimate_datatype")
          #print ("op: ", self.operation)
          #print ("op: ", self.datatype)
          
          datatype=self.datatype
          
          a = self.get_parameters()
          #print ("params: ", a)
          if a:
            a = np.hstack(a)
            max_value= a.max()            
          
            if max_value >= 1<<self.qf:
              datatype ='f'
            else:
              datatype ='h'
            
          if self.datatype != datatype:
            self.datatype = datatype
            print ("[WARNING] datatype in layer {} has been set to float because of the values of the layer parameters".format(self.name))
          #print ("post op: ", self.datatype)
          
          

        def convert_float_to_short (self,data, data_type, qf, layer_name="", saturate=True): 
          
          if data_type == 'i':
            data_size = 32
          else:
            if data_type == 'h':
              data_size = 16
            else:
              data_size = 8 
          if data_size<qf:
            print (tcolors.ERROR + "[ERROR] QF is greater than data_size!\n"+ tcolors.ENDC)     
            print ("\t data_size: %d QF: %d\n" % (data_size, qf))
            sys.exit(2) 
          
          if math.isnan(data):
            return 0
          num = int(round((data) * (1<<qf)))
          
          if saturate:
            if num>(2**(data_size-1)-1):
              print ("[WARNING] {} Positive saturation 0x{:08x} -> 0x{:08x}".format(layer_name, num & 0xffffffff,(2**(data_size-1)-1)& 0xffffffff) )
              num = (2**(data_size-1)-1)
            elif num<-2**(data_size-1):
              print ("[WARNING] {} Negative saturation 0x{:08x} -> 0x{:08x}".format(layer_name, num & 0xffffffff,-2**(data_size-1)& 0xffffffff) )
              num = -2**(data_size-1)
            
          return num

        def get_output_sizes(self):
          
          return self.isizes


#---------------------------------------------------------------------------------------------------------------#
        def nodes_input (self):
          if self.node is not None:
            node = self.node
            nodes_input = []
            if node.op_type != "Constant":   # evito di considerare i nodi constant, nella rete unet vengono introdotti per errore?                
                for input_ in node.input:
                    input_= input_.replace("/","_").replace(".","_")
                    input_ = "_{}".format(input_)
                    nodes_input.append(input_)                
            else :
                nodes_input.append("constant")
            
            if node.op_type == "Add": # the Add operator has as first input the biases istead of the x 
              nodes_input.reverse()
            
            return nodes_input, node.input
          else:
            return None, None
#---------------------------------------------------------------------------------------------------------------#
        def nodes_output (self):
          if self.node is not None:
            node = self.node
            nodes_output = []
            for output_ in node.output:
        # elimino i caratteri "/" dalle stringe degli output per evitare errori con pytorch
               line = output_
               output_= line.replace("/","_").replace(".","_")
               output_ = "_{}".format(output_)
               nodes_output.append(output_)
            return nodes_output
          else:
            return None

#---------------------------------------------------------------------------------------------------------------#
        def layers_stride (self):
            node = self.node
            if node.op_type == "Conv" or  node.op_type == "MaxPool" or node.op_type =="AveragePool":
                       lenght = (len(node.attribute))
                       i = 0
                       for attr in node.attribute:
                               if (i < lenght) :
                                       if node.attribute[i].name == "strides":
                                               stride =node.attribute[i].ints[0]
                                               break
                                       else : i = i+1
            else:
                stride = (None)
            return stride

#---------------------------------------------------------------------------------------------------------------#
        def layers_padding (self):
            node = self.node
            padding = [0,0,0,0]
            if node.op_type == "Conv" or  node.op_type == "MaxPool" or node.op_type =="AveragePool":
                for attr in node.attribute:
                    if (attr.name == "pads"):
                       
                        
                        padding[0]=attr.ints[0]
                        padding[1]=attr.ints[1]
                        padding[2]=attr.ints[2]
                        padding[3]=attr.ints[3]
                        
                    elif (attr.name == "auto_pad" ):
                        pad = attr.s
                        pad =pad.decode("utf-8")
                        if pad == "VALID":
                            padding =[0,0,0,0]
                        elif pad == "NOTSET":
                            break
                        else: padding = pad
            else :
                   padding[0] = 0
                   padding[1] = 0
                   padding[2] = 0
                   padding[3] = 0
                   
            if padding == 'SAME_UPPER':   # output input feature should have same dimensions of input feature
                kernel = self.kernel
                padding =[0,0,0,0]
                pad = (kernel-1)/2
                pad = int(pad)
                padding[0] = pad
                padding[1] = pad
                padding[2] = pad
                padding[3] = pad
                if node.op_type == "MaxPool": #FIXME: this works only with few cases
                  padding =[0,0,0,0]
                  padding[0] = (kernel-self.stride)%2 #x begin
                  padding[1] = pad #x end
                  padding[2] = (kernel-self.stride)%2 #y begin
                  padding[3] = pad #y end
                
            return padding

#---------------------------------------------------------------------------------------------------------------#
        def layers_kernel (self):
        # TODO:             raise NotImplementedError("The layers_kernel method from OnnxReader must be overridden!")
        
            node = self.node
            if node.op_type == "Conv" or  node.op_type == "MaxPool"  or node.op_type =="AveragePool":
                   lenght = (len(node.attribute))
                   for attr in node.attribute:
                                   if attr.name == "kernel_shape":
                                           kernel = attr.ints[0]
                                           break
            else:  kernel = None
            return kernel

#---------------------------------------------------------------------------------------------------------------#
        def find_shape(self):
          if self.node is not None:
            global prev_shape
            node = self.node
            if node.op_type == "Reshape":
                bias_and_nodes = self.initializer.parameters
                weights_and_nodes = self.initializer.parameters
                input_1 = self.input_[1]
                shape =  bias_and_nodes[input_1]
                prev_shape = shape

                if (shape[0] == 0 and shape[1] ==-1):
                    shape_2=True
                else: shape_2=False
                if self.input_[0]  in weights_and_nodes:
                    input_ = self.input_[0]  # l'input va cercato tra gli initializer
                else: input_ = None
            else :
                shape = None
                input_= None
                shape_2 = False
            return shape , input_, shape_2
          else:            
            return None , None, None


#-------------------------------------------------------------------------------------------------------------#
        def conv_output_dim2 ( self):
            node = self.node

            if node.op_type =="Conv" or node.op_type =="Gemm":
                k=self.input_[1]
                
                if k in self.initializer.parameters.keys():
                       
                        output_channels_n = self.initializer.parameters[k].shape[0]
                        output_channels_n_gemm = 0
                self.output_channels_prev_layer_ = output_channels_n
            else:
                output_channels_n = self.isizes[0]
                output_channels_n_gemm = None
            return output_channels_n , output_channels_n_gemm


#--------   Avoids multiple inizialitation for the same layer on Net() class  ----------------------------------------------------#
        def avoid_redundant_initialization( layer, maxpool_params, relu_ex,leaky_relu_ex, dropout_ex):
#            if layer.operation == "Relu" and relu_ex == False: # checking if the Relu layer is the first to be read in order to be initialized
#                layer.to_be_initialized = True
#                relu_ex = True

#            elif layer.operation == "Relu" and relu_ex == True:
#                layer.to_be_initialized = False

            if  layer.operation == "LeakyRelu" and leaky_relu_ex == False:
                layer.to_be_initialized == True
                leaky_relu_ex = True

            elif layer.operation == "LeakyRelu" and leaky_relu_ex == "True":
                layer.to_be_initialized = False

            if layer.operation == "Dropout" and dropout_ex == False: # checking if the Dropout layer is the first to be read in order to be initialized
                layer.to_be_initialized = True
                dropout_ex = True
            elif layer.operation == "Dropout" and dropout_ex == True:
                layer.to_be_initialized = False

            if  layer.operation == "MaxPool":   # checking if the Maxpool layer is the first to be read in order to be initialized
                params = []
                stride = layer.stride
                padding = layer.padding
                kernel = layer.kernel
                params.append(kernel)
                params.append(stride)
                params.append(padding)
                if params not in maxpool_params:
                    layer.to_be_initialized = True
                else: layer.to_be_initialized = True
                maxpool_params.append(params)


            return (layer, maxpool_params, relu_ex, leaky_relu_ex, dropout_ex)

          
          

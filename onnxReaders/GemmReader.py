
# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.


import sys, math, os, struct
import numpy as np
from .OnnxReader import OnnxReader


class GemmReader(OnnxReader):
        def __init__ (self, name, init, node, prev_layers=None, qf=8, datatype='h'):
             
                self.set_attributes(name, init, node=node, prev_layers=prev_layers, qf=qf, datatype=datatype)
                


                self.initializer             = init


                self.net_input               = init.net_input        # contiene il nome dell'input della rete
                self.input_                  = self.nodes_input()[0] #
                #self.input_original          = self.nodes_input()[1]
                self.stride                  = self.layers_stride()
                self.kernel                  = self.layers_kernel()
                self.padding                 = self.layers_padding()

                self.pool_mode                 = 0 #inizializzato a zero, vale 1 solo per il pool di adattamento dello stride
             
                self.pads                    = self.get_pads()
                
                               
                

                self.osizes = self.get_output_sizes()
                

                
                
              
                self.output_                 = self.nodes_output ()




                self.constant_param          = self.constant()
                
                self.attr                    = dict()
                self.gemm_transA             = self.gemm_transA()
                self.gemm_transB             = self.gemm_transB()
                self.reshape_params          = self.find_shape()
                self.shape                   = self.reshape_params[0]
                self.reshape_input           = self.reshape_params[1]
                self.shape_2                 = self.reshape_params[2]
                
                self.pad_params              = self.pad_parameters()       # Parametri dell operatore Pad
                self.pad_mode                = self.pad_params[0]          # None if the input of a reshape layer is not an initializer
                self.pad_pads                = self.pad_params[1]
                self.pad_value               = self.pad_params[2]



#---------------definisco quali layer hanno bisogno del codice di inizializzazione #-----------------------------------------
                if ( self.operation == "Conv" or self.operation == "MaxPool" or self.operation == "Softmax"  or self.operation == "LogSoftmax"  or self.operation == "Add"  or self.operation == "Mul"  or self.operation == "Div" or self.operation == "Sigmoid"  or
                 self.operation == "Dropout" or self.operation =="Relu" or self.operation =="BatchNormalization" or self.operation == "Concat"  or self.operation == "Floor"  or self.operation == "Clip"  or self.operation == "BitShift"  or
                 self.operation =="Squeeze" or self.operation =="Unsqueeze" or self.operation == "Elu" or
                 self.operation == "Constant" or self.operation == "Reshape" or self.operation == "AveragePool"or
                 self.operation == "LRN" or self.operation =="Pad" or self.operation == "Gemm" or self.operation == "LeakyRelu"):
                    self.to_be_initialized = True
                else :
                    self.to_be_initialized = False
                
                
                
                self.estimate_datatype()
                
                self.datatype = 'h' # h int 16 bit, f floating point
                
                
                self.output_features_size = 0
                
                
                self.weights = np.array(self.get_weights())
                self.biases  = np.array(self.get_biases())
                
                        

        def get_pads(self):
          if self.operation =="Pad":
            for attr in self.node.attribute:
              if attr.name == "pads":                
                return attr.ints
          return None

         
        def get_weights(self):
          if len(self.input_)>1:
            if self.operation == "BatchNormalization":
              if self.input_[1] in self.initializer.parameters.keys():
                return self.initializer.parameters[self.input_[1]]
              else:
                return None
            else:
              if self.input_[1] in self.initializer.parameters.keys():
                return self.initializer.parameters[self.input_[1]]
              else:
                return None
          else:
            return None
        
        def get_biases(self):
          if len(self.input_)>2:
            if self.operation == "BatchNormalization":
              if self.input_[2] in self.initializer.parameters.keys():
                return self.initializer.parameters[self.input_[2]]
              else:
                return None
            
            else:
              if self.input_[2] in self.initializer.parameters.keys():
                return self.initializer.parameters[self.input_[2]]
              else:
                return None
          else:
            return np.zeros(self.osizes[0])
            
       

        def get_output_sizes(self):
         
          OF=self.conv_output_dim2()[0]
          
          osizes=[]
          osizes.append(OF)
          
          for i, s in enumerate(self.isizes[1:]):
            osizes.append (1)
          
          
          return osizes
#---------------------------------------------------------------------------------------------------------------#
        def layers_stride (self):
            node = self.node
            if node.op_type == "Conv" or  node.op_type == "MaxPool" or node.op_type =="AveragePool":
                       lenght = (len(node.attribute))
                       i = 0
                       for attr in node.attribute:
                               if (i < lenght) :
                                       if node.attribute[i].name == "strides":
                                               stride =node.attribute[i].ints[0]
                                               break
                                       else : i = i+1
            else:
                stride = (None)
            return stride


#---------------------------------------------------------------------------------------------------------------#
        def layers_padding (self):
            node = self.node
            padding = [0,0,0,0]
            if node.op_type == "Conv" or  node.op_type == "MaxPool" or node.op_type =="AveragePool":
                for attr in node.attribute:
                    if (attr.name == "pads"):
                       
#                        padding_x = 0
#                        padding_y = 0
#                        padding_x_s = attr.ints[0]
#                        padding_x_e = attr.ints[2]
#                        padding_y_s = attr.ints[1]
#                        padding_y_e = attr.ints[3]
#                        if padding_x_s !=0:
#                            padding_x = padding_x_s
#                        elif padding_x_e !=0:
#                            padding_x = padding_x_e
#                        if padding_y_s != 0:
#                            padding_y = padding_y_s
#                        elif padding_y_e !=0:
#                            padding_y = padding_y_e
#                        padding[0] = padding_x
#                        padding[1] = padding_y
                        
                        padding[0]=attr.ints[0]
                        padding[1]=attr.ints[1]
                        padding[2]=attr.ints[2]
                        padding[3]=attr.ints[3]
                        
                    elif (attr.name == "auto_pad" ):
                        pad = attr.s
                        pad =pad.decode("utf-8")
                        if pad == "VALID":
                            padding =[0,0,0,0]
                        elif pad == "NOTSET":
                            break
                        else: padding = pad
            else :
                   padding[0] = 0
                   padding[1] = 0
                   padding[2] = 0
                   padding[3] = 0
                   
            if padding == 'SAME_UPPER':   # output input feature should have same dimensions of input feature
                kernel = self.kernel
                padding =[0,0,0,0]
                pad = (kernel-1)/2
                pad = int(pad)
                padding[0] = pad
                padding[1] = pad
                padding[2] = pad
                padding[3] = pad
                if node.op_type == "MaxPool": #FIXME: this works only with few cases
                  padding =[0,0,0,0]
                  padding[0] = (kernel-self.stride)%2 #x begin
                  padding[1] = pad #x end
                  padding[2] = (kernel-self.stride)%2 #y begin
                  padding[3] = pad #y end
                
            return padding

#---------------------------------------------------------------------------------------------------------------#
        def layers_kernel (self):
            node = self.node
            if node.op_type == "Conv" or  node.op_type == "MaxPool"  or node.op_type =="AveragePool":
                   lenght = (len(node.attribute))
                   for attr in node.attribute:
                                   if attr.name == "kernel_shape":
                                           kernel = attr.ints[0]
                                           break
            else:  kernel = None
            return kernel

#---------------------------------------------------------------------------------------------------------------#
        def concat_axis (self):
            node = self.node
            i = 0
            if  node.op_type == "Concat":
                    for attr in node.attribute:
                            if node.attribute[i].name == "axis":
                                    param = node.attribute[i].i
                                    break
                            else:  i = i+1
            else : param = None
            return param

#---------------------------------------------------------------------------------------------------------------#
        def find_shape(self):
            global prev_shape
            node = self.node
            if node.op_type == "Reshape":
                bias_and_nodes = self.initializer.parameters
                weights_and_nodes = self.initializer.parameters
                input_1 = self.input_[1]
                shape =  bias_and_nodes[input_1]
                prev_shape = shape

                if (shape[0] == 0 and shape[1] ==-1):
                    shape_2=True
                else: shape_2=False
                if self.input_[0]  in weights_and_nodes:
                    input_ = self.input_[0]  # l'input va cercato tra gli initializer
                else: input_ = None
            else :
                shape = None
                input_= None
                shape_2 = False
            return shape , input_, shape_2

#---------------------------------------------------------------------------------------------------------------#
        def gemm_transB (self):
            node = self.node
            if  node.op_type == "Gemm":
                    for attr in node.attribute:
                            if attr.name == "transB":
                                    transB = attr.i
            else : transB = None
            return transB

#---------------------------------------------------------------------------------------------------------------#
        def gemm_transA (self):
            node = self.node
            if  node.op_type == "Gemm":
                    for attr in node.attribute:
                            if attr.name == "transA":
                                    transA = attr.i
                            else : transA = None
            else : transA = None
            return transA

#---------------------------------------------------------------------------------------------------------------#
        def conv_input_dim2 (self):
            node = self.node
            if (len(self.initializer.channels)>1):  ##### !!!!!! verificare che questa condizione funzioni bene o se si può mettere qualcos'altro !!!!!! # verifica che le informazioni sul numero di canali siano incluse all'interno del mondello onnx
                #le informazioni sui canali in input al layer vengono acquisite dal file del modello
                if node.op_type =="Conv" or node.op_type=="Gemm":

                    input_ = self.nodes_input()
                    for k in self.input_:
                            k = k.replace("/","_").replace(".","_")
                            if k in self.initializer.channels.keys():
                                    input_channels_n = self.initializer.channels[k][1]
                else:
                    input_channels_n = Generic_layer_Cpp.prev_layer_OF

            else:
                # le informazioni sui canali di input al layer non sono incluse nel modello e vengono calcolate a partire dai pesi
                node = self.node
                if node.op_type =="Conv" or node.op_type=="Gemm":

                        input_ = self.nodes_input()
                        weights = self.initializer.parameters
                        for k in self.input_:
                                k= k.replace("/","_").replace(".","_")
                                if k in weights.keys():
                                        input_channels_n = weights[k].shape[1]
                else:
                    input_channels_n = Generic_layer_Cpp.prev_layer_OF
                

            return input_channels_n
#-------------------------------------------------------------------------------------------------------------#
        def conv_output_dim2 ( self):
            node = self.node

            if node.op_type =="Conv" or node.op_type =="Gemm":
                k=self.input_[1]
                
                if k in self.initializer.parameters.keys():
                       
                        output_channels_n = self.initializer.parameters[k].shape[0]
                        output_channels_n_gemm = 0
                self.output_channels_prev_layer_ = output_channels_n
            else:
                output_channels_n = self.isizes[0]
                output_channels_n_gemm = None
            return output_channels_n , output_channels_n_gemm

#---------------------------------------------------------------------------------------------------------------#
        def constant (self):
            node = self.node
            if node.op_type == "Constant":

                for attribute in node.attribute:
                    if attribute.name == "value":
                        value = attribute.t.raw_data
                    else:
                        print ("Constant attribute not supported")
            else: value = None
            return value

#-----------------------------------------------------------------
        def pad_parameters(self):
            node = self.node
            if node.op_type == "Pad":
                for attribute in node.attribute:
                    if attribute.name == "mode":
                        mode = attribute.s
                        mode = mode.decode("utf-8")
                    elif attribute.name == "pads":
                        pads = attribute.ints[0]
                    elif attribute.name == "value":
                        value = attribute.f
            else :
                mode = None
                pads = None
                value = None
            return mode, pads, value


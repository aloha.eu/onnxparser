
# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.


import sys, math, os
import numpy as np
from .OnnxReader import OnnxReader

class ClipReader(OnnxReader):
        def __init__ (self, name, init, node=None, prev_layers=None, qf=8, datatype='h'):
          
          self.set_attributes(name, init, node=node, prev_layers=prev_layers, qf=qf, datatype=datatype)
        
          self.datatype='h'
          
          self.osizes = self.get_output_sizes()
          

          self.set_parameters()

        def set_parameters (self):
          self.clip_max, self.clip_max_f, self.clip_min, self.clip_min_f = self.clip_params()


        def clip_params (self):
          clip_max = 0
          clip_min = 0
          if  self.node.op_type == "Clip":
            for attr in self.node.attribute:
              if attr.name == "max":
                clip_max_f=attr.f
                clip_max = self.convert_float_to_short(attr.f,'h',self.qf) if self.datatype == 'h' else attr.f
              if attr.name == "min":
                clip_min_f=attr.f
                clip_min = self.convert_float_to_short(attr.f,'h',self.qf) if self.datatype == 'h' else attr.f
          
            return clip_max, clip_max_f, clip_min, clip_min_f
          else:
            return None, None

        



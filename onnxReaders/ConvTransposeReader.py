
# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.

import sys, math, os, struct
import numpy as np
from .OnnxReader import OnnxReader


class ConvTransposeReader(OnnxReader):
        def __init__ (self, name, init, node, prev_layers=None, qf=8, datatype='h'):
                
          self.set_attributes(name, init, node=node, prev_layers=prev_layers, qf=qf, datatype=datatype)
                
          self.initializer             = init

          self.net_input               = init.net_input        # contiene il nome dell'input della rete
          self.input_                  = self.nodes_input()[0] #
          #self.input_original          = self.nodes_input()[1]
          self.stride                  = self.layers_stride()
          self.kernel                  = self.layers_kernel()
          self.padding                 = self.layers_padding()
          self.pool_mode                 = 0 #inizializzato a zero, vale 1 solo per il pool di adattamento dello stride
          
          self.pads                    = self.get_pads()
          
          self.dilation                = self.layers_dilation()
          self.osizes = self.get_output_sizes()
                    
          
          

          self.output_                 = self.nodes_output ()

          self.group                   = 1

          self.axis                    = self.concat_axis()
          
          self.constant_param          = self.constant()
          

          self.reshape_params          = self.find_shape()
          self.shape                   = self.reshape_params[0]
          self.reshape_input           = self.reshape_params[1]
          self.shape_2                 = self.reshape_params[2]
          self.pad_params              = self.pad_parameters()       # Parametri dell operatore Pad
          self.pad_mode                = self.pad_params[0]          # None if the input of a reshape layer is not an initializer
          self.pad_pads                = self.pad_params[1]
          self.pad_value               = self.pad_params[2]


          self.activation              = False # for relu folding
          self.leaky_relu_alpha        = 0
          '''
#---------------definisco quali layer hanno bisogno del codice di inizializzazione #-----------------------------------------
          if ( self.operation == "Conv" or self.operation == "MaxPool" or self.operation == "Softmax"  or self.operation == "LogSoftmax"  or self.operation == "Add"  or self.operation == "Mul"  or self.operation == "Div" or self.operation == "Sigmoid"  or
           self.operation == "Dropout" or self.operation =="Relu" or self.operation =="BatchNormalization" or self.operation == "Concat"  or self.operation == "Floor"  or self.operation == "Clip"  or self.operation == "BitShift"  or
           self.operation =="Squeeze" or self.operation =="Unsqueeze" or self.operation == "Elu" or
           self.operation == "Constant" or self.operation == "Reshape" or self.operation == "AveragePool"or
           self.operation == "LRN" or self.operation =="Pad" or self.operation == "Gemm" or self.operation == "LeakyRelu"):
              self.to_be_initialized = True
          else :
              self.to_be_initialized = False
          '''
          
          self.datatype = datatype # h int 16 bit, f floating point
          
          self.estimate_datatype()
          
          
          
          self.output_features_size = 0
          
          
          self.weights = np.array(self.get_weights())
          self.biases  = np.array(self.get_biases())
          
          self.mulbn_name = None
          self.mulbn_scaler = None
          self.addbn_name = None
          self.addbn_bias = None
          self.mulrelu_name = None
          self.mulrelu_scaler = None
          self.divrelu_name = None
          self.divrelu_divider = None
          self.clip_max = None
          self.clip_min = None


#---------------------------------------------------------------------------------------------------------------#
        def layers_group (self):
            node = self.node
            no_group = True
            if node.op_type == "Conv" :
                no_group = True
                for attr in node.attribute:
                    if attr.name == "group":
                        group = attr.i
                        no_group = False
            if no_group == True :
                group = 1   # default vale for group in Conv layers
            return group

        def get_pads(self):
          if self.operation =="Pad":
            for attr in self.node.attribute:
              if attr.name == "pads":                
                return attr.ints
          return None

          
        def get_weights(self):
          if len(self.input_)>1:
            if self.operation == "BatchNormalization":
              if self.input_[1] in self.initializer.parameters.keys():
                return self.initializer.parameters[self.input_[1]]
              else:
                return None
            else:
              if self.input_[1] in self.initializer.parameters.keys():
                return self.initializer.parameters[self.input_[1]]
              else:
                return None
          else:
            return None
        
        def get_biases(self):
          if len(self.input_)>2:
            if self.operation == "BatchNormalization":
              if self.input_[2] in self.initializer.parameters.keys():
                return self.initializer.parameters[self.input_[2]]
              else:
                return None
            
            else:
              if self.input_[2] in self.initializer.parameters.keys():
                return self.initializer.parameters[self.input_[2]]
              else:
                return None
          else:
            return np.zeros(self.osizes[0])
      

        def get_output_sizes(self):
         
          OF = self.conv_output_dim2()[0]
          osizes=[]
          osizes.append(OF)
          
          ndims = len(self.isizes)-1
          for i, s in enumerate(self.isizes[1:]):
          #  osizes.append ((s - self.kernel[i] + self.padding[i*ndims] + self.padding[i*ndims+1])//self.stride[i] + 1)
            osizes.append ((s-1)*self.stride[i] - 2*self.padding[i*ndims+1] + self.dilation*(self.kernel[i]-1) +1)

          
          return osizes
          
#---------------------------------------------------------------------------------------------------------------#
        def layers_stride (self):
            node = self.node

            lenght = (len(node.attribute))
            i = 0
            for attr in node.attribute:
              if (i < lenght) :
                if node.attribute[i].name == "strides":
                  stride =node.attribute[i].ints
                  break
                else : i = i+1
                
            return stride


#---------------------------------------------------------------------------------------------------------------#
        def layers_dilation (self):
            node = self.node
            no_dilations = True
            if node.op_type == "Conv" :
                no_dilations = True
                for attr in node.attribute:
                    if attr.name == "dilations":
                        dilations = attr.ints[0]
                        no_dilations = False
            if no_dilations == True :
                dilations = 1   # default vale for group in Conv layers
            return dilations

#---------------------------------------------------------------------------------------------------------------#
        def layers_padding (self):
            node = self.node
            padding = [0,0,0,0]

            for attr in node.attribute:
                    if (attr.name == "pads"):
                       
                        padding[0]=attr.ints[0]
                        padding[1]=attr.ints[1]
                        padding[2]=attr.ints[2]
                        padding[3]=attr.ints[3]
                        
                    elif (attr.name == "auto_pad" ):
                        pad = attr.s
                        pad =pad.decode("utf-8")
                        if pad == "VALID":
                            padding =[0,0,0,0]
                        elif pad == "NOTSET":
                            break
                        else: padding = pad
            else :
                   padding[0] = 0
                   padding[1] = 0
                   padding[2] = 0
                   padding[3] = 0
                   
            if padding == 'SAME_UPPER':   # output input feature should have same dimensions of input feature
                kernel = self.kernel
                padding =[0,0,0,0]
                pad = (kernel-1)/2
                pad = int(pad)
                padding[0] = pad
                padding[1] = pad
                padding[2] = pad
                padding[3] = pad
                if node.op_type == "MaxPool": #FIXME: this works only with few cases
                  padding =[0,0,0,0]
                  padding[0] = (kernel-self.stride)%2 #x begin
                  padding[1] = pad #x end
                  padding[2] = (kernel-self.stride)%2 #y begin
                  padding[3] = pad #y end
                
            return padding

#---------------------------------------------------------------------------------------------------------------#
        def layers_kernel (self):
            node = self.node
            

            lenght = (len(node.attribute))
            for attr in node.attribute:
              if attr.name == "kernel_shape":
                kernel = attr.ints
                break
                
            return kernel

#---------------------------------------------------------------------------------------------------------------#
        def concat_axis (self):
            node = self.node
            i = 0
            if  node.op_type == "Concat":
                    for attr in node.attribute:
                            if node.attribute[i].name == "axis":
                                    param = node.attribute[i].i
                                    break
                            else:  i = i+1
            else : param = None
            return param

#---------------------------------------------------------------------------------------------------------------#
        def find_shape(self):
            global prev_shape
            node = self.node
            if node.op_type == "Reshape":
                bias_and_nodes = self.initializer.parameters
                weights_and_nodes = self.initializer.parameters
                input_1 = self.input_[1]
                shape =  bias_and_nodes[input_1]
                prev_shape = shape

                if (shape[0] == 0 and shape[1] ==-1):
                    shape_2=True
                else: shape_2=False
                if self.input_[0]  in weights_and_nodes:
                    input_ = self.input_[0]  # l'input va cercato tra gli initializer
                else: input_ = None
            else :
                shape = None
                input_= None
                shape_2 = False
            return shape , input_, shape_2


#-------------------------------------------------------------------------------------------------------------#
        def conv_output_dim2 ( self):
            node = self.node


            k=self.input_[1]
            if k in self.initializer.parameters.keys():
                    output_channels_n = self.initializer.parameters[k].shape[1] # 1 because in Convstranspose the output dimension is the second
                    output_channels_n_gemm = 0
            self.output_channels_prev_layer_ = output_channels_n
            
            return output_channels_n , output_channels_n_gemm

        


#---------------------------------------------------------------------------------------------------------------#
        def constant (self):
            node = self.node
            if node.op_type == "Constant":

                for attribute in node.attribute:
                    if attribute.name == "value":
                        value = attribute.t.raw_data
                    else:
                        print ("Constant attribute not supported")
            else: value = None
            return value


#-----------------------------------------------------------------
        def pad_parameters(self):
            node = self.node
            if node.op_type == "Pad":
                for attribute in node.attribute:
                    if attribute.name == "mode":
                        mode = attribute.s
                        mode = mode.decode("utf-8")
                    elif attribute.name == "pads":
                        pads = attribute.ints[0]
                    elif attribute.name == "value":
                        value = attribute.f
            else :
                mode = None
                pads = None
                value = None
            return mode, pads, value
            
                


# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.


import sys, math, os
import numpy as np
from .OnnxReader import OnnxReader

class PadReader(OnnxReader):
        def __init__ (self, name, init, node=None, prev_layers=None, qf=8, datatype='h'):
         # print (node.name)
          self.set_attributes(name, init, node=node, prev_layers=prev_layers, qf=qf, datatype=datatype)
          
          self.datatype = prev_layers[0].datatype
          
          if len(prev_layers)>1:
            ndim = len(prev_layers[0].osizes)
            sizesmax = []
            sizesmax.append (self.prev_layers[0].osizes[0])
            for i in range (1, ndim):
              s[i] = [l.osizes[i] for l in prev_layers]
              
              sizesmax.append (max(s[i]))
              
            self.prev_layers = prev_layers
            self.isizes = sizesmax
          else:
            #we assume the first input is always the main flow
            if self.input_ is not None:
              for i, inp in enumerate(self.input_):
                if inp == self.prev_layers[0].output_[0]:
                  self.input_[0],self.input_[i]=self.input_[i],self.input_[0]
              
          if self.node is not None:
            self.padding                 = self.layers_padding_pad()
            self.osizes = self.get_output_sizes()

        def layers_padding_pad (self):
            node = self.node
            padding = [0,0,0,0]
            if node.op_type == "Pad":
                for attr in node.attribute:
                    if (attr.name == "pads"):

                        padding[0]=attr.ints[2]
                        padding[1]=attr.ints[6]
                        padding[2]=attr.ints[3]
                        padding[3]=attr.ints[7]
                        
                    elif (attr.name == "auto_pad" ):
                        pad = attr.s
                        pad =pad.decode("utf-8")
                        if pad == "VALID":
                            padding =[0,0,0,0]
                        elif pad == "NOTSET":
                            break
                        else: padding = pad
            else :
                   padding[0] = 0
                   padding[1] = 0
                   padding[2] = 0
                   padding[3] = 0
                   
            if padding == 'SAME_UPPER':   # output input feature should have same dimensions of input feature
                kernel = self.kernel
                padding =[0,0,0,0]
                pad = (kernel-1)/2
                pad = int(pad)
                padding[0] = pad
                padding[1] = pad
                padding[2] = pad
                padding[3] = pad
            
            return padding
          
          
        def get_output_sizes(self):
          
          OF = self.conv_output_dim2()[0]
          osizes=[]
          osizes.append(OF)
          
          ndims = len(self.isizes)-1
          for i, s in enumerate(self.isizes[1:]):
            osizes.append (s + self.padding[i*ndims] + self.padding[i*ndims+1])

          
          return osizes
          
          
          


# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.


import sys, math, os
import numpy as np
from .OnnxReader import OnnxReader

class CropReader(OnnxReader):
        def __init__ (self, name, init, node=None, prev_layers=None, qf=8, datatype='h'):
         # print (node.name)
          self.set_attributes(name, init, node=node, prev_layers=prev_layers, qf=qf, datatype=datatype)
          
          if len(prev_layers)>1:
            ndim = len(prev_layers[0].osizes)
            sizesmax = []
            sizesmax.append (self.prev_layers[0].osizes[0])
            for i in range (1, ndim):
              s[i] = [l.osizes[i] for l in prev_layers]
              
              sizesmax.append (max(s[i]))
              
            self.prev_layers = prev_layers
            self.isizes = sizesmax
          else:
            #we assume the first input is always the main flow
            if self.input_ is not None:
              for i, inp in enumerate(self.input_):
                if inp == self.prev_layers[0].output_[0]:
                  self.input_[0],self.input_[i]=self.input_[i],self.input_[0]
              
          self.crop                 = [0,0,0,0]
          if self.node is not None:
            self.osizes = self.get_output_sizes()

          
        def get_output_sizes(self):
          
          OF = self.conv_output_dim2()[0]
          osizes=[]
          osizes.append(OF)
          
          ndims = len(self.isizes)-1
          for i, s in enumerate(self.isizes[1:]):
            osizes.append (s + self.crop[i*ndims] + self.crop[i*ndims+1])

          
          return osizes
          
          
          

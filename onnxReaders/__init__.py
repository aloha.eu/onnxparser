from .ConvReader import ConvReader
from .OnnxReader import OnnxReader
from .MaxPoolReader import MaxPoolReader
from .GlobalAveragePoolReader import GlobalAveragePoolReader
from .ReluReader import ReluReader
from .GemmReader import GemmReader
from .AddReader import AddReader
from .MulReader import MulReader
from .DivReader import DivReader
from .Float2FixedReader import Float2FixedReader
from .Fixed2FloatReader import Fixed2FloatReader
from .ClipReader import ClipReader
from .BatchNormalizationReader import BatchNormalizationReader
from .FlattenReader import FlattenReader
from .CastReader import CastReader
from .ConcatReader import ConcatReader
from .RegionReader import RegionReader
from .DropoutReader import DropoutReader
from .SigmoidReader import SigmoidReader
from .PadReader import PadReader
from .CropReader import CropReader
from .ConvTransposeReader import ConvTransposeReader
from .TransposeReader import TransposeReader
from .ReshapeReader import ReshapeReader

mapping = {"Conv"               : ConvReader,
           "Gemm"               : GemmReader,
           "MaxPool"            : MaxPoolReader,
           "AveragePool"        : MaxPoolReader,
           "GlobalAveragePool"  : GlobalAveragePoolReader,
           "Relu"               : ReluReader,
           "LeakyRelu"          : ReluReader,
           "Add"                : AddReader,
           "Mul"                : MulReader,
           "Div"                : DivReader,
           "Clip"               : ClipReader,
           "BatchNormalization" : BatchNormalizationReader,
           "Flatten"            : FlattenReader,
           "Cast"               : CastReader,
           "Concat"             : ConcatReader,
           "Float2Fixed"        : Float2FixedReader,
           "Fixed2Float"        : Fixed2FloatReader,
           "Region"             : RegionReader,
           "Dropout"            : DropoutReader,
           "Sigmoid"            : SigmoidReader,
           "Pad"                : PadReader,
           "Crop"                : CropReader,
           "ConvTranspose"      : ConvTransposeReader,
           "Transpose"          : TransposeReader,
           "Reshape"            : ReshapeReader,
    }
    
    
supported_operators=["Conv", "Gemm", "MaxPool", "AveragePool", "GlobalAveragePool", "Relu", "LeakyRelu", "Pad", "Crop", "Fixed2Float","Float2Fixed", "Flatten","Dropout",
                     "Add", "Mul", "Div", "BatchNormalization", "Region", "Sigmoid", "Concat", "Floor", "BitShift", "Clip", "Constant", "Cast",
                     "ConvTranspose", "Reshape", "Transpose"]
         

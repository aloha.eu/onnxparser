
# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.

from Initializer import *
import sys, math, os
import numpy as np
import onnxReaders as rd
import cLKAMWriters as wr


class NetWriterCLKAM():

        def __init__ (self, model_file, path, app_name,  weights_path, readers, init, graph, input_size, **kwargs):
          
          try:
            qf = kwargs['qf']
          except:
            qf = 8
            
          try:
            yolo_like = kwargs['yolo_like']
          except:
            yolo_like = False
            
          self.app_name = app_name
          self.path = path
          self.smart_memalloc = False
          
          self.readers = readers
          self.weights_path = weights_path
          
          self.model_file     = model_file

          self.yolo_like      = yolo_like
          self.qf             = qf
          
          self.init     = init
          
          self.input_size     = input_size
          
          self.net = []
          

          i=0
          for r in readers:
                       
            if r.operation in wr.mapping.keys():
              w = wr.mapping[r.operation](r)
              self.net.append(w)
            elif r.operation not in wr.supported_operators:
              print ("Operator not supported: {} in {}".format(r.operation, r.name))
              exit(1)
            else:
              print ("Skipping operator: {} in {}".format(r.operation, r.name))
            
            
        
        def optimizeNet (netReader, batch_norm=False, neuraghe=False):
           netReader.removePreProcessing(['Conv'])
           netReader.removeOperators(['Cast', 'Dropout', 'Flatten'])
           netReader.batch_norm_folding(batch_norm)
           netReader.set_lkam_inference()
           netReader.md2shift()
           netReader.mul_rshift_folding()
           netReader.stride2subsampling(neuraghe)
          # netReader.datatype_conversion()

            
            
            
            
            
            
            
        def save_parameters(self):
        
            print ("Extracting weights...")
            
            os.makedirs(self.weights_path, exist_ok=True)

            for layer in self.net:
              layer.save_parameters(self.weights_path)
          
          
            maxog = 4
            conv_line = '{"name":"%s","weights":{"file":"%s","size":[%d,%d,%d,%d]},"bias":{"file":"%s","size":[%d]}, "maxog":%d}\n'
            with open(os.path.join(self.weights_path,"conv.json"), 'w') as f:
              for layer in self.net:
                if layer.operation=="Conv": # and layer.weights != None and layer.biases != None:
                  w = layer.name + "_weights.bin"
                  b = layer.name + "_biases.bin"
                  wshape = [layer.osizes[0], layer.isizes[0], layer.kernel, layer.kernel]
                  bshape = [layer.osizes[0]]
                  
                  f.write(conv_line%(layer.name, w, wshape[0], wshape[1], wshape[2], wshape[3], b, bshape[0], maxog))
        
          
      
        
        
        
        
        
        
        def net_description(self):
           
            net_description = "Source file: %s\n\n" % self.model_file
            net_description +="%-20s %5s %5s %5s %5s %5s %5s\n"%("Layer", "IF", "ih", "iw", "OF", "oh", "ow")
            
            for layer in self.net:
                net_description+= "%-20s %5d %5d %5d %5d %5d %5d\n"%(layer.name, layer.isizes[0], layer.isizes[1], layer.isizes[2], layer.osizes[0], layer.osizes[1], layer.osizes[2])
            return net_description
            
            
            
         
        def get_mem_allocation_string(self):

          string = ""

          for layer in self.net:
            if layer.operation in rd.supported_operators:
              if layer.operation=="Region":
                string+=""
              else:
                size = layer.get_output_pixels()                
                dt= "float" if layer.datatype=='f' else "DATA"
                string += "{}_param->output = ({}*)malloc({}*sizeof({}));\n".format(layer.name, dt, size, dt)
          
          string += "\n"
          return string
          
          
          
          
          
        def get_smart_mem_allocation_string(self):
        #  print (self.osizes[0])
          names = ['A','B','C']
          
          string =  "DATA * %s = (DATA*)malloc(%d*sizeof(DATA));\n" % (names[0],5000000)
          string += "DATA * %s = (DATA*)malloc(%d*sizeof(DATA));\n" % (names[1],5000000)
          string += "DATA * %s = (DATA*)malloc(%d*sizeof(DATA));\n" % (names[2],5000000)
  
          #allocate space for the output
          i=0
          for layer in self.net:            
            if layer.operation in rd.supported_operators:
              if layer.operation=="Region":
                string+=""
              else:
                string += "%s_param->output = %s;\n" % (layer.name, names[i])
                i+=1
                if i==3:
                  i=0
            ##    if (layer.operation=="Conv" and layer.stride>1):
            ##       string += "%s_Max_param->output = %s;\n" % (layer.name, names[i])  
            ##    i+=1
            ##    if i==3:
            ##      i=0
          string += "\n"
          return string
          
#---------------------------------------------------------------------------------------------------------------------------------------------
        def print_net(self):
        
                
                name_file        = os.path.join(self.path, self.app_name +".cpp")
                
                print (self.net_description())
                
                print ("Creating file {} ...".format(name_file))

                file_name_weights = "weights.npz"
                file_name_bias = "bias.npz"
                name_file = open(name_file,"w")
                name_file.write("\n//This code is auto_generated.//\n")
                name_file.write("\n//This file must be embedded in the host application that will provide the data to be processed.\n\n\n\n")
                name_file.write("/* ################## Net description ###################\n")
                name_file.write(self.net_description())
                name_file.write("#########################################################*/\n")
                name_file.write("\n#include "'"%s.h"'" \n"%self.app_name)
                name_file.write("#include "'"assert.h"'"\n")
                name_file.write("#define ELEM_PER_WS 32\n")
                name_file.write("\n// global variables\n\n")
                name_file.write("#if !(_RELEASE_)\n")
                name_file.write("     #define _rprintf_(...) printf(__VA_ARGS__)\n")
                name_file.write("     #define spatconv_forward_hw(...) spatconv_forward_hw_sync(__VA_ARGS__)\n")

                name_file.write("     #ifdef _DEBUG_\n")
                name_file.write("         #define _dprintf_(...) printf(__VA_ARGS__)\n")
                name_file.write("         #define _dprint_data_(...) print_data(__VA_ARGS__)\n")

                name_file.write("        #else\n")
                name_file.write("         #define _dprintf_(...) ;\n")
                name_file.write("         #define _dprint_data_(...) ;\n")

                name_file.write("        #endif\n")
                name_file.write("#else\n")
                name_file.write("       #define _rprintf_(...) ;\n")
                name_file.write("       #define _dprintf_(...) ;\n")
                name_file.write("       #define _dprint_data_(...) ;\n")

                name_file.write("#endif\n\n")
                name_file.write("\n// global variables\n")

                
                name_file.write("DATA* wPointer;\n")
                name_file.write("DATA* aPointer;\n")

#------------stampo il codice per dichiarare e inizializzare  le strutture dei layer  conv e gemm (fully connected layer) -------------------------------------------------------

                for layer in self.net:
                    name_file.write(layer.get_declaration_string())  
                    
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

                name_file.write("\n\n")
                name_file.write("\nvoid getInputSize(VARSIZE* inSz)\n{ // CHW\n")
                
                for i,d in enumerate(self.input_size):
                  name_file.write("        inSz[{}]={};\n".format(i,d))
                  
                name_file.write("  }\n")
                name_file.write("\n\n")
                name_file.write("\nvoid getOutputSize(VARSIZE* output_size)\n{ \n")
                name_file.write("        *output_size={};\n".format(self.net[-1].osizes[0]))
                name_file.write("  }\n")
                
#----------------------------------------------- main init --------------------------------------------------------------------------------------------------------------------------------------
                name_file.write("\nvoid cnnMainInit(VARNAME load_data_dir)\n{")
                name_file.write("        int minIF=0;\n")
                name_file.write("        int wsize=0;\n")
                name_file.write("        int minOF=0;\n")
                name_file.write("        int IG=0, OG=0;\n")

                name_file.write("VARNAME filename;\n")


                for layer in self.net:
                    if layer.to_be_initialized:
                        name_file.write( layer.get_initialization_string())
                        
                        
                name_file.write("\n\n// ##################################################################\n")
                name_file.write("// ######################## WEIGHTS LOADING #######################\n")
                name_file.write("// ################################################################\n\n")
                
                name_file.write("//\tOnly convolutional layers and FC or GEMM layers have weights and biases\n")
                name_file.write("//\tConvolutional layers will be accelerated and their weights must be stored in the DDR ram chunck accessible by the soc. \n")
                name_file.write("//\tThe FC and GEMM weights and biases may be stored in any place \n\n")
                
                for layer in self.net:
                    name_file.write(layer.get_weights_loading_string())
                
                        
                name_file.write("\n\n// ########################################################\n")
                name_file.write("// ######################## MEMORY ALLOCATION #######################\n")
                name_file.write("// #######################################################\n\n")
                
                name_file.write("//\tEach layer produces an output. By default the output is stored in a dedicated DDR segment accwssible by the accelerator.\n")
                name_file.write("//\tWe assume that the output of a layer will be the input of the netx so we do not need to allocate RAM for the input.\n")
                name_file.write("//\tBy editing this you can customize the memory utilization\n\n")
                
                
                # allocate space for the first input
                string = ""                
                size = np.prod(self.net[0].isizes)
                string = "%s_param->input = (DATA*)malloc(%d*sizeof(DATA));" % (self.net[0].name, size)
                string += "\n"
                name_file.write(string)

                
                #allocate space for the output
                
                if (self.smart_memalloc):
                  name_file.write(self.get_smart_mem_allocation_string())
                else:
                  name_file.write(self.get_mem_allocation_string())     
   
                name_file.write("\n\n// ########################################################\n")
                name_file.write("// ######################## WIRING #######################\n")
                name_file.write("// #######################################################\n\n")
                
                name_file.write("//\tEach layer has an input and an output pointer. The connection between two self.net is defined here\n\n")
                

                for layer in self.net:
                    name_file.write(layer.get_wiring_string())

                    
                


                name_file.write("\n\n")
                name_file.write("\n}")
                name_file.write("\n\n//#########################################################################################################################################\n")
                name_file.write("//#########################################################################################################################################\n\n")
                name_file.write("\nvoid cnnMain(DATA* image, DATA** results)\n")
                name_file.write("{\n")





                s = "SIZE input_size[] = {{{},{},{}}}; \n".format(layer.initializer.model_input_shape[1], layer.initializer.model_input_shape[2], layer.initializer.model_input_shape[3])
                s1= "SIZE batch_join_dim = {} * {} * {};\n".format(layer.initializer.model_input_shape[1], layer.initializer.model_input_shape[2], layer.initializer.model_input_shape[3])
               # s2 = "DATA* {} = (DATA*)neu_DDRmalloc(batch_join_dim*sizeof(DATA));\n\n".format(self.initializer.net_input)
                s2= "for (int j=0;j<batch_join_dim;j++)\n  {}_param->input[j]=image[j];\n\n".format(self.net[0].name)
               # s2+= "{}_param->input={};\n\n".format(self.net[0].name, self.initializer.net_input)
                name_file.write(s)
                name_file.write(s1)
                name_file.write(s2)
                #name_file.write("int job_id = 0; \n")
                #name_file.write("int job_id_sc = 0;\n")


#------------------stampo la funzione main con i layer che compongono la struttura della rete --------------------------------------------------


                for layer in self.net:

                    name_file.write(layer.get_forward_string())
#------------------------------------------------------------------------------------------------------------------------------------------------

                name_file.write("\n\n")
                global output_name
                
                last_layer=-1
                if self.net[-1].operation == "Region" or self.net[-1].operation == "Softmax" or self.net[-1].operation == "LogSoftmax":
                  last_layer-=1
                
                if (self.net[last_layer].operation == "Conv"):
                    s="DATA* res = (DATA *) calloc({}, sizeof(DATA));\n".format(self.net[last_layer].get_output_pixels())
                    s += "for(unsigned int i = 0; i < {}; i++){{\n".format(self.net[last_layer].get_output_pixels())
                   # s4 = "	results[i] = (float) ({}[i] / (1<<5));\n".format(self.net[last_layer].output_[0])
                    s+= "	res[i] = {}_param->output[i] ;\n".format(self.net[last_layer].name)
                    s+= "}\n"
                    s+="*results = res;\n"
                    s+= "}\n\n"
                else:
                    s="DATA* res = (DATA *) calloc({}, sizeof(DATA));\n".format(self.net[last_layer].get_output_pixels())
                    s+=  "for(unsigned int i = 0; i < {}; i++){{\n".format(self.net[last_layer].get_output_pixels())
                    
                    #i=-1
                 #   while self.net[i].operation not in rd.supported_operators:
                 #     i-=1
                      
                    s+=  "\tres[i] = {}_param->output[i];\n".format(self.net[last_layer].name)
                    s+= '//\t_dprintf_("[%d] %d \\n", i, {}_param->output[i]);\n'.format(self.net[last_layer].name)
                      
                    s+= "}\n"
                    s+= "*results = res;\n"
                    s+= "}\n"
                name_file.write(s)
                
                
                s= "\n\nvoid getInputImage(VARNAME image_path, DATA* image_pixels, VARSIZE* inSz){\n"
                s+="// This function is executed for each file in the images folder.\n"
                s+="// After you load the content of the file you can perform whatever preprocessing you want\n\n" 
                
                
                s+="  int input_type;\n\n"
  
                s+="  input_type= /*input_type here*/; //set the input type, 0 binary, 1 txt floating point, 2 JPEG to be normalized with imagenet normalization\n"
                s+="                                   //                    1 txt floating point, 2 JPEG to be normalized with imagenet normalization\n"
                s+="                                   //                    2 JPEG to be normalized with imagenet normalization\n"
                s+="                                   //                    3 JPEG with letterbox format (yolo-like), no stretch\n\n"

                s+="  switch (input_type){\n"
                s+="    case 0:\n"
                s+="      /*load_fixed expects images stored in binary format, the data shape (CHW or HWC), the data size (16 bit or 8 bit per pixel) */\n"
                s+="      /*and the representation (0 - 255; 0 - 1; -1 - 1) depends on the model and on the way it has been trained.*/\n"
                s+="      load_fixed(image_path,{},image_pixels);// loads binary files\n".format(np.prod(self.net[0].isizes))
                s+="      break;\n"
                s+="    case 1:\n"
                s+="      // loads txt files applying a normalization (the value depends on the dataset. Often it is the maximum value, for example 255.0) \n"
                s+="      load_float_txt(image_path,{},image_pixels,{}, /*set a nomalization value*/);\n".format(np.prod(self.net[0].isizes), self.qf)
                s+="      break;\n"
                s+="    case 2:\n"
                s+="      source_forward(image_path, image_pixels, inSz, {}); // preprocesses JPG images \n".format(self.qf)
                s+="      break;\n"
                s+="    case 3:\n"
                s+="      source_forward_letterbox(image_path, image_pixels, inSz, {}); // preprocesses JPG images \n".format(self.qf)
                s+="      break;\n"
                s+="    default:\n"
                s+='      printf ("Wrong input type!\\n");\n'
                s+="      exit(1);\n"
                s+="      break;\n"
                s+="  }\n"
                s+="}\n"
                
                name_file.write(s)
                
                
                s="\n\nint resultsProcessing(DATA* results, int size){\n"
                s+="//What do you want to do with the results of the CNN? Here is the place where you should put the classifier or the detection (see YOLO detection for example)\n"
                s+="//The simplest classifier is a maximum search for the results which returns the index value of the maximum\n\n"


                if self.yolo_like:
                  s+=self.get_region()
                else:
                  s+=self.get_classifier()
                s+="}\n"
                
                
                name_file.write(s)
                
     
     
     
        def get_classifier(self):
          
          
          l= ' char *labels[%d]={'%self.net[-1].osizes[0]

          for i in range (0,self.net[-1].osizes[0]):
            l+='"label_%d", '%i
              
          l=l[0:-2]
          l+='};\n\n'

          s=l
          s+="// TODO: check the size parameter\n" 
          s+="  int size_wa = {};\n".format(self.net[-1].osizes[0])
          
          s+="  float* r= (float*) malloc (size_wa*sizeof(float));\n"
          s+="  int*  c= (int*)  malloc (size_wa*sizeof(int));\n"
          s+="  float* results_float= (float*) malloc (size_wa*sizeof(float));\n"

          s+="  float sum=0.0;\n"
          
          
          s+="  DATA max=0;\n"
          s+="  for (int i =0;i<size_wa;i++){\n"
          s+="      results_float[i] = FIXED2FLOAT(results[i],{});\n".format(self.qf)
          s+="    int n;\n"
          s+="    if (results[i]>0)\n"
          s+="      n=results[i];\n"
          s+="    else\n"
          s+="      n=-results[i];\n"
      
          s+="    if (n>max){\n"
          s+="      max=n;\n"
          s+="    }  \n"
          s+="  }\n"
          
          
          s+="  for (int i =0;i<size_wa;i++)\n"
          s+="    sum+=exp(results_float[i]);\n\n"
          
          s+="  for (int i =0;i<size_wa;i++){\n"
          s+="    r[i]=exp(results_float[i]) / sum;\n"
          s+="    c[i]=i;\n"
          s+="  }\n"
          
          s+="  for (int i =0;i<size_wa;i++){\n"
          s+="    for (int j =i;j<size_wa;j++){\n"
          s+="      if (r[j]>r[i]){\n"
          s+="        float t= r[j];\n"
          s+="        r[j]=r[i];\n"
          s+="        r[i]=t;\n"
          s+="        int tc= c[j];\n"
          s+="        c[j]=c[i];\n"
          s+="        c[i]=tc;\n"
          s+="      }\n"
          s+="    }\n"
          s+="  }\n"
          
          s+="  int top0=0;\n"
          s+="  float topval=results_float[0];\n"
          s+="  for (int i =1;i<size_wa;i++){\n"          
          s+="    if (results_float[i]>topval){\n"
          s+="      top0=i;\n"
          s+="      topval=results_float[i];\n"
          s+="    }  \n"
          s+="  }\n"
          
          
          s+='  printf("\\n\\n");\n'
          s+="  for (int i =0;i<5;i++){\n"
          s+='    printf("            TOP %d: [%d] %s   ",i, c[i], labels[c[i]]);\n'
          s+="    for (int j =0;j<100*r[i];j++)\n"
          s+='      if (j<40) printf("#");\n'
          s+='    printf(" %0.1f%\\n",r[i]*100);\n'
          s+="  }  \n"
          s+='  printf("\\n\\n\\n\\n\\n");\n'
          
          s+='  return top0;\n\n\n'         
          
        
          return s
          
          
        def get_region(self):
                        
          s =  "region_param->input = (float*)malloc({} * sizeof(float));\n\n".format (self.net[-1].get_output_pixels())

          s += "for (int i = 0; i<{};i++){{\n".format (self.net[-1].get_output_pixels())

          s += "  region_param->input[i]=FIXED2FLOAT(results[i],{});\n".format(self.qf)
          s += "}\n\n"


          s += "float a[]={1.08,1.19,3.42,4.41,6.63,11.38,9.42,5.11,16.62,10.52};//fixme: get them from a file\n"

          s += "//region_param->output=(float*)malloc(0 * sizeof(float));\n"
          s += "region_param->anchors=a;\n"

          s += "region_forward_wrap(region_param);\n"
          
          
          s += 'return -1;\n\n\n'
        
          return s
          

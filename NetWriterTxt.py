
# Copyright (c) 2018
# by Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.

from Initializer import *
import sys, math, os
import numpy as np
import onnxReaders as rd
import pytorchWriters as wr


class NetWriterTxt():

        def __init__ (self, model_file, path, app_name,  weights_path, readers, init, graph, input_size, **kwargs):
          
          try:
            softmax      = kwargs['softmax']
          except:
            softmax = False
          
          try:
            yolo_like      = kwargs['yolo_like']
          except:
            yolo_like = False
          
          self.app_name = app_name
          self.path = path
          
          self.readers = readers
          self.weights_path = weights_path
          
          self.model_file     = model_file

          self.yolo_like      = yolo_like
          
          self.load_params = kwargs.get('load_params', True)

          
          self.init     = init
          
          self.input_size     = input_size
          
          self.net = []
          self.softmax_layer=softmax

          i=0
          for r in readers:
                       
            if r.operation in wr.mapping.keys():
              print (r.operation)
              w = wr.mapping[r.operation](r)
              self.net.append(w)
            elif r.operation not in wr.supported_operators:
              print ("[WRITER] Operator not supported: {} in {}".format(r.operation, r.name))
              exit(1)
            else:
              print ("[WRITER] Skipping operator: {} in {}".format(r.operation, r.name))
            
            
            
          #self.init.write_weights()
          #self.init.write_parameters()
            
            
            
            
        #class method
        def optimizeNet (netReader, batch_norm=False, neuraghe=False):
          netReader.removePreProcessing(['Conv','Pad'])
          netReader.removeOperators(['Cast'])
          netReader.batch_norm_folding(batch_norm)
          netReader.add_flatten()
          netReader.remove_redundant_Pad()
          netReader.padlayer_folding()
          pass
        
        
        
        def save_parameters(self):
          pass
        
          
      

        
        def net_description(self):
           
          ndim = len(self.net[0].isizes)
          
          formatters="".join(["%5s\t"]*ndim)
          
            
          description  = "#Source file: %s\n\n" % self.model_file
          description += "%-20s\t" % ("Layer")
          description += "%-20s\t" % ("Operator")
          description += "%5s\t"%"IF"
          for s in range (1,ndim):
           description += "%5s\t"%("ix{}".format(s-1))
          
          description += "%5s\t"%"OF"
          
          for s in range (1,ndim):
           description += "%5s\t"%("ox{}".format(s-1))
           
          description += "%10s\t" % ("Kernel")
          description += "%10s\t" % ("Stride")
          description += "%10s\t" % ("Pad")
           
          description += "\n"
          
          
          for layer in self.net:
              description += "%-20s\t%-20s\t" + formatters + formatters + "%10s\t%10s\t%10s\t\n"
              description  = description%(layer.name, layer.operation, *layer.isizes, *layer.osizes, layer.kernel, layer.stride, layer.padding)
          return description
            
            
        

#  ███╗   ██╗███████╗████████╗
#  ████╗  ██║██╔════╝╚══██╔══╝
#  ██╔██╗ ██║█████╗     ██║   
#  ██║╚██╗██║██╔══╝     ██║   
#  ██║ ╚████║███████╗   ██║   
#  ╚═╝  ╚═══╝╚══════╝   ╚═╝   
#                             

        def print_net(self, tool_specific=None):
            import os

            with open(os.path.join(self.path, "Net.txt"),"w") as txtfile:
              txtfile.write("#----This code is auto-generated.----#\n\n")
              txtfile.write (self.net_description())


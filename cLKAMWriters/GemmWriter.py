
# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.


import sys, math, os, struct
import numpy as np
from .CWriter import CWriter


class GemmWriter(CWriter):
        def __init__ (self,  reader):

          super().__init__(reader)
        

            
#  ███████╗ █████╗ ██╗   ██╗███████╗    ██████╗  █████╗ ██████╗  █████╗ ███╗   ███╗
#  ██╔════╝██╔══██╗██║   ██║██╔════╝    ██╔══██╗██╔══██╗██╔══██╗██╔══██╗████╗ ████║
#  ███████╗███████║██║   ██║█████╗      ██████╔╝███████║██████╔╝███████║██╔████╔██║
#  ╚════██║██╔══██║╚██╗ ██╔╝██╔══╝      ██╔═══╝ ██╔══██║██╔══██╗██╔══██║██║╚██╔╝██║
#  ███████║██║  ██║ ╚████╔╝ ███████╗    ██║     ██║  ██║██║  ██║██║  ██║██║ ╚═╝ ██║
#  ╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚══════╝    ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝

        def save_parameters(self, path):
          from tqdm import tqdm
          print ("\n"+self.reader.name)
          if self.reader.operation == "Gemm" or self.reader.operation == "Conv":
            if self.reader.weights is not None:
              filename = self.reader.name + "_weights.bin"
              with open(os.path.join(path, filename), "wb") as f:
                
                for wb in tqdm(self.reader.weights.flatten(), bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                 # print (wb)
                 # print (self.reader.convert_float_to_short(wb,'h',self.reader.qf))
                  w_byte=struct.pack("h",self.reader.convert_float_to_short(wb,'h',self.reader.qf))
                    
                  f.write(w_byte)
             
              filename = self.reader.name + "_biases.bin"
              if self.reader.biases is not None: # check if biases exsist
                biases = self.reader.biases
              else:
                biases = np.zeros(self.reader.osizes[0])
              with open(os.path.join(path, filename), "wb") as f:
                for wb in tqdm(biases, bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                  b_byte=struct.pack("h",self.reader.convert_float_to_short(wb,'h',self.reader.qf))
                  f.write(b_byte)
            
         
                  
           

#  ██████╗ ███████╗ ██████╗██╗      █████╗ ██████╗ ███████╗
#  ██╔══██╗██╔════╝██╔════╝██║     ██╔══██╗██╔══██╗██╔════╝
#  ██║  ██║█████╗  ██║     ██║     ███████║██████╔╝█████╗  
#  ██║  ██║██╔══╝  ██║     ██║     ██╔══██║██╔══██╗██╔══╝  
#  ██████╔╝███████╗╚██████╗███████╗██║  ██║██║  ██║███████╗
#  ╚═════╝ ╚══════╝ ╚═════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝
#                                                          

#---------------------------------------------------------------------------------------------------------------#
        def get_declaration_string(self):
            string = ''
           
            if self.reader.operation == "Gemm":
              if ('IF_flag' not in self.reader.special_attr_dict.keys()):
                string = "\nLINEAR {}_param;\n".format(self.reader.name)
              else:
                string = "\nLINEAR_PI {}_param;\n".format(self.reader.name)
            return string
            
#---------------------------------------------------------------------------------------------------------------#

#  ██╗    ██╗██╗██████╗ ██╗███╗   ██╗ ██████╗ 
#  ██║    ██║██║██╔══██╗██║████╗  ██║██╔════╝ 
#  ██║ █╗ ██║██║██████╔╝██║██╔██╗ ██║██║  ███╗
#  ██║███╗██║██║██╔══██╗██║██║╚██╗██║██║   ██║
#  ╚███╔███╔╝██║██║  ██║██║██║ ╚████║╚██████╔╝
#   ╚══╝╚══╝ ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝ ╚═════╝ 
#                                             

        
#inherited

#  ██╗    ██╗███████╗██╗    ██╗      ██████╗  █████╗ ██████╗ 
#  ██║    ██║██╔════╝██║    ██║     ██╔═══██╗██╔══██╗██╔══██╗
#  ██║ █╗ ██║█████╗  ██║    ██║     ██║   ██║███████║██║  ██║
#  ██║███╗██║██╔══╝  ██║    ██║     ██║   ██║██╔══██║██║  ██║
#  ╚███╔███╔╝███████╗██║    ███████╗╚██████╔╝██║  ██║██████╔╝
#   ╚══╝╚══╝ ╚══════╝╚═╝    ╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ 
#                                                            

        def get_weights_loading_string(self):
          string = ''
        
          if self.reader.operation == "Gemm":
            
            size = self.reader.isizes[0] * self.reader.osizes[0]
            string += '%s_param->weights = (DATA*)malloc(%d * sizeof(DATA));\n' % (self.reader.name, size)
            string += 'sprintf(filename, "%%s/%s_weights.bin", load_data_dir);\n' % (self.reader.name)
            string += 'load_fixed(filename, %d, %s_param->weights);\n' % (size, self.reader.name)
            size  = self.reader.osizes[0]
            string += '%s_param->bias = (DATA*)malloc(%d * sizeof(DATA));\n' % (self.reader.name, size)
            string += 'sprintf(filename, "%%s/%s_biases.bin", load_data_dir);\n' % (self.reader.name)
            string += 'load_fixed(filename,%d,%s_param->bias);\n' % (size, self.reader.name)
            
            
            
            
            
          
          
          string += "\n"
          return string


#  ██╗███╗   ██╗██╗████████╗
#  ██║████╗  ██║██║╚══██╔══╝
#  ██║██╔██╗ ██║██║   ██║   
#  ██║██║╚██╗██║██║   ██║   
#  ██║██║ ╚████║██║   ██║   
#  ╚═╝╚═╝  ╚═══╝╚═╝   ╚═╝   
#                           


        def get_initialization_string(self):
            
            if self.reader.operation == "Gemm":
                s = "\n\n// #########################################\n"
                s += "//        FULLY CONNECTED LAYER init *\n"
                s += "//##########################################\n\n"
                name = self.reader.nodes_input()[0]
                if ('IF_flag' not in self.reader.special_attr_dict.keys()):
                  s += "{}_param = linear_create();\n"                                   .format (self.reader.name)
                else:   
                  s += "{}_param = linear_PI_create();\n"                                   .format (self.reader.name)

                if self.reader.prev_layers[0].operation != "Reshape" and self.reader.prev_layers[0].operation != "Gemm": # devo inserire una condizione che faccia si che la riga s14 venga generata solo nel caso in cui la gemm sia la prima della rete


                    s+= "{}_param->in_s = {};\n"               .format (self.reader.name, self.reader.isizes[0])
                elif self.reader.prev_layers[0].operation != "Gemm":
            
                    s+= "SIZE {}_dim = {};\n"                     .format (self.reader.input_[0], self.reader.initializer.layer_input_shape[name[1]].dim_value)
                   
                else :
                    s += ""
                
                
                s += "{}_param->out_s = {};\n"                                     .format (self.reader.name, self.reader.osizes[0])#initializer.layer_input_shape[name[1]].dim_value)
                s += "{}_param->in_s = {};\n"                                      .format (self.reader.name, self.reader.isizes[0])
                
                s+= " {}_param->qf = {};\n"                                        .format (self.reader.name, self.reader.qf)
                if ('IF_flag' not in self.reader.special_attr_dict.keys()):
                  s+= "{}_param->last_layer_output_dim = {};\n".format(self.reader.name, self.reader.prev_layers[0].osizes[1]*self.reader.prev_layers[0].osizes[2])   
            return s

#  ███████╗ ██████╗ ██████╗ ██╗    ██╗ █████╗ ██████╗ ██████╗ 
#  ██╔════╝██╔═══██╗██╔══██╗██║    ██║██╔══██╗██╔══██╗██╔══██╗
#  █████╗  ██║   ██║██████╔╝██║ █╗ ██║███████║██████╔╝██║  ██║
#  ██╔══╝  ██║   ██║██╔══██╗██║███╗██║██╔══██║██╔══██╗██║  ██║
#  ██║     ╚██████╔╝██║  ██║╚███╔███╔╝██║  ██║██║  ██║██████╔╝
#  ╚═╝      ╚═════╝ ╚═╝  ╚═╝ ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ 
#                                                             


#---------------------------------------------------------------------------------------------------------------#
        def get_forward_string(self):
                
                if self.reader.operation == "Gemm":
                    gemm_ex = True # segnale che è stato trovato un layer Gemm
                    
                    a5 = "\n//################################################# Linear layer ##########################################################\n"

                    previous_dim = "{}_dim".format(self.reader.input_[0])
                    a2 = ""#"static DATA {}[{}_dim];\n".format(self.reader.name, self.reader.name)
                    if ('IF_flag' not in self.reader.special_attr_dict.keys()):
                      a3 = "linear_forward_wrap({}_param);\n\n".format(self.reader.name )
                    else:
                      a3 = "linear_PI_forward_wrap({}_param);\n\n".format(self.reader.name )
                    s = a5 + a2 + a3

                return s

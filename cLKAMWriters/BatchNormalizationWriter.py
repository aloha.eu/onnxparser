
# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.


import sys, math, os, struct
import numpy as np
from .CWriter import CWriter

class BatchNormalizationWriter(CWriter):
        def __init__ (self,  reader):

          super().__init__(reader)

          '''

#  ███████╗ █████╗ ██╗   ██╗███████╗    ██████╗  █████╗ ██████╗  █████╗ ███╗   ███╗
#  ██╔════╝██╔══██╗██║   ██║██╔════╝    ██╔══██╗██╔══██╗██╔══██╗██╔══██╗████╗ ████║
#  ███████╗███████║██║   ██║█████╗      ██████╔╝███████║██████╔╝███████║██╔████╔██║
#  ╚════██║██╔══██║╚██╗ ██╔╝██╔══╝      ██╔═══╝ ██╔══██║██╔══██╗██╔══██║██║╚██╔╝██║
#  ███████║██║  ██║ ╚████╔╝ ███████╗    ██║     ██║  ██║██║  ██║██║  ██║██║ ╚═╝ ██║
#  ╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚══════╝    ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝

        def save_parameters(self, path):
          from tqdm import tqdm
          if self.reader.operation == "BatchNormalization":
            print ("BN datatype ", self.reader.datatype)
            if self.reader.weights is not None:
              filename = self.reader.name + "_weights.bin"
              with open(os.path.join(path, filename), "wb") as f:
                for wb in tqdm(self.reader.weights.flatten(), bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                  if self.reader.datatype == 'h':
                    w_byte=struct.pack("h",self.reader.convert_float_to_short(wb,'h',self.reader.qf))
                  else:
                    w_byte=struct.pack("f",wb)
                    
                  f.write(w_byte)
             
              filename = self.reader.name + "_biases.bin"
              if self.reader.biases is not None: # check if biases exsist
                biases = self.reader.biases
              else:
                biases = np.zeros(self.reader.osizes[0])
              
              with open(os.path.join(path, filename), "wb") as f:
                for wb in tqdm(biases, bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                  if self.reader.datatype == 'h':
                    b_byte=struct.pack("h",self.reader.convert_float_to_short(wb,'h',self.reader.qf))
                  else:
                    b_byte=struct.pack("f",wb)
                  f.write(b_byte)
            
            if self.reader.mean is not None:
              filename = self.reader.name + "_mean.bin"
              with open(os.path.join(path, filename), "wb") as f:
                for wb in tqdm(self.reader.mean.flatten(), bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                  if self.reader.datatype == 'h':
                    w_byte=struct.pack("h",self.reader.convert_float_to_short(wb,'h',self.reader.qf))
                  else:
                    w_byte=struct.pack("f",wb)
                    
                  f.write(w_byte)
                  
            if self.reader.var is not None:
              filename = self.reader.name + "_var.bin"
              with open(os.path.join(path, filename), "wb") as f:
                for wb in tqdm(self.reader.var.flatten(), bar_format="{l_bar}{bar:40}{r_bar}{bar:-40b}"):
                  if self.reader.datatype == 'h':
                    w_byte=struct.pack("h",self.reader.convert_float_to_short(wb,'h',self.reader.qf))
                  else:
                    w_byte=struct.pack("f",wb)
                    
                  f.write(w_byte)
                  
                  '''
#  ██████╗ ███████╗ ██████╗██╗      █████╗ ██████╗ ███████╗
#  ██╔══██╗██╔════╝██╔════╝██║     ██╔══██╗██╔══██╗██╔════╝
#  ██║  ██║█████╗  ██║     ██║     ███████║██████╔╝█████╗  
#  ██║  ██║██╔══╝  ██║     ██║     ██╔══██║██╔══██╗██╔══╝  
#  ██████╔╝███████╗╚██████╗███████╗██║  ██║██║  ██║███████╗
#  ╚═════╝ ╚══════╝ ╚═════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝
#                                                          

#---------------------------------------------------------------------------------------------------------------#
        def get_declaration_string(self):
          s=''
          if self.reader.datatype == 'h':
            s = "\nBATCH {}_param;" .format(self.reader.name)
          else:
            s = "\nBATCH_F {}_param;" .format(self.reader.name)
                  
          return s
        



#  ██╗    ██╗███████╗██╗    ██╗      ██████╗  █████╗ ██████╗ 
#  ██║    ██║██╔════╝██║    ██║     ██╔═══██╗██╔══██╗██╔══██╗
#  ██║ █╗ ██║█████╗  ██║    ██║     ██║   ██║███████║██║  ██║
#  ██║███╗██║██╔══╝  ██║    ██║     ██║   ██║██╔══██║██║  ██║
#  ╚███╔███╔╝███████╗██║    ███████╗╚██████╔╝██║  ██║██████╔╝
#   ╚══╝╚══╝ ╚══════╝╚═╝    ╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ 
#                                                            

        def get_weights_loading_string(self):
          s=''
          
          if self.reader.datatype == 'h':
                
            size = self.reader.isizes[0]
            s += '%s_param->weights = (DATA*)malloc(%d * sizeof(DATA));\n' % (self.reader.name, size)
            s += 'sprintf(filename, "%%s/%s_weights.bin", load_data_dir);\n' % (self.reader.name)
            s += 'load_fixed(filename, %d, %s_param->weights);\n' % (size, self.reader.name)
            size  = self.reader.isizes[0]
            s += '%s_param->bias = (DATA*)malloc(%d * sizeof(DATA));\n' % (self.reader.name, size)
            s += 'sprintf(filename, "%%s/%s_biases.bin", load_data_dir);\n' % (self.reader.name)
            s += 'load_fixed(filename,%d,%s_param->bias);\n' % (size, self.reader.name)
            size  = self.reader.isizes[0]
            s += '%s_param->mean = (DATA*)malloc(%d * sizeof(DATA));\n' % (self.reader.name, size)
            s += 'sprintf(filename, "%%s/%s_mean.bin", load_data_dir);\n' % (self.reader.name)
            s += 'load_fixed(filename,%d,%s_param->mean);\n' % (size, self.reader.name)
            size  = self.reader.isizes[0]
            s += '%s_param->var = (DATA*)malloc(%d * sizeof(DATA));\n' % (self.reader.name, size)
            s += 'sprintf(filename, "%%s/%s_var.bin", load_data_dir);\n' % (self.reader.name)
            s += 'load_fixed(filename,%d,%s_param->var);\n' % (size, self.reader.name)
          else:                  
            size = self.reader.isizes[0]
            s += '%s_param->weights = (float*)malloc(%d * sizeof(float));\n' % (self.reader.name, size)
            s += 'sprintf(filename, "%%s/%s_weights.bin", load_data_dir);\n' % (self.reader.name)
            s += 'load_float(filename, %d, %s_param->weights);\n' % (size, self.reader.name)
            size  = self.reader.isizes[0]
            s += '%s_param->bias = (float*)malloc(%d * sizeof(float));\n' % (self.reader.name, size)
            s += 'sprintf(filename, "%%s/%s_biases.bin", load_data_dir);\n' % (self.reader.name)
            s += 'load_float(filename,%d,%s_param->bias);\n' % (size, self.reader.name)
            size  = self.reader.isizes[0]
            s += '%s_param->mean = (float*)malloc(%d * sizeof(float));\n' % (self.reader.name, size)
            s += 'sprintf(filename, "%%s/%s_mean.bin", load_data_dir);\n' % (self.reader.name)
            s += 'load_float(filename,%d,%s_param->mean);\n' % (size, self.reader.name)
            size  = self.reader.isizes[0]
            s += '%s_param->var = (float*)malloc(%d * sizeof(float));\n' % (self.reader.name, size)
            s += 'sprintf(filename, "%%s/%s_var.bin", load_data_dir);\n' % (self.reader.name)
            s += 'load_float(filename,%d,%s_param->var);\n' % (size, self.reader.name)
              
          return s


#  ██╗███╗   ██╗██╗████████╗
#  ██║████╗  ██║██║╚══██╔══╝
#  ██║██╔██╗ ██║██║   ██║   
#  ██║██║╚██╗██║██║   ██║   
#  ██║██║ ╚████║██║   ██║   
#  ╚═╝╚═╝  ╚═══╝╚═╝   ╚═╝   
#                           

        def get_initialization_string(self):
          s=''
                
          s = "\n\n// ###############################################################\n"
          s += "// #######             BATCH NORMALIZATION LAYER init          #############\n"
          s += "// #############################################################\n\n"
          if self.reader.datatype == 'h':
            s += "\n\n {}_param = batch_create();\n".format (self.reader.name)
          else:
            s += "\n\n {}_param = batch_f_create();\n".format (self.reader.name)
            
          s+= "{}_param->size[0] = {};\n".format(self.reader.name,  self.reader.isizes[0])
          s+= "{}_param->size[1] = {};\n".format(self.reader.name,  self.reader.isizes[1])
          s+= "{}_param->size[2] = {};\n".format(self.reader.name,  self.reader.isizes[2])
         
          s+= "{}_param->qf      = {};\n".format (self.reader.name, self.reader.qf)
               
               
               
               
          return s


#  ███████╗ ██████╗ ██████╗ ██╗    ██╗ █████╗ ██████╗ ██████╗ 
#  ██╔════╝██╔═══██╗██╔══██╗██║    ██║██╔══██╗██╔══██╗██╔══██╗
#  █████╗  ██║   ██║██████╔╝██║ █╗ ██║███████║██████╔╝██║  ██║
#  ██╔══╝  ██║   ██║██╔══██╗██║███╗██║██╔══██║██╔══██╗██║  ██║
#  ██║     ╚██████╔╝██║  ██║╚███╔███╔╝██║  ██║██║  ██║██████╔╝
#  ╚═╝      ╚═════╝ ╚═╝  ╚═╝ ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ 
#                                                             


#---------------------------------------------------------------------------------------------------------------#
        def get_forward_string(self):
          s=''
                   
          s = "\n//##################################### BatchNormalization layer ##########################################################\n"
          if self.reader.datatype=='h':
            s += "batch_forward_wrap({}_param);\n\n".format(self.reader.name)
          else:
            s += "batch_f_forward_wrap({}_param);\n\n".format(self.reader.name)
                    
          return s
          

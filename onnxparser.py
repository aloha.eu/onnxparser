#!/usr/bin/env python3.5
# Copyright (c) 2018
# by Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.


# if you get import problems, add the path of this script to PYTHONPATH env variable

from Initializer import Initializer
from NetReader import NetReader
from NetWriterC import NetWriterC
from NetWriterPytorch import NetWriterPytorch
from NetWriterNEURAghe import NetWriterNEURAghe
from NetWriterCMSIS import NetWriterCMSIS
from NetWriterARMCL import NetWriterARMCL
from NetWriterCLKAM import NetWriterCLKAM
from NetWriterTxt import NetWriterTxt

import sys
import time
import onnx
import os
import numpy as np

target_mapping={"pytorch"  : NetWriterPytorch,
                "plainC"   : NetWriterC,
                "NEURAghe" : NetWriterNEURAghe,
                "CMSIS"    : NetWriterCMSIS,
                "ARMCL"    : NetWriterARMCL,
                "cLKAM"    : NetWriterCLKAM,
                "text"      : NetWriterTxt,
}

def sort_nodes(nodes):
          done = False
          while not done:
            done = True
            for i, layer in enumerate(nodes):
              out = layer.output[0]
              for j, l in enumerate(nodes):
                if out in l.input and j<i:                  
                  done = False
                  print ("Swapping {} and {}".format(layer.name, l.name))
                  nodes[i], nodes[j] = nodes[j], nodes[i] #swap
                    
                  
            

def onnxextract (model_file,  path = './output', weights_path = "./temp_weights/", net_input_height=None, net_input_width=None, 
                 channels=None, class_elements=None, app_name=None, discard_params=False, yolo_like=False, batch_norm_folding=False, 
                 smart_allocation=False, remove_preprocessing=True, neuraghe=False, qf=8, target=None, nRow = None, nCol = None):
                 
        
    if target not in target_mapping:
      raise ValueError("Invalid target: '{}' not in '{}'".format(str(target), str(list(target_mapping.keys()))))
    
    if neuraghe:
      print ("\n\n## Mapping on NEURAghe accelerator ##")
      
    
    
    print ("\nLoading onnx model...")
    model = onnx.load(model_file)
    print ("done\n\n")
    nodes = model.graph.node
    nodes=[n for n in nodes]
    sort_nodes (nodes)
    
    graph = model.graph
    print ("Creating initializer object....")
    initializer = Initializer(model, path, graph)
    print ("done.\n")
    input_shape = initializer.model_input_shape
    
    if net_input_height == None and net_input_width == None and channels == None:

        print ("input shape NCHW", input_shape)
        input_size = input_shape[1:]
        
        print ("input size CHW", input_size)
        
    else:
        net_input_height = input_shape[2]
        print("net_input_height",net_input_height)
        net_input_width  = input_shape[3]
        print("net_input_width",net_input_width)
        channels         = input_shape[1]
        print("channels",channels)   
    
        input_size=[channels, net_input_height,net_input_width] #CHW

    print ("Reading onnx ...")

    netReader = NetReader(model_file, nodes, initializer, graph, input_size, qf=qf,yolo_like=yolo_like)
    
    print ("\tdone.")
    
    netReader.sort_layers()
    
    if class_elements is not None:
      netReader.change_output_feature_maps(class_elements)
    
    
    print ("Optimizing net ...")
    
    
#    if target!= "pytorch" :
#       netReader.quantization_folding()
    
    optimizer = target_mapping[target].optimizeNet
    optimizer(netReader, batch_norm=batch_norm_folding)
    print ("Optimizing net: complete")
    
    if target== "pytorch" :
      if remove_preprocessing:
        netReader.removePreProcessing(['Conv', 'Pad', 'Gemm'])
    
    if yolo_like:
      print ("Yolo-like net")
      if target == "pytorch":
        netReader.to_yolo([class_elements,None,None])
      elif target == "plainC" or target == "NEURAghe":
        anchors = [[1.08,1.19],[3.42,4.41],[6.63,11.38],[9.42,5.11],[16.62,10.52]]
        anchors_file = os.path.join(os.path.split(model_file)[0], "anchors.txt")
        #print (anchors_file)
        if os.path.isfile(anchors_file):
          try:
            with open(anchors_file) as f:
              lines = f.readlines()
              anchors = [[float(l.split(",")[0]), float(l.split(",")[1])] for l in lines]
            print ('Anchors file found.')
          except FileNotFoundError:
            print ('Anchors file does not not exists. Using default anchors')
          except:
            print ('Error reading anchors file. Using default anchors')
        else:
          print ('Anchors file not found. Using default anchors')
        print (anchors)
        
        classes = None
        classes_file = os.path.join(os.path.split(model_file)[0], "classes_names.txt")
        #print (classes_file)
        if os.path.isfile(classes_file):
          try:
            with open(classes_file) as f:
              lines = f.readlines()
              classes = [l.strip() for l in lines]
            print ('classes file found.')
          except FileNotFoundError:
            print ('classes file does not not exists. Using default classes')
          except:
            print ('Error reading classes file. Using default classes')
        else:
          print ('classes file not found. Using default classes')
        print (classes)

        netReader.add_regionLayer(anchors, classes = classes)
    
    
#    if target== "CMSIS" :
#       RPI=netReader.quantization_folding()
#    else:
#       RPI=0

    print ("\tdone.")
    
    
    if discard_params:
      netReader.set_load_params(False)
    
    
    print ("Mapping net to technology...")
    
    kwargs={'RPI'       : netReader.RPI,
            'datatype'  : 'h',
            'softmax'   : False,
            'qf'        : qf,
            'yolo_like' : yolo_like,
            'nRow'      : nRow,
            'nCol'      : nCol,
            'smart_memalloc' : smart_allocation,
            'load_params' : not discard_params
            
    } # optional arguments
    
    netWriter= target_mapping[target](model_file, path, app_name, weights_path, netReader.net, initializer, graph, input_size, **kwargs)
    print ("\tdone.")    
    
    if not discard_params:
      netWriter.save_parameters()

    print ("Writing {} net...".format(target))
    netWriter.print_net()
    print ("\tdone.\n")
    
    
    #To be fixed: adds some parameters target-specific
    if target == 'pytorch':
      print ("Writing net test...")
      netWriter.print_test_net()
      print ("\tdone.\n")
    
    if target == 'pytorch' or target == 'text':
      print (netWriter.net_description())


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Call the onnxextract function passing the parameters you want to setself. If no parameters are inserted by the user(exept model file),network parameters are by default extracted from the model file.')
    parser.add_argument('model', default=None, type=str, help =" .onnx file model")
    parser.add_argument('name',  default=None, type=str, help =" app name")
    parser.add_argument('-P',  '--class_path', default = './', type = str, help= ' Path where you want to save the class, weights and bias file')
    parser.add_argument('-H',  '--net_input_height', default = None, type = int, help ='Height of the network input')
    parser.add_argument('-W',  '--net_input_width', default = None, type = int,  help ='Width of the network input')
    parser.add_argument('-C',  '--channels', default = None, type = int,  help ='Number of channels on the network input')
    parser.add_argument('-E',  '--class_elements', default = None, type = int, help ='Number of output classes ')
    parser.add_argument('-S',  '--softmax_enable', default = False, type = bool,  help ='Set whether the softmax layer is enabled or not')
    parser.add_argument('-D',  '--discard_params', help="Discard parameters stored in the onnx", action="store_true")
    parser.add_argument('-Y',  '--yolo_like', help="Add the detection layer", action="store_true")
    parser.add_argument('-F',  '--batch_norm_folding', help="Do Batch Normalization Folding", action="store_true")
    parser.add_argument('-SA', '--smart_allocation', help="Smart Memory Allocation", action="store_true")
    parser.add_argument('-n',  '--neuraghe', help="Convert targetting NEURAghe", action="store_true")
    parser.add_argument('-Q',  '--qf', default=8, type = int, help="Number of bit for fractional part of fixed point representation")
    parser.add_argument('-T',  '--target', default='pytorch', type=str, choices =list(target_mapping.keys()), help ="Target technology against which to generate the code")
    parser.add_argument(       '--nRow', default = None, type = int, help ='Number of Rows of the NEURAghe SoP matrix')
    parser.add_argument(       '--nCol', default = None, type = int, help ='Number of Columns of the NEURAghe SoP matrix')

    args = parser.parse_args()
    
    os.makedirs(args.class_path, exist_ok=True)
        
    path_to_class    = args.class_path
    net_input_height = args.net_input_height
    net_input_width  = args.net_input_width
    channels         = args.channels
    class_elements   = args.class_elements
    softmax_layer    = args.softmax_enable
    app_name         = args.name

    print ("Model:", args.model)
    print ("\nNet parameters:")
    print ("input channels:", channels)
    print ("input height:", net_input_height)
    print ("input width", net_input_width)
    print ("output classes", class_elements)
    print ("path:", path_to_class)

    onnxextract(args.model, weights_path = "./temp_weights_{}/".format(app_name), path=path_to_class, net_input_height=net_input_height,
                net_input_width=net_input_width, channels=channels, class_elements=class_elements, 
                app_name=app_name, discard_params=args.discard_params, yolo_like=args.yolo_like, 
                batch_norm_folding=args.batch_norm_folding, smart_allocation=args.smart_allocation,
                neuraghe=args.neuraghe, qf=args.qf, target=args.target, nRow = args.nRow, nCol = args.nCol)


    print ("Conversion finished!")







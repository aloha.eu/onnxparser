from setuptools import find_packages, setup

setup(package_dir={'': 'onnxparser'},
      packages=find_packages(where='onnxparser'), )

# OnnxParser
This tool is intended to be used to parse a ONNX file and to produce automatically the code for a specified target.  
A few target are already supported:  
* PyTorch
* Plain C, which can be compiled for any General Purpos CPU
* C accelerated by NEURAghe
* CMSIS
* ARMCL
* Text file



The generated apps in C code are intended to be embedded into the NEURAghe framework which provvides the implementations for the CNN operators: you can get it at [git@gitlab.com:aloha.eu/convnet.git]

The user can define the writers for its own target.

## Dependencies

python3  
tqdm  
onnx  
NEURAghe Convnet framework: git@gitlab.com:aloha.eu/convnet.git  
*optional* ARM gcc toolchain to crosscompile for ARM processors  

## Usage

```
python3 onnxparser.py -T pytorch model.onnx app_name
```


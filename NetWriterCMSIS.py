
# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.

from Initializer import *
import sys, math, os
import numpy as np
import onnxReaders as rd
import CMSISWriters as wr


class NetWriterCMSIS():

        def __init__ (self, model_file, path, app_name,  weights_path, readers, init, graph, input_size, **kwargs):
          
          
          try:
            RPI      = kwargs['RPI']
          except:
            RPI = 0
            
          try:
            datatype = kwargs['datatype']
          except:
            datatype = 'h'
            
          try:
            qf = kwargs['qf']
          except:
            qf = 8
            
          try:
            yolo_like = kwargs['yolo_like']
          except:
            yolo_like = False
          
          self.app_name = app_name
          self.path = path
          
          self.readers = readers
          self.weights_path = weights_path
          
          self.model_file     = model_file

          self.yolo_like      = yolo_like
          self.qf             = qf
          
          self.init     = init
          
          self.input_size     = input_size
          
          self.net = []
          self.datatype=datatype
          self.RPI=kwargs['RPI']
          i=0
          for r in readers:
                       
            if r.operation in wr.mapping.keys():
              w = wr.mapping[r.operation](r)
              self.net.append(w)
            elif r.operation not in wr.supported_operators:
              print ("Operator not supported: {} in {}".format(r.operation, r.name))
              exit(1)
            else:
              print ("Skipping operator: {} in {}".format(r.operation, r.name))
            
            
        def optimizeNet (netReader, batch_norm=False, neuraghe=False):
           netReader.removePreProcessing(['Conv'])
           netReader.removeOperators(['Cast', 'Dropout', 'Flatten'])
           netReader.force_datatype('h', netReader.net)
           
           netReader.batch_norm_folding(batch_norm)
           netReader.md2shift()
           netReader.mul_rshift_folding()
           netReader.stride2subsampling(neuraghe)
           
          # netReader.datatype_conversion()
            
            
            
            
                
        def save_parameters(self):
        
            print ("Extracting weights...")
            
            os.makedirs(self.weights_path, exist_ok=True)

            for layer in self.net:
              print(layer.name)
              layer.save_parameters(self.weights_path, self.RPI)
          
          
            maxog = 4
            conv_line = '{"name":"%s","weights":{"file":"%s","size":[%d,%d,%d,%d]},"bias":{"file":"%s","size":[%d]}, "maxog":%d}\n'
            with open(os.path.join(self.weights_path,"conv.json"), 'w') as f:
              for layer in self.net:
                if layer.operation=="Conv": # and layer.weights != None and layer.biases != None:
                  w = layer.name + "_weights.bin"
                  b = layer.name + "_biases.bin"
                  wshape = [layer.osizes[0], layer.isizes[0], layer.kernel[0], layer.kernel[1]]
                  bshape = [layer.osizes[0]]
                  
                  f.write(conv_line%(layer.name, w, wshape[0], wshape[1], wshape[2], wshape[3], b, bshape[0], maxog))
        
          
      
        
        
        
        
        
        
        def net_description(self):
           
            net_description = "Source file: %s\n\n" % self.model_file
            net_description +="%-20s %5s %5s %5s %5s %5s %5s\n"%("Layer", "IF", "ih", "iw", "OF", "oh", "ow")
            
            for layer in self.net:
                net_description+= "%-20s %5d %5d %5d %5d %5d %5d\n"%(layer.name, layer.isizes[0], layer.isizes[1], layer.isizes[2], layer.osizes[0], layer.osizes[1], layer.osizes[2])
            return net_description
            
            
            
         
        def get_mem_allocation_string(self):

          string = ""

          for layer in self.net:
            if layer.operation in rd.supported_operators:
              if layer.operation=="Region":
                string+=""
              else:
                size = layer.get_output_pixels()                
                dt= "float" if layer.datatype=='f' else "DATA"
                string += "{}_param->output = ({}*)malloc({}*sizeof({}));\n".format(layer.name, dt, size, dt)
          
          string += "\n"
          return string
          
          
          
          
          
        def get_smart_mem_allocation_string(self):

          names = ['A','B']
          if (self.RPI):
            data="uint8_t"
          else:
            data="int16_t"
          string  = data+ "* {} = (".format(names[0])+data+"*)malloc({}*sizeof(".format(8192)+data+"));\n"
          string += data+ "* {} = &input[0];\n".format(names[1])
  
          string += "int16_t * test_col_buffer = (int16_t*)malloc({}*sizeof(int16_t));\n".format(8192)
          
          #allocate space for the output
          i=1
          
          string += data+"* {}_input = {};\n".format(self.net[0].name,names[0])
          for layer in self.net:            
            if layer.operation in rd.supported_operators:
              if layer.operation=="Region":
                string+=""
              elif(self.RPI==0):
                string += data+"* {}_output = {};\n".format(layer.name, names[i])
                i+=1
                if i==len(names):
                  i=0
              elif(self.RPI==1 and layer.operation!="Gemm"):
                string += data+"* {}_output = {};\n".format(layer.name, names[i])
                i+=1
                if i==len(names):
                  i=0
              elif(self.RPI==1 and layer.operation=="Gemm"):
                string += "int16_t* {}_output = (int16_t*)malloc({}*sizeof(int16_t));\n".format(layer.name, layer.osizes[0])
          string += "\n"
          return string
          
#---------------------------------------------------------------------------------------------------------------------------------------------
        def print_net(self):
        
                
                name_file        = os.path.join(self.path, "cnnNet.h")
                
                print (self.net_description())
                
                print ("Creating file {} ...".format(name_file))

                name_file = open(name_file,"w")
                name_file.write("\n//This code is auto_generated.//\n")
                name_file.write("\n//This file must be embedded in the host application that will provide the data to be processed.\n\n\n\n")
                name_file.write("/* ################## Net description ###################\n")
                name_file.write(self.net_description())
                name_file.write("#########################################################*/\n")

                

#------------stampo il codice per dichiarare e inizializzare  le strutture dei layer  conv e gemm (fully connected layer) -------------------------------------------------------
                s  = ''
                    
                s += '#include "global.h"\n'
                s += '#include "arm_nnfunctions.h"\n'
                s += '#include "datalog_application.h"\n'
                
                for layer in self.net:
                    s += layer.get_declaration_string()
                    
                s += "/* ################## input for test ################### */\n"
                s += '#include "input11562_16x32_RPI.h"\n'
                s += "/* #################################################### */\n\n"
                s += "\n\n//#########################################################################################################################################\n"

                name_file.write(s)

                s  = "static void cnnClassifier(int sensorId);\n"

                s += "\nstatic void cnnClassifier(int sensorId){\n"

                s += '  osEvent evt;\n'
                s += '  T_SensorsData *rptr;\n'
                s += '  int i,idx;\n'
                s += '  for (;;){\n'                
                
                
                
                s += '    evt = osMessageGet(sensorSetup[sensorId].cnn_QueueId, osWaitForever);\n'

                s += '    STLBLE_PRINTF("CNN classifier\\n");\n'

                s += '    if (evt.status == osEventMessage){\n'
                s += '      rptr = (T_SensorsData *) evt.value.p;\n'
                s += '      osPoolFree(message_PoolId, rptr);\n'
                
                name_file.write(s)
                
                s  = "\n\n// ########################################################\n"
                s += "// ######################## MEMORY ALLOCATION #######################\n"
                s += "// #######################################################\n\n"
                
                s += "//\tEach layer produces an output. By default the output is stored in a dedicated DDR segment accwssible by the accelerator.\n"
                s += "//\tWe assume that the output of a layer will be the input of the netx so we do not need to allocate RAM for the input.\n"
                s += "//\tBy editing this you can customize the memory utilization\n\n"
                
                name_file.write(s)
                
                s  = self.get_smart_mem_allocation_string()
                
                
                s += "\n\n// ########################################################\n"
                s += "// ######################## WIRING #######################\n"
                s += "// #######################################################\n\n"
                s += "//\tEach layer has an input and an output pointer. The connection between two self.net is defined here\n\n"
                
                name_file.write(s)
                
                print ("RPI"+str(self.RPI))
                for layer in self.net:
                    name_file.write(layer.get_wiring_string(self.RPI))
                
                m=''
                for d in self.input_size:
                  m+='{}*'.format(d)
                m=m[:-1]
                
                s  ="      for (int j=0;j<2*{};j++)\n".format(m)
                s +="        {}_input[j]=0;\n".format(self.net[0].name)
                s +="      for (int j=0;j<{};j++)\n".format(m)
                s +="        {}_input[j*2]=input[j];\n".format(self.net[0].name)
                
                name_file.write(s)

                for layer in self.net:
                    name_file.write(layer.get_forward_string(self.RPI))
#------------------------------------------------------------------------------------------------------------------------------------------------

                name_file.write("\n\n")
                
                name_file.write("free(test_col_buffer);\n")
                name_file.write("free(A);\n")


         #       name_file.write("		exec_cnn++;\n")

                name_file.write("    }\n")
                name_file.write("  }\n")
                name_file.write("}\n")
                
     
     
     
        def get_classifier(self):
          
          
          l= ' char *labels[%d]={'%self.net[-1].osizes[0]

          for i in range (0,self.net[-1].osizes[0]):
            l+='"label_%d", '%i
              
          l=l[0:-2]
          l+='};\n\n'

          s=l
          s+="// TODO: check the size parameter\n" 
          s+="  int size_wa = {};\n".format(self.net[-1].osizes[0])
          
          s+="  float* r= (float*) malloc (size_wa*sizeof(float));\n"
          s+="  int*  c= (int*)  malloc (size_wa*sizeof(int));\n"
          s+="  float* results_float= (float*) malloc (size_wa*sizeof(float));\n"

          s+="  float sum=0.0;\n"
          
          
          s+="  DATA max=0;\n"
          s+="  for (int i =0;i<size_wa;i++){\n"
          s+="      results_float[i] = FIXED2FLOAT(results[i],{});\n".format(self.qf)
          s+="    int n;\n"
          s+="    if (results[i]>0)\n"
          s+="      n=results[i];\n"
          s+="    else\n"
          s+="      n=-results[i];\n"
      
          s+="    if (n>max){\n"
          s+="      max=n;\n"
          s+="    }  \n"
          s+="  }\n"
          
          
          s+="  for (int i =0;i<size_wa;i++)\n"
          s+="    sum+=exp(results_float[i]);\n\n"
          
          s+="  for (int i =0;i<size_wa;i++){\n"
          s+="    r[i]=exp(results_float[i]) / sum;\n"
          s+="    c[i]=i;\n"
          s+="  }\n"
          
          s+="  for (int i =0;i<size_wa;i++){\n"
          s+="    for (int j =i;j<size_wa;j++){\n"
          s+="      if (r[j]>r[i]){\n"
          s+="        float t= r[j];\n"
          s+="        r[j]=r[i];\n"
          s+="        r[i]=t;\n"
          s+="        int tc= c[j];\n"
          s+="        c[j]=c[i];\n"
          s+="        c[i]=tc;\n"
          s+="      }\n"
          s+="    }\n"
          s+="  }\n"
          
          s+="  int top0=0;\n"
          s+="  float topval=results_float[0];\n"
          s+="  for (int i =1;i<size_wa;i++){\n"          
          s+="    if (results_float[i]>topval){\n"
          s+="      top0=i;\n"
          s+="      topval=results_float[i];\n"
          s+="    }  \n"
          s+="  }\n"
          
          
          s+='  printf("\\n\\n");\n'
          s+="  for (int i =0;i<5;i++){\n"
          s+='    printf("            TOP %d: [%d] %s   ",i, c[i], labels[c[i]]);\n'
          s+="    for (int j =0;j<100*r[i];j++)\n"
          s+='      if (j<40) printf("#");\n'
          s+='    printf(" %0.1f%\\n",r[i]*100);\n'
          s+="  }  \n"
          s+='  printf("\\n\\n\\n\\n\\n");\n'
          
          s+='  return top0;\n\n\n'         
          
        
          return s
          
          
        def get_region(self):
                        
          s =  "region_param->input = (float*)malloc({} * sizeof(float));\n\n".format (self.net[-1].get_output_pixels())

          s += "for (int i = 0; i<{};i++){{\n".format (self.net[-1].get_output_pixels())

          s += "  region_param->input[i]=FIXED2FLOAT(results[i],{});\n".format(self.qf)
          s += "}\n\n"


          s += "float a[]={1.08,1.19,3.42,4.41,6.63,11.38,9.42,5.11,16.62,10.52};//fixme: get them from a file\n"

          s += "//region_param->output=(float*)malloc(0 * sizeof(float));\n"
          s += "region_param->anchors=a;\n"

          s += "region_forward_wrap(region_param);\n"
          
          
          s += 'return -1;\n\n\n'
        
          return s
          


# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.

from Initializer import *
import sys, math, os
import numpy as np
import onnxReaders as rd

DEFAULT_DATATYPE = 'h'

class NetReader():

        def __init__ (self, model_file, nodes, init, graph, input_size, qf=8, yolo_like=False):
          
          self.operators_dict = {}
          
          self.model_file     = model_file

          self.yolo_like      = yolo_like
          self.qf             = qf
          
          self.init     = init
          
          self.input_size     = input_size
          
          self.net = []
          self.RPI = 0
          net_input = rd.OnnxReader ("NetInput", init, prev_layers=None, qf=qf, datatype=DEFAULT_DATATYPE)
          net_input.operation = 'Input'
          

          net_input.osizes = [i for i in input_size]
          net_input.isizes = [i for i in input_size]

          
          net_input.input_  = [None]
          net_input.output_ = [init.net_input]
          net_input.prev_layers=[]
          self.net.append(net_input)

          i=0
          for node in nodes:
            if node.op_type in self.operators_dict.keys():
              self.operators_dict[node.op_type]+=1
            else:
              self.operators_dict[node.op_type]=1
            name = node.op_type + str(self.operators_dict[node.op_type]-1)
            

            prev_layers = self.find_prev_layers(self.net, node) if len(self.net)>0 else None #self.net[-1] if len(self.net)>0 else None
            
          #  prev_layers = self.net[-1] if len(self.net)>0 else None
            if node.op_type in rd.mapping.keys():
              layer = rd.mapping[node.op_type](name, init, node, prev_layers=prev_layers, qf=qf, datatype=DEFAULT_DATATYPE)
              self.net.append(layer)
            elif node.op_type not in rd.supported_operators:
              print ("[READER] Operator not supported: {} in {}".format(node.op_type, node.name))
              exit(1)
            else:
              print ("[READER] Skipping operator: {} in {}".format(node.op_type, node.name))
              
        def find_prev_layers (self, net, node):
          prev = []
          inputs = ["_"+n for n in node.input]
          for layer in net:
            if layer.output_[0] in inputs:
              prev.append (layer)
          return prev if prev else None
        
        
        
        
        
        def sort_layers(self):
          print ("\n\n## Sorting layers ... ##\n")
          nothing_to_do = True
        
          done = False
          while not done:
            done = True
            for i, layer in enumerate(self.net):
              out = layer.output_[0]
              for j, l in enumerate(self.net):
                if out in l.input_ and j<i:                  
                  done = False
                  print ("Swapping {} and {}".format(layer.name, l.name))
                  self.net[i], self.net[j] = self.net[j], self.net[i] #swap
                  nothing_to_do = False
          if nothing_to_do:
            print ("  Nothing to do.")
          print ("Done.")
          
          
        def force_datatype (self, dtype, layers):
          print ("\n\n## Forcing datatype to {}... ##\n".format(dtype))
          nothing_to_do = True
          for l in layers:
            if l.datatype != dtype:
              print ("Converting parameters of {}".format(l.name))
              l.datatype = dtype
              l.set_parameters()
              nothing_to_do = False
              print ("Done.")
            
          if nothing_to_do:
            print ("  Nothing to do.")
          print ("Done.")
            
          
        def change_output_feature_maps (self, of):
          print ("\n\n## Changing output feature maps ... ##\n")
          nothing_to_do = True
          print (self.net[-1].osizes)
          if self.net[-1].osizes[0]!= of:
            self.net[-1].osizes[0]= of
            self.net[-1].load_params=False
            nothing_to_do = False
          print (self.net[-1].osizes)
          if nothing_to_do:
            print ("  Nothing to do.")
          print ("Done.")
            
        def batch_norm_folding(self,batch_norm=False):
          if batch_norm:
            print ("\n\n## Batch Normalization Folding ##\n")
            nothing_to_do = True
            for layer in self.net:
                 for l in self.net:
                   if l.operation=="BatchNormalization":
                     if (layer.output_[0] == l.input_[0] and layer.operation=="Conv"):  
                       print("Folding",layer.name,l.name)
                       for o in range (0, layer.osizes[0]): #do folding 
                          w_factor = l.weights[o]/(pow(l.var[o],1/2))
                          b_offset = l.biases[o] - l.mean[o]*w_factor
        
                          layer.biases[o] = layer.biases[o]*w_factor + b_offset

                          layer.weights[o][:][:][:]=[x*w_factor for x in layer.weights[o]]
                      
                      
                       self.init.parameters[layer.input_[1]] = layer.weights
                       
                       if len(layer.input_)<3:
                         layer.input_.append(layer.name + "_bias")
                         
                       self.init.parameters[layer.input_[2]] = layer.biases.astype(float)
                       
                       nothing_to_do = False
                         
                   
                       for next in self.net: #wiring fix
                         print (l.name, next.name)
                         if (l.output_[0] == next.input_[0]):
                           next.input_[0] = l.input_[0]
                           next.prev_layers=[layer]
                       self.net.remove(l) #remove batch norm
            if nothing_to_do:
              print ("  Nothing to do.")
            print ("Done.")

        def md2shift_(self): #mul and div to shift
          for layer in self.net: #Replace power of 2 mul and div with shifts 
            if layer.operation == "Mul":
              print(layer.name, layer.scaler_float)
            if layer.operation == "Mul" and len(layer.scaler_float)==1 and layer.datatype=='h':
              powerof2 = np.log2(layer.scaler_float[0])-int(np.log2(layer.scaler_float[0]))==0
              if powerof2:
                bits = int(np.log2(layer.scaler_float[0]))
                layer.name      = "LeftShift"+layer.name
                layer.operation = "BitShift"
                layer.direction = "LEFT"
                layer.shift     = bits
                
            if layer.operation == "Div" and len(layer.divider_float)==1:
              powerof2 = np.log2(layer.divider_float[0])-int(np.log2(layer.divider_float[0]))==0

              if powerof2:
                bits = int(np.log2(layer.divider_float[0]))
                layer.shift     = bits

        def md2shift(self): #mul and div to shift
          print ("\n\n## Convert power of 2 mul and div to shift ##\n")
          nothing_to_do = True
          for layer in self.net: #Replace power of 2 mul and div with shifts 
            if layer.operation == "Mul" and len(layer.scaler_float)==1 and layer.datatype=='h' and layer.scaler_float[0]!=0:
              print (layer.scaler_float[0])
              print (layer.input_)
              print (layer.name)
              
              powerof2 = np.log2(layer.scaler_float[0]) - int(np.log2(layer.scaler_float[0]))==0
              
              if powerof2:
                prevname = layer.name
                bits = int(np.log2(layer.scaler_float[0]))
                layer.name      = "LeftShift"+layer.name
                layer.operation = "BitShift"
                layer.direction = "LEFT"
                layer.shift     = bits
                print("{} -> {}".format(prevname, layer.name))
                print("  y=x<{}".format(layer.shift))
                nothing_to_do = False
                
            if layer.operation == "Div" and len(layer.divider_float)==1 and layer.datatype=='h':
              powerof2 = np.log2(layer.divider_float[0])-int(np.log2(layer.divider_float[0]))==0

              if powerof2:
                prevname = layer.name
                bits = int(np.log2(layer.divider_float[0]))
                layer.name      = "RightShift"+layer.name
                layer.operation = "BitShift"
                layer.direction = "RIGHT"
                layer.shift     = bits
                print("{} -> {}".format(prevname, layer.name))
                print("  y=x>{}".format(layer.shift))
                nothing_to_do = False
          if nothing_to_do:
              print ("  Nothing to do.")
          print ("Done.")
            
        def mul_rshift_folding(self):
            print ("\n\n## Mul-RShift folding ... ##\n")
            nothing_to_do = True
           # for layer in self.net: #Replace mul followed by right shift with a single layer
            for layer in self.net:
                     for l in self.net:
                       if l.operation=="BitShift":
                         if (layer.output_[0] == l.input_[0] and layer.operation=="Mul"):
                           print("Replacing",layer.name,l.name)
                           
                           layer.qf += l.shift
                       
                           for next in self.net: #wiring fix
                             if (l.output_[0] == next.input_[0]):
                               next.input_[0] = l.input_[0] 
                               next.prev_layers=[layer]
                           self.net.remove(l) 
                           
                           nothing_to_do = False
            if nothing_to_do:
              print ("  Nothing to do.")
            print ("Done.")
            
            
        def conv_relu_folding(self):
            print ("\n\n## Conv-Relu folding ... ##\n")
            nothing_to_do = True
           # for layer in self.net: #Replace mul followed by right shift with a single layer
            for layer in self.net:
                     for l in self.net:
                       if l.operation=="Relu":# or l.operation=="LeakyRelu":
                         if (layer.output_[0] == l.input_[0] and layer.operation=="Conv"):
                           print("Replacing",layer.name,l.name)
                           
                           layer.activation = True
                           
#                           if l.operation=="LeakyRelu":
#                             layer.leakyReluAlpha = l.leaky_relu_alpha
                             
                       
                           for next in self.net: #wiring fix
                             if (l.output_[0] == next.input_[0]):
                               next.input_[0] = l.input_[0] 
                               next.prev_layers=[layer]
                           self.net.remove(l)
                           nothing_to_do = False
            if nothing_to_do:
              print ("  Nothing to do.")
            print ("Done.")
       
       
        def get_nextLayer (self, layer):
          for l in self.net:
            if layer.output_[0] in l.input_:
              return l
              
        def removeOperators (self, operators):
          print ("\n\n## Removing operators ... ##\n")
          nothing_to_do = True
          i = 0
          for layer in self.net: 
            #print (layer.name)
            if layer.operation in operators:
              print ("{} found".format(layer.operation))
              prev_layers = layer.prev_layers
              next_layer = self.get_nextLayer(layer)


              for j, input_ in enumerate(next_layer.input_):
                if input_ == layer.output_[0]:
                  next_layer.input_[j] = prev_layers[0].output_[0]
              next_layer.prev_layers = prev_layers
              print ("[WARNING] Removed operator: " + layer.name)
              self.net.remove(layer)
              nothing_to_do = False
            
            i+=1
          if nothing_to_do:
              print ("  Nothing to do.")
          print ("Done.")
          
        def remove_redundant_Pad (self):
          print ("\n\n## Remove redundand pad ... ##\n")
          for layer in self.net: 
            #print (layer.name)
            if layer.operation =="Pad":
              keep = False
              for p in layer.padding:
                if p!=0:
                  keep = True
              if keep==False:
                print ("Redundand Pad layer found. padding: {}".format(layer.padding))
                prev_layers = layer.prev_layers
                next_layer = self.get_nextLayer(layer)


                for j, input_ in enumerate(next_layer.input_):
                  if input_ == layer.output_[0]:
                    next_layer.input_[j] = prev_layers[0].output_[0]
                next_layer.prev_layers = prev_layers
                print ("[WARNING] Removed operator: " + layer.name)
                self.net.remove(layer)
            
        def set_load_params (self, load_params):
          for layer in self.net:
            layer.load_params = load_params
            
        def removePreProcessing (self, keep_operators):
          print ("\n\n## Remove pre-processing ... ##")
          print ("Stop removing when find: ", keep_operators)
          while self.net[1].operation not in keep_operators:
              layer = self.net[1]
            #  print ("name: " + layer.name + layer.operation)
              #print ("{} found".format(operator))
              prev_layers = [self.net[0]]
              next_layer = self.net[2]
              
             # print ("out: " + layer.output_[0])
              for j, input_ in enumerate(next_layer.input_):
              #  print ("in " + input_)
                if input_ == layer.output_[0]:
                  next_layer.input_[j] = prev_layers[0].output_[0]
                  
              next_layer.prev_layers = prev_layers
              print ("[WARNING] Removed operator: " + layer.name)
              self.net.remove(layer)
          print ("Done")
          #exit(1)
            
            
        def stride2subsampling(self, neuraghe):
          if neuraghe:
            for layer in self.net: # Stride
              if (layer.operation=="Conv" and layer.stride>1):
                new_layer =  rd.MaxPoolReader("{}_Subsampling".format(layer.name), node, self.init, qf=self.qf, datatype='h')


                new_layer.operation = "MaxPool"

                new_layer.isizes = list(layer.isizes)

                new_layer.osizes = list(layer.osizes)

                new_layer.kernel = layer.stride
                new_layer.stride = layer.stride
                new_layer.padding[0] = 0
                new_layer.padding[1] = 0
                new_layer.padding[2] = 0
                new_layer.padding[3] = 0
                new_layer.pool_mode  = 1
                
                new_layer.output_[0]= layer.output_[0]
                new_layer.input_[0] = new_layer.name + "edge"
                layer.output_[0] = new_layer.input_[0]
                
                
                layer.osizes = list(layer.isizes)

                
               # for l in self.net:
                   #if (layer.output_[0] == l.input_[0]):
                  #    l.input_[0]=l.input_[0]+"0"
                 #     new_layer.output_[0]=l.input_[0]
                self.net.insert(self.net.index(layer)+1, new_layer)
      
        def check_input_NEUConv(self):
          print ("\n\n## check_input_NEUConv ... ##\n")
          done = 0
          n=0
          while done == 0:
            done = 1
            for i, l in enumerate(self.net):
              if l.operation == "Conv":
                if l.isizes[2]%2 == 1:
                  
                  padLayer = rd.PadReader("Pad"+l.name, self.init, prev_layers=list(l.prev_layers), qf=self.qf, datatype='h')
                  padLayer.operation = "Pad"
                  padLayer.isizes=list(l.isizes)
                  padLayer.padding = [0,1,0,0]
                  padLayer.osizes=list(padLayer.isizes)
                  padLayer.osizes[2]+=1
                  padLayer.input_=[l.input_[0]]
                  padLayer.output_=["padout"+str(n)]
                  
                  
                  
                  l.input_[0] = padLayer.output_[0]
                  l.isizes = list(padLayer.osizes)
                  l.osizes[2]+=1
                  l.prev_layers=[padLayer]
                  
                  cropLayer = rd.CropReader("Crop"+l.name, self.init, prev_layers=[self.net[i]], qf=self.qf, datatype='h')
                  cropLayer.operation = "Crop"
                  cropLayer.isizes=list(l.osizes)
                  cropLayer.crop = [0,1,0,0]
                  cropLayer.osizes=list(cropLayer.isizes)
                  cropLayer.osizes[2]-=1
                  cropLayer.input_=list(l.output_)
                  cropLayer.output_=["cropout"+str(n)]
                  print ("Inserting pad layer before {}".format(l.name))
                  print ("Inserting crop layer after {}".format(l.name))
                  try:
                    self.net[i+1].input_[0]=cropLayer.output_[0]
                    self.net[i+1].prev_layers=[cropLayer]
                  except IndexError:
                    pass
                  self.net.insert(i+1, cropLayer)
                  self.net.insert(i, padLayer)
                  n+=1
                  done = 0
                  break
                  
        def fold_ReLU_in_NEUConv(self):
          print ("\n\n## fold_ReLU_in_NEUConv ... ##\n")
          done = 0
          while done == 0:
            done = 1
            for i, l in enumerate(self.net):
              if l.operation == "Relu":
                if l.prev_layers[0].operation == "Conv":
                
                  l.prev_layers[0].activate = 1;
                  self.net[i+1].input_=[l.prev_layers[0].output_[0]]
                  self.net.remove(l)
                  
                  done = 0
                  break
      
        def add_regionLayer(self, anchors, classes = None):
          print ("############# YOLO LIKE Model conversion enabled! #####################")
          name  = "Region0"
          prev_layer = self.net[-1]
          layer = rd.RegionReader (name, self.init, prev_layers=[prev_layer], qf=self.qf, datatype='f')
          
          layer.output_ = [o for o in prev_layer.output_]
          layer.input_ = [name+'_input']
          layer.anchors = anchors
          layer.classes = classes
          prev_layer.output_ = [i for i in layer.input_]
          self.net.append(layer)
              
        def add_flatten (self):
        
          for index, layer in enumerate(self.net):
            if layer.operation == 'Flatten':
              return
              
            if layer.operation == 'Gemm':
              gemmLayer = layer
              
              flatten_Layer           = rd.FlattenReader ("Flatten", self.init, prev_layers=list(gemmLayer.prev_layers), qf=self.qf, datatype='h')
              flatten_Layer.operation = 'Flatten'
              flatten_Layer.input_    = [gemmLayer.prev_layers[0].output_[0]]
              flatten_Layer.output_   = ['flatten_out']
              self.net.insert(index, flatten_Layer)
              
              gemmLayer.prev_layers = [flatten_Layer]
              gemmLayer.input_[0]   = flatten_Layer.output_[0]
              gemmLayer.isizes      = [s for s in flatten_Layer.osizes]
              
              return
        
        def to_yolo (self, outsizes):
          
          leaky_relu_alpha = 0
          reluop = 'Relu'
          for i, l in enumerate(self.net): 
            if l.operation == 'LeakyRelu':
              reluop = 'LeakyRelu'
              leaky_relu_alpha = l.leaky_relu_alpha
            
            if l.operation == 'MaxPool':
              last_maxpool_layer = i
#              if self.net[last_maxpool_layer+1].operation=='BatchNormalization':
#                last_maxpool_layer = last_maxpool_layer+1
#                if self.net[last_maxpool_layer+1].operation=='Relu' or self.net[last_maxpool_layer+1].operation=='LeakyRelu':
#                  last_maxpool_layer = last_maxpool_layer+1

          #last_maxpool_layer -= 1
          
          FORCE_STRIDE_TO_1 = True
          
          if FORCE_STRIDE_TO_1:
#            if self.net[last_maxpool_layer].isizes[1]%2 == 1:
#              self.net[last_maxpool_layer].padding = [0,1,0,1]
            self.net[last_maxpool_layer].setStride( [1,1])
          
          out = self.net[-1].output_
          for i in range (last_maxpool_layer+1, len(self.net)):#remove next layers
            
            del self.net[-1]
          
          NUM_OF_GROUPS = 2
          
          for i in range(NUM_OF_GROUPS):
          
          
            layer           = rd.ConvReader ("ConvYOLO3x3_{}".format(i), self.init, node = None, prev_layers=list(self.net[-1].prev_layers), qf=self.qf, datatype='h')
            layer.operation = 'Conv'
            layer.load_params = False
            layer.kernel    = [3,3]
            layer.stride    = [1,1]
            layer.dilation  = [1,1]
            layer.group     = 1
            layer.padding   = [1,1,1,1]
            layer.input_    = [self.net[-1].output_[0]]
            layer.output_   = out
            layer.isizes = list(self.net[-1].osizes)
            layer.osizes = [1024]+layer.isizes[1:]
            self.net.insert(len(self.net), layer)
            
            layer           = rd.BatchNormalizationReader ("BNYOLO3x3_{}".format(i), self.init, node = None, prev_layers=self.net[-1].prev_layers, qf=self.qf, datatype='h')
            layer.operation = 'BatchNormalization'
            layer.load_params = False
            layer.batch_eps               = 1e-05
            layer.batch_spatial           = None
            layer.batch_momentum          = 0.1
            layer.input_    = [self.net[-1].output_[0]]
            layer.output_   = out
            layer.isizes = list(self.net[-1].osizes)
            layer.osizes = [1024]+layer.isizes[1:]
            self.net.insert(len(self.net), layer)
            
            layer           = rd.ReluReader (reluop+"YOLO3x3_{}".format(i), self.init, node = None, prev_layers=self.net[-1].prev_layers, qf=self.qf, datatype='h')

            layer.operation = reluop
            layer.leaky_relu_alpha = leaky_relu_alpha

            layer.load_params = False
            layer.input_    = [self.net[-1].output_[0]]
            layer.output_   = out
            layer.isizes = list(self.net[-1].osizes)
            layer.osizes = [1024]+layer.isizes[1:]
            self.net.insert(len(self.net), layer)
          
          
          
          
          conv_Layer           = rd.ConvReader ("ConvYOLO1x1", self.init, node = None, prev_layers=self.net[-1].prev_layers, qf=self.qf, datatype='h')
          conv_Layer.operation = 'Conv'
          conv_Layer.load_params = False
          conv_Layer.kernel    = [1,1]
          conv_Layer.stride    = [1,1]
          conv_Layer.dilation  = [1,1]
          conv_Layer.group     = 1
          conv_Layer.padding   = [0,0,0,0]
          conv_Layer.input_    = [self.net[-1].output_[0]]
          conv_Layer.output_   = out
          conv_Layer.isizes = list(self.net[-1].osizes)
          
          if outsizes[1]==None:
            outsizes=[outsizes[0]] + conv_Layer.isizes[1:]
          
          conv_Layer.osizes = outsizes
          self.net.insert(len(self.net), conv_Layer)
          
          
          
          
        def datatype_conversion (self):
            print ("\n\n## Inserting datatype conversion operators... ##\n")
            nothing_to_do = True
          
            fixed_point_format = ['b','h']

            i = 0
            converter_count=0
            skip = False
            prev_layer=None
            for layer in self.net:
              if skip:
                skip = False
                continue
                
              if i>0:
                print ("{}: {}".format(layer.name, layer.datatype))
                
                #prev_layer = self.net[i-1]
                
                for prev_layer in layer.prev_layers:
                  try:
                    prev_datatype = prev_layer.datatype_o
                  except:
                    prev_datatype = prev_layer.datatype
                  
                  try:
                    next_datatype = layer.datatype_i
                  except:
                    next_datatype = layer.datatype


                  if next_datatype!=prev_datatype and prev_layer.operation !='Input' and \
                      (next_datatype not in fixed_point_format or prev_datatype not in fixed_point_format):
                  
                  
                    if prev_datatype in fixed_point_format:
                      name = "Fixed2Float{}".format(converter_count)
                      new_layer =  rd.Fixed2FloatReader(name, self.init, prev_layers=[prev_layer], qf=self.qf, datatype='f')
                      new_layer.operation = 'Fixed2Float'
                      print ("[WARNING] DATATYPE changes, inserting conversion layer. h->f")

                    else:
                      name = "Float2Fixed{}".format(converter_count)
                      new_layer =  rd.Float2FixedReader(name, self.init, prev_layers=[prev_layer], qf=self.qf, datatype='h')
                      new_layer.operation = 'Float2Fixed'
                      print ("[WARNING] DATATYPE changes, inserting conversion layer. f->h")
                      

                    
                    new_layer.input_  = [prev_layer.output_[0]] 
                    new_layer.output_ = [new_layer.name + "edge"]
                    for j, input_ in enumerate(layer.input_):
                      if input_ == prev_layer.output_[0]:
                        layer.input_[j]      = new_layer.output_[0]
                    layer.prev_layers=[new_layer]
                    
                    self.net.insert(i, new_layer)
                    skip=True
                    i+=1 #add one to the total count of the layers
                    converter_count+=1
                    
                    nothing_to_do = False
                  
              
              
              i+=1
              
            if nothing_to_do:
              print ("  Nothing to do.")
            print ("Done.")
      
        def remove_layer (self, layer_name):
          for i, l in enumerate(self.net):
            if l.name == layer_name:
              to_be_removed = l
              try:
                prev_l = [lay for lay in to_be_removed.prev_layers]
              except:
                prev_l = None
              try:
                next_l=[]
                for lay in self.net:
                  if to_be_removed.output_[0] in lay.input_:
                    next_l.append(lay)
                
              except:
                next_l = None
              
              break
          for n in next_l:
            n.input_  = [pl.output_[0] for pl in prev_l]
            n.prev_layers= [l for l in to_be_removed.prev_layers]
            
          self.net.remove(to_be_removed)
#          for l in self.net:
#            print (l.name)
#            print ("  i: ", l.input_)
#            print ("  o: ", l.output_)
          
          #exit(8)
      
        def quantization_folding(self):
              print("\n\n## Quantization folding ... ##\n")
              nothing_to_do = True
              self.RPI=0
              
              print("  looking for BN Mul after Conv...")
              done = False
              while not done:
                done = True
                for layer in self.net:
                  for l in self.net: 
                    if layer.operation=="Conv" and l.operation=="Mul":
  #                    print(layer.name)
  #                    print(l.name)
  #                    print(layer.output_)
  #                    print(l.input_)
  #                    print(layer.osizes)
  #                    print(l.osizes)
  #                    exit(8)
                      if layer.output_[0] == l.input_[0] and layer.osizes[0]==l.osizes[0] and l.osizes[0]==len(l.scaler_float): #mul bn
                        print("    found", layer.name, l.name)
                        
                        layer.mulbn_name = l.name
                        layer.output_[0] = l.output_[0]
                        layer.mulbn_scaler = l.scaler_float
                      #  print (layer.mulbn_scaler)
                        self.remove_layer(l.name)
                        done = False
                        nothing_to_do = False
                        break
                  if not done:
                    break
                    
                    
              print("  looking for BN Add after Conv...")
              done = False
              while not done:
                done = True
                for layer in self.net:
                  for l in self.net: 
                    if layer.operation=="Conv" and l.operation=="Add" and l.osizes[0]==len(l.bias_float):
                      if layer.output_[0] == l.input_[0]: #add bn
                        print("    found", layer.name, l.name)
                        layer.addbn_name = l.name
                        layer.output_[0] = l.output_[0]
                        layer.addbn_bias = l.bias_float
                        self.remove_layer(l.name)
                        done = False
                        nothing_to_do = False
                        self.RPI=1
                        layer.RPI=1
                        break
                  if not done:
                    break
            
              print("  looking for Mul after Conv...\n")
              done = False
              while not done:
                done = True
                for layer in self.net:
                  for l in self.net: 
                    if layer.operation=="Conv" and l.operation=="Mul": 
#                      print(layer.name)
#                      print(l.name)
#                      print(layer.output_)
#                      print(l.input_)
#                      print(layer.osizes)
#                      print(l.osizes)
#                      exit(8)
                      if layer.output_[0] == l.input_[0]: #mul relu
                        print("    found", layer.name, l.name)
                        layer.mulrelu_name = l.name
                        layer.output_[0] = l.output_[0]
                        layer.mulrelu_scaler = l.scaler_float
                        self.remove_layer(l.name)
                        done = False
                        nothing_to_do = False
                        break
                  if not done:
                    break
             
              print("  looking for Div after Conv...\n")
              done = False
              while not done:
                done = True
                for layer in self.net:
                  for l in self.net: 
                    if layer.operation=="Conv" and l.operation=="Div": 
                      if layer.output_[0] == l.input_[0]: #div relu
                        print("    found", layer.name, l.name)
                        layer.divrelu_name = l.name
                        layer.output_[0] = l.output_[0]
                        if hasattr(l,'shift'):
                          if l.shift is not None:
                            layer.divrelu_divider = l.shift
                        else:
                          layer.divrelu_divider = l.divider_float
                        self.remove_layer(l.name)
                        done = False
                        nothing_to_do = False
                        break
                  if not done:
                    break
              
              print("  looking for Clip after Conv...\n")
              done = False
              while not done:
                done = True
                for layer in self.net:
                  for l in self.net: 
                    if layer.operation=="Conv" and l.operation=="Clip":
                      if layer.output_[0] == l.input_[0]: #clip
                        print("    found", layer.name, l.name)
                        layer.clip_max=l.clip_max
                        layer.clip_min=l.clip_min
                        layer.output_[0] = l.output_[0]
                        if layer.clip_max<=255:
                          layer.datatype="b"
                        else:
                          layer.datatype="h"
                          
                        self.remove_layer(l.name)
                        nothing_to_do = False
                        break
                  if not done:
                    break
             
              for layer in self.net:
                for l in self.net: 
                  if layer.operation=="Conv":
                    if layer.output_[0] == l.input_[0]:
                      l.prev_layers=[layer]
              
              if nothing_to_do:
                print ("  Nothing to do.")
            
            
              for layer in self.net:
                if layer.operation=="Conv":
                  print ("Conv " + layer.name)
                  if self.RPI==1:
                    layer.RPI=True
                    
                    if layer.addbn_bias == None:
                      print ("addbn_bias set to default")
                      layer.addbn_bias = [0]*layer.osizes[0]
                      
                    if layer.mulbn_scaler == None:
                      print ("mulbn_scaler set to default")
                      layer.mulbn_scaler = [1]*layer.osizes[0]
                      
                    if layer.mulrelu_scaler == None:
                      print ("mulrelu_scaler set to default")
                      layer.mulrelu_scaler = [1]
                      
                    if layer.divrelu_divider == None:
                      print ("divrelu_divider set to default")
                      layer.divrelu_divider = [1]
                    
                    if layer.clip_max == None:
                      print ("clip_max set to default")
                      layer.clip_max = 10000000000
                      
                    if layer.clip_min == None:
                      print ("clip_min set to default")
                      layer.clip_min = -10000000000
                    last_conv = layer
                last_layer = layer
              
              if self.RPI==1 and last_layer.operation == "Region":
                last_conv.datatype= 'f'
                last_conv.datatype_i= 'h'
                last_conv.datatype_o= 'f'
              
              print ("Done.")
#              import IPython
#              IPython.embed()
              return self.RPI
    
         
    
        def padlayer_folding(self):
          for layer in self.net:
             if layer.operation=="Conv":
               if layer.prev_layers[0].operation=="Pad":
                   l=layer.prev_layers[0]
                   layer.padding=l.padding
                  # print(layer.name)
                  # print(layer.padding)
                   layer.input_[0]=l.input_[0]
                   layer.prev_layers[0]=l.prev_layers[0]
                   self.net.remove(l)

         
         
        def set_lkam_inference(self):
           flag_list=[]
           LKAM=0
           for layer in self.net:
             for l in self.net: 
                if (layer.operation=="Mul" and layer.output_[0]==l.input_[0] and l.operation=="Sigmoid"):
                   l.input_[0]=layer.input_[0]
                   self.net.remove(layer)
           for layer in self.net:
             for l in self.net: 
                if (layer.operation=="Conv" and layer.kernel==1 and layer.output_[0]==l.input_[0] and l.operation=="Sigmoid"):
                   layer.output_[0]=l.output_[0]
                   self.net.remove(l)
                   LKAM=1
           for layer in self.net:
             for l in self.net: 
               if (layer.operation=="Conv" and layer.kernel==1 and l.operation=="Mul" and LKAM==1): #mul sigmoid mul : lkam pattern
                 if (layer.output_[0]==l.input_[1]):
                   layer.output_[0]=l.output_[0]
                   for l2 in self.net:
                     if l2.input_[0] == l.output_[0]:
                        l2.prev_layers = l.prev_layers[1]

                        try:
                          print(l2.name, l2.special_attr_dict['IF_flag'])
                        except:
                          pass
                   self.net.remove(l)
           if LKAM:
              print("LKAM INFERENCE\n")
              for layer in self.net:
                for l in self.net:
                  if l.input_[0]==layer.input_[0] and l.operation=="Conv" and layer.name != l.name:
                    l.special_attr_dict['OF_flag'] = "Flag_{}".format(l.name)
                    flag_list.append("Flag_{}".format(l.name))
              print(flag_list)
              for layer in self.net:
               if (layer.operation=="Conv" and layer.kernel==1): 
                    for l in self.net:
                      if l.operation == "Relu":
                        node = l.node
                    new_layer =  rd.ReluReader(flag_list[0], self.init, node=node, qf=self.qf, datatype=layer.datatype)  #inference : Sigmoid-> Relu
                    flag_list.remove(flag_list[0])
                    new_layer.operation = "Relu"


                    new_layer.isizes = list(layer.isizes)

                    new_layer.osizes = list(layer.osizes)
                    
                    new_layer.prev_layers = [layer]
                    new_layer.special_attr_dict['isflag'] = 1  #add threshold 1
                    new_layer.output_[0]= layer.output_[0]
                    new_layer.input_[0] = new_layer.name + "edge"
                    layer.output_[0] = new_layer.input_[0]
                
                    self.net.insert(self.net.index(layer)+1, new_layer)
              for layer in self.net:  #assign flags
                for l in self.net:
                  if (l.prev_layers is not None):
                    if l.prev_layers[0].operation=="Conv":
                      l.special_attr_dict['IF_flag']=l.prev_layers[0].special_attr_dict['OF_flag'] 
                    else:
                      l.special_attr_dict['IF_flag']=l.prev_layers[0].special_attr_dict['IF_flag']       
              for layer in self.net:  #reorder layers
                 for l in self.net:
                    if layer.operation=="Conv":
                      if layer.special_attr_dict['OF_flag'] is not None and l.name==layer.special_attr_dict['OF_flag']:
                         self.net.remove(layer)
                         self.net.insert(self.net.index(l)+1,layer)   

           

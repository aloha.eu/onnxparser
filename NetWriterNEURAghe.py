
# Copyright (c) 2018
# by Mauro Gioi       - gioimau@gmail.com
#    Gianfranco Deriu - gianfranco.deriu@unica.it
#    Paolo Meloni     - paolo.meloni@diee.unica.it
#
# Universita' di Cagliari
# www.unica.it
#
# All rights reserved.
#
#
# If you use this script in your work, please cite us.
# --------------------------------------------------------------------
# The ONNX2Pytorch script is
# Copyright (c) 2018
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and
# its associated documentation for any purpose and without fee is
# hereby granted, provided that the above copyright notice appears in
# all copies, and that both that copyright notice and this permission
# notice appear in supporting documentation, and that the name of
# University of Cagliari or the authors not be used in advertising or publicity
# pertaining to distribution of the software without specific, written
# prior permission.

from Initializer import *
import sys, math, os
import numpy as np
import onnxReaders as rd
import NEURAgheWriters as wr


class NetWriterNEURAghe():

        def __init__ (self, model_file, path, app_name,  weights_path, readers, init, graph, input_size, **kwargs):
          
          try:
            qf = kwargs['qf']
          except:
            qf = 8
            
          try:
            yolo_like = kwargs['yolo_like']
          except:
            yolo_like = False
          
          self.nRow = kwargs['nRow']
          self.nCol = kwargs['nCol']
          
          if self.nRow == None or self.nCol == None:
            print ("\n[ERROR] [NEURAghe Writer] nRow and nCol must be defined. ")
            print ("nRow:", self.nRow, " - nCol:", self.nCol)
            exit(1)
          
          self.app_name = app_name
          self.path = path
          self.smart_memalloc = kwargs['smart_memalloc']
          
          self.readers = readers
          self.weights_path = weights_path
          
          self.model_file     = model_file
          self.neuraghe       = True
          self.yolo_like      = yolo_like
          self.qf             = qf
          
          self.init     = init
          
          self.input_size     = input_size
          
          self.net = []
          

          i=0
          for r in readers:
                       
            if r.operation in wr.mapping.keys():
              w = wr.mapping[r.operation](r)
              if r.operation == 'Conv':
                w.nRow = self.nRow
                w.nCol = self.nCol
                
              self.net.append(w)
            elif r.operation not in wr.supported_operators:
              print ("Operator not supported: {} in {}".format(r.operation, r.name))
              exit(1)
            else:
              print ("Skipping operator: {} in {}".format(r.operation, r.name))
            
            
        
        def optimizeNet (netReader, batch_norm=False, neuraghe=False):
           netReader.removePreProcessing(['Conv', 'Pad', 'Gemm'])
           netReader.removeOperators(['Cast', 'Dropout', 'Flatten'])
           netReader.fold_ReLU_in_NEUConv()
           netReader.batch_norm_folding(batch_norm)
           netReader.check_input_NEUConv()
           netReader.md2shift()
           netReader.mul_rshift_folding()
           netReader.stride2subsampling(neuraghe)
           netReader.datatype_conversion()

            
            
            
            
            
            
            
        def save_parameters(self):
        
            print ("Extracting weights...")
            
            os.makedirs(self.weights_path, exist_ok=True)

            for layer in self.net:
              layer.save_parameters(self.weights_path)
          
          
            maxog = 4
            conv_line = '{"name":"%s","weights":{"file":"%s","size":[%d,%d,%d,%d]},"bias":{"file":"%s","size":[%d]}, "maxog":%d}\n'
            with open(os.path.join(self.weights_path,"conv.json"), 'w') as f:
              for layer in self.net:
                if layer.operation=="Conv": # and layer.weights != None and layer.biases != None:
                  w = layer.name + "_weights.bin"
                  b = layer.name + "_biases.bin"
                  wshape = [layer.osizes[0], layer.isizes[0], *layer.kernel]
                  bshape = [layer.osizes[0]]
                  
                  f.write(conv_line%(layer.name, w, wshape[0], wshape[1], wshape[2], wshape[3], b, bshape[0], maxog))
        
          
      
        
        
        
        
        
        
        def net_description(self):
           
          net_description = "Source file: %s\n\n" % self.model_file
          net_description +="%-20s %5s %5s %5s %5s %5s %5s %10s %20s\n"%("Layer", "IF", "ih", "iw", "OF", "oh", "ow", "datatype", "prev_layers")
          
          for layer in self.net:
            net_description+= "%-20s %5d %5d %5d %5d %5d %5d %10s %20s\n"%(layer.name,
             layer.isizes[0],
              layer.isizes[1], 
              layer.isizes[2],
               layer.osizes[0], 
               layer.osizes[1], 
               layer.osizes[2], 
               layer.datatype, 
               str([l.name for l in layer.prev_layers]))
          
          net_description += "\n\nWiring: \n\n"
          net_description +="%-20s %20s %20s\n"%("next_layers", "Layer", "prev_layers")
          
          for layer in self.net:
            next_layers = []
            prev_layers = []
            
            for l in self.net:
              #print (l.name)
              #print (l.input_)
              if layer.output_[0] in l.input_:
                next_layers.append(l)
              if l.output_[0] in layer.input_:
                prev_layers.append(l)
            net_description+= "%-20s %20s %20s\n"%(str([l.name for l in next_layers]), layer.name, str([l.name for l in prev_layers]))
          
          
          
          
          
          return net_description
            
            
            
         
        def get_mem_allocation_string(self):

          string = ""

          for layer in self.net:
            if layer.operation in wr.supported_operators:
              if layer.operation=="Region":
                string+=""
              else:
                if (layer.osizes[1]%2  and self.neuraghe and layer.operation!="Gemm"):   #conv on odd images  SOLO PER LE CONV DA ESEGUIRE IN HW
                  L_oh=layer.osizes[1]+1
                  L_ow=layer.osizes[2]+1
                else:
                  L_oh=layer.osizes[1]
                  L_ow=layer.osizes[2]
                size = layer.osizes[0] * L_oh * L_ow

                string += "{}_param->output = (DATA*)neu_DDRmalloc({}*sizeof(DATA));\n".format(layer.name, size)
          
          string += "\n"
          return string
          
          
          
          
          
        def get_smart_mem_allocation_string(self):
        #  print (self.osizes[0])
          names = ['A','B','C']
          
          string =  "DATA * %s = (DATA*)neu_DDRmalloc(%d*sizeof(DATA));\n" % (names[0],5000000)
          string += "DATA * %s = (DATA*)neu_DDRmalloc(%d*sizeof(DATA));\n" % (names[1],5000000)
          string += "DATA * %s = (DATA*)neu_DDRmalloc(%d*sizeof(DATA));\n" % (names[2],5000000)
  
          #allocate space for the output
          i=0
          for layer in self.net:            
            if layer.operation in wr.supported_operators:
              if layer.operation=="Region":
                string+=""
              else:
                string += "%s_param->output = %s;\n" % (layer.name, names[i])
                i+=1
                if i==3:
                  i=0
            ##    if (layer.operation=="Conv" and layer.stride>1):
            ##       string += "%s_Max_param->output = %s;\n" % (layer.name, names[i])  
            ##    i+=1
            ##    if i==3:
            ##      i=0
          string += "\n"
          return string
          
#---------------------------------------------------------------------------------------------------------------------------------------------
        def print_net(self):
        
                
                self.results_datatype = "float" #"DATA"
                
                
                name_file        = os.path.join(self.path, self.app_name +".cpp")
                
                print (self.net_description())
                
                print ("Creating file {} ...".format(name_file))

                file_name_weights = "weights.npz"
                file_name_bias = "bias.npz"
                name_file = open(name_file,"w")
                name_file.write("\n//This code is auto_generated.//\n")
                name_file.write("\n//This file must be embedded in the host application that will provide the data to be processed.\n\n\n\n")
                name_file.write("/* ################## Net description ###################\n")
                name_file.write(self.net_description())
                name_file.write("#########################################################*/\n")
                name_file.write("\n#include "'"%s.h"'" \n"%self.app_name)
                name_file.write("#include "'"assert.h"'"\n")
                name_file.write("#include "'"string.h"'"\n")
                name_file.write("#define ELEM_PER_WS 32\n")
                name_file.write("\n// global variables\n\n")
                name_file.write("#if !(_RELEASE_)\n")
                name_file.write("     #define _rprintf_(...) printf(__VA_ARGS__)\n")
                name_file.write("     #define spatconv_forward_hw(...) spatconv_forward_hw_sync(__VA_ARGS__)\n")

                name_file.write("     #ifdef _DEBUG_\n")
                name_file.write("         #define _dprintf_(...) printf(__VA_ARGS__)\n")
                name_file.write("         #define _dprint_data_(...) print_data(__VA_ARGS__)\n")

                name_file.write("        #else\n")
                name_file.write("         #define _dprintf_(...) ;\n")
                name_file.write("         #define _dprint_data_(...) ;\n")

                name_file.write("        #endif\n")
                name_file.write("#else\n")
                name_file.write("       #define _rprintf_(...) ;\n")
                name_file.write("       #define _dprintf_(...) ;\n")
                name_file.write("       #define _dprint_data_(...) ;\n")

                name_file.write("#endif\n\n")
                name_file.write("\n// global variables\n")

                name_file.write("\nSOCMAP socs[_NCLUSTER_];\n")
                name_file.write("DATA* wPointer;\n")
                name_file.write("DATA* aPointer;\n")

#------------stampo il codice per dichiarare e inizializzare  le strutture dei layer  conv e gemm (fully connected layer) -------------------------------------------------------

                for layer in self.net:
                    name_file.write(layer.get_declaration_string())  
                    
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

                name_file.write("\nvoid cnnMain(DATA* image, {}** results);\n".format(self.results_datatype))
                name_file.write("int  resultsProcessing({}* results, int size);\n".format(self.results_datatype))
                name_file.write("\n\n")
                
                name_file.write("\nvoid getInputSize(VARSIZE* inSz)\n{ // CHW\n")
                
                for i,d in enumerate(self.input_size):
                  name_file.write("        inSz[{}]={};\n".format(i, d))
                  
                name_file.write("  }\n")
                name_file.write("\n\n")
                name_file.write("\nvoid getOutputSize(VARSIZE* output_size)\n{ \n")
                name_file.write("        *output_size={};\n".format(self.net[-1].osizes[0]))
                name_file.write("  }\n")
                
                
                
                name_file.write('\nvoid print_array (DATA * array, int len, int qf = 0){\n')
                name_file.write('  printf ("\\n\\n");\n')
                name_file.write('  for (int i=0; i<len; i++){\n')
                name_file.write('    printf ("[%03d] 0x%08x -> %08d -> %02.3f\\n", i, array[i], array[i], FIXED2FLOAT(array[i], qf));\n')
                name_file.write('  }\n')
                name_file.write('}\n')
                
                name_file.write("void init_platform(char* bitstream){\n	init_soc(socs, &wPointer, _MAXMEM_, 0, bitstream);\n}\n\n\n")

#----------------------------------------------- main init --------------------------------------------------------------------------------------------------------------------------------------
                name_file.write("\nvoid cnnMainInit(VARNAME load_data_dir)\n{")
                name_file.write("\n        #define _NCOL_ {}\n".format(self.nRow))
                name_file.write("        #define _NROW_ {}\n".format(self.nRow))
                name_file.write("        int minIF=0;\n")
                name_file.write("        int wsize=0;\n")
                name_file.write("        int minOF=0;\n")
                name_file.write("        int IG=0, OG=0;\n")

                name_file.write("VARNAME filename;\n")


                for layer in self.net:
                    if layer.to_be_initialized:
                        name_file.write( layer.get_initialization_string())
                        
                        
                name_file.write("\n\n// ##################################################################\n")
                name_file.write("// ######################## WEIGHTS LOADING #######################\n")
                name_file.write("// ################################################################\n\n")
                
                name_file.write("//\Convolutional layers, FC layers (or GEMM or MatMul layers) and BN layers have weights and biases\n")
                name_file.write("//\tConvolutional layers will be accelerated and their weights must be stored in the DDR ram chunck accessible by the SoC. \n")
                name_file.write("//\tThe FC weights and biases may be stored in any place \n\n")
                
                for layer in self.net:
                    name_file.write(layer.get_weights_loading_string())
                
                        
                name_file.write("\n\n// #######################################################\n")
                name_file.write("// ################# MEMORY ALLOCATION ###################\n")
                name_file.write("// #######################################################\n\n")
                
                name_file.write("//\tEach layer produces an output. By default the output is stored in a dedicated DDR segment accessible by the accelerator.\n")
                name_file.write("//\tWe assume that the output of a layer will be the input of the next so we do not need to allocate RAM for the input.\n")
                name_file.write("//\tBy editing this you can customize the memory utilization\n\n")
                
                
                # allocate space for the first input
               # string = ""                
              #  size = np.prod(self.net[0].isizes)

               # string = "%s_param->input = (DATA*)neu_DDRmalloc(%d*sizeof(DATA));" % (self.net[0].name, size)
                
               # string += "\n"
               # name_file.write(string)

                
                #allocate space for the output
                
                if (self.smart_memalloc):
                  name_file.write(self.get_smart_mem_allocation_string())
                else:
                  name_file.write(self.get_mem_allocation_string())     
   
                name_file.write("\n\n// #######################################################\n")
                name_file.write("// ######################## WIRING #######################\n")
                name_file.write("// #######################################################\n\n")
                
                name_file.write("//\tEach layer has an input and an output pointer. The connection between two self.net is defined here\n\n")
                

                for layer in self.net:
                    name_file.write(layer.get_wiring_string())

                    
                

                name_file.write("\n\n")
                name_file.write("\n}")
                name_file.write("\n\n//#########################################################################################################################################\n")
                name_file.write("//#########################################################################################################################################\n\n")
                name_file.write("\nvoid cnnMain(DATA* image, {}** results)\n".format(self.results_datatype))
                name_file.write("{\n")
                
                name_file.write("SOCMAP soc = socs[0];\n")





                s = "SIZE input_size[] = {{{},{},{}}}; \n".format(layer.initializer.model_input_shape[1], layer.initializer.model_input_shape[2], layer.initializer.model_input_shape[3])
                s1= "SIZE batch_join_dim = {} * {} * {};\n".format(layer.initializer.model_input_shape[1], layer.initializer.model_input_shape[2], layer.initializer.model_input_shape[3])
               # s2 = "DATA* {} = (DATA*)neu_DDRmalloc(batch_join_dim*sizeof(DATA));\n\n".format(self.initializer.net_input)
                s2 = "for (int j=0;j<batch_join_dim;j++) {\n"
                for l in self.net:
                  if  l.operation=="Input":
                    s2+= "  {}_param->output[j]=image[j];\n\n".format(l.name)
                s2 += "}\n"
                
                name_file.write(s)
                name_file.write(s1)
                name_file.write(s2)
                #name_file.write("int job_id = 0; \n")
                #name_file.write("int job_id_sc = 0;\n")


#------------------stampo la funzione main con i layer che compongono la struttura della rete --------------------------------------------------


                for layer in self.net:

                    name_file.write(layer.get_forward_string())
#------------------------------------------------------------------------------------------------------------------------------------------------

                name_file.write("\n\n")
                global output_name
                
                last_layer=-1
                if self.net[-1].operation == "Region" or self.net[-1].operation == "Softmax" or self.net[-1].operation == "LogSoftmax":
                  last_layer-=1
                
           #     if (self.net[last_layer].operation == "Conv"):
                s="{}* res = ({} *) calloc({}, sizeof({}));\n".format(self.results_datatype, self.results_datatype, self.net[last_layer].get_output_pixels(),self.results_datatype)
                s += "for(unsigned int i = 0; i < {}; i++){{\n".format(self.net[last_layer].get_output_pixels())

                if (self.net[last_layer].datatype=='f' and self.results_datatype=="float") or \
                   (self.net[last_layer].datatype in ['h', 'b'] and self.results_datatype=="DATA"):
                  s+= "\tres[i] = {}_param->output[i] ;\n".format(self.net[last_layer].name)
                elif (self.net[last_layer].datatype in ['h', 'b'] and self.results_datatype=="float"):
                  s+= "\tres[i] = FIXED2FLOAT({}_param->output[i],{})  ;\n".format(self.net[last_layer].name, self.qf)
                else:
                  s+= "\tres[i] = FLOAT2FIXED({}_param->output[i],{})  ;\n".format(self.net[last_layer].name, self.qf)
                s+= "}\n"
                s+="*results = res;\n"
                s+= "}\n\n"
        #        else:
        #            s="{}* res = ({} *) calloc({}, sizeof({}));\n".format(self.results_datatype, self.results_datatype, self.net[last_layer].get_output_pixels(), self.results_datatype)
        #            s+=  "for(unsigned int i = 0; i < {}; i++){{\n".format(self.net[last_layer].get_output_pixels())
        #            
        #            if self.net[last_layer].datatype=='f':
        #              s+= "\tres[i] = {}_param->output[i] ;\n".format(self.net[last_layer].name)
        #            else:
        #              s+= "\tres[i] = FIXED2FLOAT({}_param->output[i],{})  ;\n".format(self.net[last_layer].name, self.qf)
        #              
        #            s+= '//\t_dprintf_("[%d] %{} \\n", i, {}_param->output[i]);\n'.format('f' if self.net[last_layer].datatype else 'd',self.net[last_layer].name)
        #              
        #            s+= "}\n"
        #            s+= "*results = res;\n"
        #            s+= "}\n"

                s+= "void free_platform(){\n"
                s+= "  munmap_soc(socs);\n"
                s+= "  free(socs[0]);\n"
                s+= "}"
                name_file.write(s)
                
                
                s= "\n\nvoid getInputImage(VARNAME image_path, DATA* image_pixels, VARSIZE* inSz){\n"
                s+="// This function is executed for each file in the images folder.\n"
                s+="// After you load the content of the file you can perform whatever preprocessing you want\n\n" 
                
                
                s+="  int input_type;\n\n"
  
                s+="  input_type= /*input_type here*/; //set the input type, 0 binary, 1 txt floating point, 2 JPEG to be normalized with imagenet normalization\n"
                s+="                                   //                    1 txt floating point, 2 JPEG to be normalized with imagenet normalization\n"
                s+="                                   //                    2 JPEG to be normalized with imagenet normalization\n"
                s+="                                   //                    3 JPEG with letterbox format (yolo-like), no stretch\n\n"

                s+="  switch (input_type){\n"
                s+="    case 0:\n"
                s+="      /*load_fixed expects images stored in binary format, the data shape (CHW or HWC), the data size (16 bit or 8 bit per pixel) */\n"
                s+="      /*and the representation (0 - 255; 0 - 1; -1 - 1) depends on the model and on the way it has been trained.*/\n"
                s+="      load_fixed(image_path,{},image_pixels);// loads binary files\n".format(np.prod(self.net[0].isizes))
                s+="      break;\n"
                s+="    case 1:\n"
                s+="      // loads txt files applying a normalization (the value depends on the dataset. Often it is the maximum value, for example 255.0) \n"
                s+="      load_float_txt(image_path,{},image_pixels,{}, /*set a nomalization value*/);\n".format(np.prod(self.net[0].isizes), self.qf)
                s+="      break;\n"
                s+="    case 2:\n"
                s+="      source_forward(image_path, image_pixels, inSz, {}); // preprocesses JPG images \n".format(self.qf)
                s+="      break;\n"
                s+="    case 3:\n"
                s+="      source_forward_letterbox(image_path, image_pixels, inSz, {}); // preprocesses JPG images \n".format(self.qf)
                s+="      break;\n"
                s+="    default:\n"
                s+='      printf ("Wrong input type!\\n");\n'
                s+="      exit(1);\n"
                s+="      break;\n"
                s+="  }\n"
                s+="}\n"
                
                name_file.write(s)
                
                
                s="\n\nint resultsProcessing({}* results, int size){{\n".format(self.results_datatype)
                s+="//What do you want to do with the results of the CNN? Here is the place where you should put the classifier or the detection (see YOLO detection for example)\n"
                s+="//The simplest classifier is a maximum search for the results which returns the index value of the maximum\n\n"


                if self.yolo_like:
                  s+=self.get_region()
                else:
                  s+=self.get_classifier()
                s+="}\n"
                
                
                name_file.write(s)
                
     
     
     
        def get_classifier(self):
          
          
          l= ' char *labels[%d]={'%self.net[-1].osizes[0]

          for i in range (0,self.net[-1].osizes[0]):
            l+='"label_%d", '%i
              
          l=l[0:-2]
          l+='};\n\n'

          s=l
          s+="// TODO: check the size parameter\n" 
          s+="  int size_results = {};\n".format(self.net[-1].osizes[0])
          
          s+="  double* r= (double*) malloc (size_results*sizeof(double));\n"
          s+="  int*  c= (int*)  malloc (size_results*sizeof(int));\n"
          s+="  double* results_float= (double*) malloc (size_results*sizeof(double));\n"

          s+="  double sum=0.0;\n"
          
          
          s+="\n\n// convert to double\n"
          s+="  double max=0;\n"
          s+="  for (int i =0;i<size_results;i++){\n"
          if self.results_datatype == 'float':
            s+="      results_float[i] = results[i];\n".format(self.qf)
          else:
            s+="      results_float[i] = FIXED2FLOAT(results[i],{});\n".format(self.qf)
          s+="    double n;\n"
          s+="    if (results_float[i]>0)\n"
          s+="      n=results_float[i];\n"
          s+="    else\n"
          s+="      n=-results_float[i];\n"
      
          s+="    if (n>max){\n"
          s+="      max=n;\n"
          s+="    }  \n"
          s+="  }\n"
          
          s+="\n\n// normalize results to avoid huge exponential results.\n"
          s+="  int norm =0;\n"
          s+="  if (norm == 1 && max>1.0)\n"
          s+="    for (int i =0;i<size_results;i++)\n"
          s+="      results_float[i]=results_float[i]/max;\n"
      
          
          s+="\n\n// Compute SOFTMAX r_i = exp(results_i)/sum_i(exp(results_i))\n"
          s+="  for (int i =0;i<size_results;i++)\n"
          s+="    sum+=exp(results_float[i]);\n\n"
          
          s+="  for (int i =0;i<size_results;i++){\n"
          s+="    r[i]=exp(results_float[i]) / sum;\n"
          s+="    c[i]=i;\n"
          s+="  }\n"
          
          
          s+="\n\n// sort results\n"
          s+="  for (int i =0;i<size_results;i++){\n"
          s+="    for (int j =i;j<size_results;j++){\n"
          s+="      if (r[j]>r[i]){\n"
          s+="        double t= r[j];\n"
          s+="        r[j]=r[i];\n"
          s+="        r[i]=t;\n"
          s+="        int tc= c[j];\n"
          s+="        c[j]=c[i];\n"
          s+="        c[i]=tc;\n"
          s+="      }\n"
          s+="    }\n"
          s+="  }\n"
          
          s+="\n\n// find top position and top confidence\n"
          s+="  int top0=0;\n"
          s+="  double topval=results_float[0];\n"
          s+="  for (int i =1;i<size_results;i++){\n"          
          s+="    if (results_float[i]>topval){\n"
          s+="      top0=i;\n"
          s+="      topval=results_float[i];\n"
          s+="    }  \n"
          s+="  }\n"
          
          
          s+="\n\n// Print the top classes\n"
          s+='  printf("\\n\\n");\n'          
          s+='  int top_n = 5;\n'
          s+='  if (top_n>size_results)\n'
          s+='    top_n=size_results;\n'
          s+="  for (int i =0;i<top_n;i++){\n"
          s+='    printf("            TOP %d: [%d] %s   ",i, c[i], labels[c[i]]);\n'
          s+="    for (int j =0;j<100*r[i];j++)\n"
          s+='      if (j<40) printf("#");\n'
          s+='    printf(" %0.1f%\\n",r[i]*100.0);\n'
          s+="  }  \n"
          s+='  printf("\\n\\n\\n\\n\\n");\n'
          
          s+='  return top0;\n\n\n'         
          
        
          return s
          
          
        def get_region(self):
                        
          s =  "region_param->input = (float*)malloc({} * sizeof(float));\n\n".format (self.net[-1].get_output_pixels())

          s += "for (int i = 0; i<{};i++){{\n".format (self.net[-1].get_output_pixels())

          
          if self.results_datatype == 'float':
            s += "  region_param->input[i]=results[i];\n".format(self.qf)
          else:
            s += "  region_param->input[i]=FIXED2FLOAT(results[i],{});\n".format(self.qf)
          s += "}\n\n"

          anchors = [str(a)+", " for sublist in self.net[-1].anchors for a in sublist]
          
          anchors = "".join(anchors)[0:-2]

          s += "float a[]={{{}}};\n".format(anchors)

          s += "//region_param->output=(float*)malloc(0 * sizeof(float));\n"
          s += "region_param->anchors=a;\n"

          s += "region_forward_wrap(region_param);\n"
          
          
          s += 'return -1;\n\n\n'
        
          return s
          
